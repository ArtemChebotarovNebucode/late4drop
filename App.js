import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import AppLoading from "expo-app-loading";
import AuctionsScreen from "./screens/AuctionsScreen/AuctionsScreen";
import ProductDetailsScreen from "./screens/ProductDetailsScreen/ProductDetailsScreen";
import LoginScreen from "./screens/LoginScreen/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen/RegisterScreen";
import ResetPasswordScreen from "./screens/ResetPasswordScreen/ResetPasswordScreen";
import TestingModal from "./screens/TestingModal/TestingModal";
import MainScreen from "./screens/MainScreen/MainScreen";
import ProfileScreen from "./screens/ProfileScreen/LoggedInUser/ProfileScreen";
import FavouriteProductsScreen from "./screens/FavouriteProductsScreen/FavouriteProductsScreen";
import CustomNavBar from "./components/partials/CustomNavBar/CustomNavBar";
import { Provider } from "react-redux";
import rootReducer from "./store/reducers/rootReducer";
import { createStore, applyMiddleware } from "redux";
import {
    BuyNowScreen,
    CalculationsScreen,
    ChatListScreen,
    ChatScreen,
    CreateAuction,
    GuaranteeScreen,
    NotificationsScreen,
    OpinionScreen,
    ProtectionBuyersScreen,
    SearchScreenMain,
    TrackingScreen, WebViewScreen,
} from "./screens";
import SoldProductsScreen from "./screens/SoldProductsScreen/SoldProductsScreen";
import MyOrdersScreen from "./screens/MyOrdersScreen/MyOrdersScreen";
import SpecialProductsScreen from "./screens/SpecialProductsScreen/SpecialProductsScreen";
import { AsyncStorage, LogBox } from "react-native";
import AuthProvider from "./contexts/AuthProvider/AuthProvider";
import { useFonts } from "expo-font";
import {
    Montserrat_400Regular,
    Montserrat_500Medium,
    Montserrat_600SemiBold,
    Montserrat_700Bold,
    Montserrat_800ExtraBold,
} from "@expo-google-fonts/montserrat";
import AnotherProfileScreen from "./screens/ProfileScreen/AnotherUser/AnotherProfileScreen";

const Stack = createStackNavigator();


LogBox.ignoreLogs(["Warning: ..."]); // Ignore log notification by message
LogBox.ignoreAllLogs(); //Ignore all log notifications


export default function App() {
    let [fontsLoaded] = useFonts({
        Montserrat_400Regular,
        Montserrat_800ExtraBold,
        Montserrat_600SemiBold,
        Montserrat_500Medium,
        Montserrat_700Bold,
    });

    // let [fontsLoaded] = useFonts({
    //     "FuturaLight": require("./assets/fonts/FuturaLight.ttf"),
    //     "FuturaBook": require("./assets/fonts/FuturaBook.ttf"),
    //     "FuturaMedium": require("./assets/fonts/FuturaMedium.ttf"),
    //     "FuturaBold": require("./assets/fonts/FuturaBold.ttf"),
    //     "FuturaHeavy": require("./assets/fonts/FuturaHeavy.ttf"),
    // });
    const [routeName, setRouteName] = useState("");

    const getToken = () => {
        AsyncStorage.getItem("ACCESS_TOKEN").then((routeName) => {
            setRouteName(routeName ? "BottomNav" : "Login");
        });
    };

    getToken();

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    const store = createStore(rootReducer);

    return (

        <AuthProvider>
            <Provider store={store}>
                {routeName ? (
                    <NavigationContainer>
                        <Stack.Navigator initialRouteName={routeName}>
                            <Stack.Screen
                                name="BottomNav"
                                component={CustomNavBar}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Chat"
                                component={ChatScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="ProtectionBuyers"
                                component={ProtectionBuyersScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Tracking"
                                component={TrackingScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="ChatList"
                                component={ChatListScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="SoldProducts"
                                component={SoldProductsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="AnotherUser"
                                component={AnotherProfileScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Guarantee"
                                component={GuaranteeScreen}
                                options={{ headerShown: false }}
                            />

                            <Stack.Screen
                                name="BuyNow"
                                component={BuyNowScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="TestingModal"
                                component={TestingModal}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Calculations"
                                component={CalculationsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Login"
                                component={LoginScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Register"
                                component={RegisterScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="ResetPassword"
                                component={ResetPasswordScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Notifications"
                                component={NotificationsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Opinion"
                                component={OpinionScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="CreateAuction"
                                component={CreateAuction}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Main"
                                component={SearchScreenMain}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="ProductDetails"
                                component={ProductDetailsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Profile"
                                component={ProfileScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="Auctions"
                                component={AuctionsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="MyOrders"
                                component={MyOrdersScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="SpecialProducts"
                                component={SpecialProductsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="FavouriteProducts"
                                component={FavouriteProductsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="WebView"
                                component={WebViewScreen}
                                options={{ headerShown: false }}
                            />
                        </Stack.Navigator>
                    </NavigationContainer>
                ) : (
                    <></>
                )}
            </Provider>
        </AuthProvider>
    );
}
