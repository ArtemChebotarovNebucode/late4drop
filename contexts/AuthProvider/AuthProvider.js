import React, { useContext, useState } from "react";
import { auth } from "../../firebase";
import { AsyncStorage } from "react-native";
import UserService from "../../services/userService";
import axios from "axios";

const AuthContext = React.createContext({});

export function useAuth() {
    return useContext(AuthContext);
}

const AuthProvider = ({ children }) => {
    const userService = new UserService();

    const [currentUser, setCurrentUser] = useState();
    const [activeProducts, setActiveProducts] = useState();
    const [userToken, setUserToken] = useState(null);
    const [loading, setLoading] = useState(true);
    const [lastAddedProducts, setLastAddedProducts] = useState([]);
    const [likedProducts, setLikedProducts] = useState([]);

    const signUp = (email, password) => {
        return auth.createUserWithEmailAndPassword(email, password);
    };

    const login = (email, password) => {
        return auth.signInWithEmailAndPassword(email, password);
    };

    const logout = () => {
        return auth.signOut();
    };

    const resetPassword = (email) => {
        return axios.post(
            `https://api.late4drop.com/user/reset-password`,
            JSON.stringify(email),
            {
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                },
            }
        );
    };

    const signInWithAuthProvider = async () => {};

    const refreshMyInfo = async (token) => {
        const userFetched = await userService.fetchMyInfo(token);
        setCurrentUser(userFetched?.data?.data?.user);
        setLikedProducts(userFetched?.data?.data?.user?.likedProductsIds)
        return userFetched;
    };
    const handleLikeProduct = (productId) => {
        setLikedProducts(prev => prev.includes(productId) ? prev.filter((item) => item !== productId) : prev.concat(productId))
    }

    const refreshMyActiveProducts = async (userId) => {
        const activeProductsFetched = await userService.getActiveProducts(
            userId
        );
        setActiveProducts(activeProductsFetched?.data.data);
    };

    const addLastAddedProduct = (product) => {
        setLastAddedProducts((prev) => [...prev, product]);
    };

    React.useEffect(() => {
        return auth.onAuthStateChanged((user) => {
            if (user) {
                user.getIdToken(false).then(async (token) => {
                    setUserToken(token);
                    await AsyncStorage.setItem("ACCESS_TOKEN", token);
                    await userService.login(token, user.email);

                    await refreshMyInfo(token).then(
                        async (user) =>
                            await refreshMyActiveProducts(
                                user?.data?.data?.user?.id
                            )
                    );
                });
            } else {
                setCurrentUser(null);
            }
            setLoading(false);
        });
    }, []);

    const value = {
        currentUser,
        userToken,
        activeProducts,
        lastAddedProducts,
        likedProducts,
        signUp,
        login,
        logout,
        resetPassword,
        refreshMyInfo,
        signInWithAuthProvider,
        setLastAddedProducts,
        addLastAddedProduct,
        refreshMyActiveProducts,
        handleLikeProduct
    };
    return (
        <AuthContext.Provider value={value}>
            {!loading && children}
        </AuthContext.Provider>
    );
};

export default AuthProvider;
