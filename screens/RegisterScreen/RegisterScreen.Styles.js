import { Dimensions, Platform, StyleSheet } from "react-native";
import Colors from "../../constants/Colors";
import Constants from "expo-constants";

const windowHeight = Dimensions.get("window").height;

export const styles = StyleSheet.create({
    container: {
        paddingTop: Constants.statusBarHeight,
        paddingHorizontal: 20,
        paddingBottom:
          Platform.OS === "android" ? 20 : Constants.statusBarHeight,
        flex: 1,
        marginTop: windowHeight > 560 ? windowHeight / 10 : 0,
    },
    title: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 28,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: 10,
        marginBottom: 40
    },
    subtitle: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 16,
        color: Colors.GRAY_TEXT,
        textAlign: "center",
    },
    socialNetworkButtons: {
        marginTop: 30,
        flexDirection: "row",
        marginBottom: 30,
    },
    footer: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
    },
    underInputs: {
        flexDirection: "row",
        marginTop: windowHeight > 560 ? 80 : 20,
        alignItems: "center",
        flexWrap: "wrap"
    },
    primaryButton: {
        marginTop: 20,
    },
    error: {
        fontSize: 11,
        fontFamily: 'Montserrat_400Regular',
        color: "#F20656",
    }
});
