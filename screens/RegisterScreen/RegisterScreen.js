import React, { useState } from "react";
import { AsyncStorage, Text, View } from "react-native";
import Colors from "../../constants/Colors";
import CircledButton from "../../components/buttons/CircledButton/CircledButton";
import CustomTextInput from "../../components/inputs/CustomTextInput/CustomTextInput";
import PrimaryButton from "../../components/buttons/PrimaryButton/PrimaryButton";
import CustomCheckBox from "../../components/inputs/CustomCheckBox/CustomCheckBox";
import TouchableText from "../../components/buttons/TouchableText/TouchableText";
import { styles } from "./RegisterScreen.Styles";
import { DismissKeyboard } from "../../components";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";
import UserService from "../../services/userService";
import MenuOption from "../ProfileScreen/LoggedInUser/MenuOption/MenuOption";

const RegisterScreen = ({ navigation }) => {
    const [providedData, setProvidedData] = useState({
        email: "",
        password: "",
    });
    const [isTermsChecked, setIsTermsChecked] = useState(false);
    const { signUp, signInWithAuthProvider, refreshMyInfo } = useAuth();
    const userService = new UserService();

    const [passwordError, setPasswordError] = useState("");
    const [emailError, setEmailError] = useState("");

    const handleOnRedirectToLogin = () => {
        navigation.navigate("Login");
    };

    const setErrorMessage = (message) => {
        if (message === "The email address is badly formatted.")
            setEmailError("Adres e-mail jest źle sformatowany.");
        else if (
            message ===
            "The email address is already in use by another account."
        )
            setEmailError("Adres e-mail jest już używany przez inne konto.");
    };
    const handleRegister = () => {
        setPasswordError("");
        setEmailError("");
        if (providedData.password.length >= 6 && isTermsChecked) {
            signUp(providedData.email, providedData.password)
                .then((resp) => {
                    resp.user.getIdToken(false).then((token) => {
                        userService
                            .login(token, providedData.email)
                            .then((respp) => {
                                refreshMyInfo(token);
                            });
                        AsyncStorage.setItem("ACCESS_TOKEN", token);
                        navigation.replace("BottomNav");
                    });
                })
                .catch((err) => {
                    setErrorMessage(err.message);
                });
        } else if (providedData.password.length < 6) {
            setPasswordError("Hasło powinno mieć co najmniej 6 znaków.");
        } else if (!isTermsChecked) {
        }
    };
    const registerWithGoogleAsync = () => {
        signInWithAuthProvider();
    };

    return (
        <DismissKeyboard>
            <View style={styles.container}>
                <Text style={styles.title}>ZAREJESTRUJ SIĘ</Text>

                {/*<View style={styles.socialNetworkButtons}>*/}
                {/*    <CircledButton*/}
                {/*        iconType={"svg"}*/}
                {/*        iconSource={require("../../assets/icons/facebook-icon.svg")}*/}
                {/*        iconWidth={21.6}*/}
                {/*        iconHeight={21.6}*/}
                {/*        additionalStyle={{*/}
                {/*            marginRight: 10,*/}
                {/*            backgroundColor: "#3B5998",*/}
                {/*        }}*/}
                {/*    />*/}
                {/*    <CircledButton*/}
                {/*        iconType={"png"}*/}
                {/*        iconSource={require("../../assets/icons/google-icon.png")}*/}
                {/*        iconWidth={21}*/}
                {/*        iconHeight={21}*/}
                {/*        additionalStyle={{*/}
                {/*            borderColor: Colors.GRAY_BACKGROUND,*/}
                {/*            borderWidth: 2,*/}
                {/*            backgroundColor: "white",*/}
                {/*        }}*/}
                {/*        onPress={() => registerWithGoogleAsync()}*/}
                {/*    />*/}
                {/*</View>*/}

                <View>
                    <CustomTextInput
                        inlineImageLeft={require("../../assets/icons/message-icon.svg")}
                        label={"Adres e-mail"}
                        textContentType={"email"}
                        placeholder={"Wpisz swój adres e-mail"}
                        onChangeText={(email) =>
                            setProvidedData({ ...providedData, email: email })
                        }
                    />
                    {emailError.length !== 0 ? (
                        <Text style={styles.error}>{emailError}</Text>
                    ) : null}

                    <CustomTextInput
                        additionalStyle={{ marginTop: 20 }}
                        inlineImageLeft={require("../../assets/icons/lock-icon.svg")}
                        label={"Hasło"}
                        textContentType={"password"}
                        placeholder={"Wpisz swoje hasło"}
                        onChangeText={(password) =>
                            setProvidedData({
                                ...providedData,
                                password: password,
                            })
                        }
                    />
                    {passwordError.length !== 0 ? (
                        <Text style={styles.error}>{passwordError}</Text>
                    ) : null}
                </View>

                <View style={styles.underInputs}>
                    <CustomCheckBox
                        label={"Klikając przycisk, akceptujesz nasz"}
                        additionalStyle={{ marginRight: 5 }}
                        fontSize={12}
                        isChecked={isTermsChecked}
                        setIsChecked={setIsTermsChecked}
                    />
                    <TouchableText
                        onPress={() =>
                            navigation.navigate("WebView", {
                                url: "https://late4drop.com/regulations/",
                                isPayments: false,
                            })
                        }
                        label={"Regulamin"}
                        fontSize={12}
                    />
                    <Text style={styles.error}>*</Text>
                </View>

                <PrimaryButton
                    additionalStyle={styles.primaryButton}
                    onPress={() => handleRegister()}
                    label={"ZAREJESTRUJ SIĘ"}
                />

                <View style={styles.footer}>
                    <View style={{ flexDirection: "row" }}>
                        <Text
                            style={{
                                marginRight: 10,
                                fontFamily: "Montserrat_400Regular",
                                fontSize: 14,
                            }}
                        >
                            Masz już konto?
                        </Text>

                        <TouchableText
                            label={"Zaloguj się"}
                            onPress={handleOnRedirectToLogin}
                        />
                    </View>
                </View>
            </View>
        </DismissKeyboard>
    );
};

export default RegisterScreen;
