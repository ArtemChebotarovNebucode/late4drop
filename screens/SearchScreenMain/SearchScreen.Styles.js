import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  title: {
    fontFamily: "Montserrat_700Bold",
    marginTop: 15,
    alignSelf:"center",
  },
});