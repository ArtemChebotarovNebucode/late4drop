import React, { useEffect, useState } from "react";
import { View } from "react-native";

import Header from "./Header/Header";

import Colors from "../../constants/Colors";
import { styles } from "./SearchScreen.Styles";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Offsets from "../../constants/Offsets";
import FiltersModal from "../../components/modals/FiltersModal/FiltersModal";
import ProductService from "../../services/productService";
import AuctionService from "../../services/auctionService";

import { connect } from "react-redux";
import { addSearchedProducts, setSearchedProducts} from "../../store/actions/productAction";
import Products from "./Products/Products";



const SearchScreenMain = (props) => {
    const [filtersAreVisible, setFiltersAreVisible] = useState(false);
    const [searchedText, setSearchedText] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const [page, setPage] = useState(0);
    // const [products, setProducts] = useState([]);
    const [filters, setFilters] = useState({
        brands: [],
        shoeSizes: [],
        clothingSizes: [],
        colors: [],
        conditions: [],
        categories: [],
        sort: "",
        minPrice: 0,
        maxPrice: 30000,
        page: page,
        searchString: ""
    });

    const handleOpenFilters = () => setFiltersAreVisible(true);
    const handleCloseFilters = () => setFiltersAreVisible(false);

    const productService = new ProductService();
    const auctionService = new AuctionService();

    const { searchedProductsTest, setProducts, addProducts } = props;

    const handleChangeFilters = (name, value) => {
        setFilters((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };

    const handleSearchProducts = () => {
        setIsLoading(true);
        // if (searchedText !== "") {
        //     productService.searchProducts(searchedText).then((response) => {
        //         setProducts(
        //             response.data.data !== undefined ? response.data.data : []
        //         );
        //         setIsLoading(false);
        //     });
        // } else {
            handleFilterProducts(page === 0);
        // }
    };

    const handleFilterProducts = (isStartPage) => {
        setIsLoading(true);
        productService
            .getAllProducts({ ...filters, page: page, searchString: searchedText })
            .then((response) => {
                if (isStartPage) {
                    setProducts(
                        response?.data?.data?.products !== undefined
                            ? response?.data?.data?.products
                            : []
                    );
                } else {
                    setProducts(
                        response?.data?.data?.products !== undefined
                            ? [...searchedProductsTest, ...response?.data?.data?.products]
                            : searchedProductsTest
                    );
                }
                setIsLoading(
                    !!(
                        response?.data?.data?.products &&
                        response?.data?.data?.products.length > 0
                    ) && !response?.data?.data?.products.length >= 40
                );
            })
            // .then(() => {
            //     auctionService
            //         .getAllAuction({ ...filters, page: page })
            //         .then((response) => {
            //             addProducts( response?.data?.data?.results);
            //         });
            // });
    };

    useEffect(() => {
        if (page !== 0) {
            handleFilterProducts(page === 0);
        }
    }, [page]);
    useEffect(() => {
        handleSearchProducts();
    }, [])

    useEffect(() => {
        setPage(0);
    }, [filters]);
    console.log(searchedProductsTest[0])
    return (
        <View style={styles.container}>
            <FiltersModal
                closeModal={handleCloseFilters}
                isVisible={filtersAreVisible}
                handleFilterProducts={handleFilterProducts}
                setFilters={setFilters}
            />
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />

            <Header
                handleOpenFilters={handleOpenFilters}
                setSearchedText={setSearchedText}
                handleSearchProducts={handleSearchProducts}
            />

            <View style={{ height: "100%" }}>
                <Products
                    usersProducts={searchedProductsTest}
                    setPage={setPage}
                    isLoading={isLoading}
                />

                <View style={{ height: Offsets.BOTTOM_OFFSET + 60 }} />
            </View>
        </View>
    );
};
const mapStateToProps = (state) => {
    return {
        searchedProductsTest: state.product.searchedProducts
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setProducts: (data) => dispatch(setSearchedProducts(data)),
        addProducts: (data) => dispatch(addSearchedProducts(data)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreenMain);

