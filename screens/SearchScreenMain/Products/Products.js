import React, { useState } from "react";
import {ActivityIndicator, FlatList, Text, View} from "react-native";

import ProductContainer from "../../../components/partials/ProductContainer/ProductContainer";

import { styles } from "./Products.Styles";
import { products } from "../../../data/dummyProductsData";
import {useAuth} from "../../../contexts/AuthProvider/AuthProvider";
import Colors from "../../../constants/Colors";
import {connect} from "react-redux";
import ProductService from "../../../services/productService";

const Products = ({
                      usersProducts,
                      setPage,
                      isLoading,
                      lastSearchingProducts,
                      isAuctionScreen
                  }) => {
    const { currentUser, likedProducts } = useAuth();
    const productService = new ProductService();


    const [products, setProducts] = useState([]);

    const handleEndResearch = () => {
        if (usersProducts?.length >= 40) {
            setPage((prev) => prev + 1);
        }
    };
    React.useEffect(() => {
        productService.getLateForDropChoiceProducts().then((response) => {
            setProducts(response.data.data);
        });
    }, []);
    const getHeaderListComponent = () => {
        return (
            <View>
                <Text
                    style={[styles.title, { paddingBottom: 15, marginTop: 10 }]}
                >
                    WYBÓR LATE4DROP
                </Text>
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    legacyImplementation={false}
                    data={products}
                    renderItem={({ item }) => (
                        <ProductContainer
                            special
                            productImg={
                                item?.images?.find((obj) => obj.size === "big").url
                            }
                            productTitle={item.title}
                            productPrice={item.price}
                            productOldPrice={item.oldPrice}
                            productSize={item.sizeName}
                            productLikesCount={0}
                        />
                    )}
                    keyExtractor={(product) => product.id}
                />
                <Text style={styles.title}>PRODUKTY</Text>
            </View>
        );
    };
    const getListFooterComponent = () => {
        return (
            <ActivityIndicator
                animating={isLoading}
                size={"small"}
                color={Colors.BLUE_TEXT}
            />
        );
    };


    return (
        <View style={styles.productsContainer}>
            <FlatList
                style={{ width: "100%" }}
                columnWrapperStyle={{ justifyContent: "space-around" }}
                vertical
                showsVerticalScrollIndicator={false}
                legacyImplementation={false}
                ListHeaderComponent={isAuctionScreen ? <></> : getHeaderListComponent()}
                data={usersProducts}
                numColumns={2}
                ListFooterComponent={getListFooterComponent()}
                onEndReached={() => handleEndResearch()}
                renderItem={({ item }) => (
                    <ProductContainer
                        key={item.id}
                        productImg={
                            item?.images?.find((obj) => obj.size === "big").url
                        }
                        productTitle={item.title}
                        productPrice={item?.minPrice === undefined  ? item?.price : item?.minPrice }
                        productOldPrice={item.oldPrice}
                        productSize={item?.size?.clothingSizeId  === null ? item?.size?.shoeSize?.name : item?.size?.clothingSize?.name}
                        productId={item.id}
                        productImagesCount={item?.images?.length}
                        additionalStyles={{ marginBottom: 10 }}
                        isProductLike={likedProducts?.includes(item.id)}
                        isAuction={item?.isAuction}
                        productEnds={item?.endsAt}
                        isSold={item?.title === "MEN stone"}
                    />
                )}
                keyExtractor={(product) =>
                    product?.minPrice === undefined
                        ? product?.id?.toString()
                        : product?.id?.toString() + product.minPrice
                }
            />
        </View>
    );
};
const mapStateToProps = (state) => {
    return {
        lastSearchingProducts: state.product.lastSearchingProducts,
    };
};

export default connect(mapStateToProps, null)(Products);
