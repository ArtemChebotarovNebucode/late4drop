import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    productsContainer: {
        flexDirection: "column",
        marginBottom: 20,
        justifyContent: "center",
        alignItems: 'center',
        flex: 1,
    },
    title: {
        fontFamily: "Montserrat_700Bold",
        marginBottom: 10,
        alignSelf:"center",
    },
});