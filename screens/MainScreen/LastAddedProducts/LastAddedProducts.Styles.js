import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        marginTop: 10
    },
    productsContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        marginBottom: 20,
        justifyContent: "space-around",
    }
});