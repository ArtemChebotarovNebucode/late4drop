import React, { useState } from "react";
import { Text, View } from "react-native";

import { styles } from "./LastAddedProducts.Styles";
import ProductContainer from "../../../components/partials/ProductContainer/ProductContainer";
import ProductService from "../../../services/productService";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";

const LastAddedProducts = ({ navigation, lastAddedProducts }) => {
    const productService = new ProductService();
    const [products, setProducts] = useState([]);
    const { currentUser } = useAuth();
    const [likedProductsIds, setLikedProductsIds] = useState(
        currentUser?.likedProductsIds ?? []
    );


    return (
        <View style={styles.container}>
            <Text
                style={{ fontFamily: "Montserrat_700Bold", marginBottom: 10 }}
            >
              OSTATNIO DODANE
            </Text>

            <View style={styles.productsContainer}>
                {lastAddedProducts.map((product) => {
                    return (
                        <ProductContainer
                            key={product.id}
                            productImg={product?.images?.find((obj) => obj.size === "big").url}
                            productTitle={product?.title}
                            productPrice={product?.price}
                            productOldPrice={product?.oldPrice}
                            productSize={product?.sizeName}
                            productId={product?.id}
                            productImagesCount={product?.images?.length}
                            additionalStyles={{ marginBottom: 10 }}
                            isProductLike={
                                likedProductsIds.find(
                                    (item) => item === product.id
                                )
                            }
                            productLikesCount={product.likesCount ?? 0}
                        />
                    );
                })}
            </View>
        </View>
    );
};

export default LastAddedProducts;
