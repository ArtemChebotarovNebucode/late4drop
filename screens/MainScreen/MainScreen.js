import React, { useEffect, useState } from "react";
import { ScrollView, View } from "react-native";

import Header from "./Header/Header";
import SpecialProducts from "./SpecialProducts/SpecialProducts";
import LastAddedProducts from "./LastAddedProducts/LastAddedProducts";

import Colors from "../../constants/Colors";
import { styles } from "./MainScreen.Styles";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import SearchedProducts from "./SearchedProducts/SearchedProducts";
import ProductService from "../../services/productService";
import FiltersModal from "../../components/modals/FiltersModal/FiltersModal";
import AuctionService from "../../services/auctionService";

import { connect } from "react-redux";
import { setProducts, addProducts, addLastAddedProducts } from "../../store/actions/productAction";

const MainScreen = (props) => {
    const [searchedText, setSearchedText] = useState("");
    const [filtersAreVisible, setFiltersAreVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [page, setPage] = useState(0);
    const [isStartScreen, setIsStartScreen] = useState(true);
    const [numberOfFounds, setNumberOfFounds] = useState(0);
    const { searchedProductsTest, setProducts, addProducts, addLastAddedProducts, lastAddedProducts } = props;

    const [filters, setFilters] = useState({
        brands: [],
        shoeSizes: [],
        clothingSizes: [],
        colors: [],
        conditions: [],
        categories: [],
        sort: "",
        minPrice: 0,
        maxPrice: 30000,
        page: 0,
    });

    const handleOpenFilters = () => setFiltersAreVisible(true);
    const handleCloseFilters = () => setFiltersAreVisible(false);

    const productService = new ProductService();
    const auctionService = new AuctionService();


    const handleSearchProducts = () => {
        if (searchedText === "" || !isStartScreen) {
            setIsLoading(true);
            productService.searchProducts(searchedText).then((response) => {
                setProducts(
                    response.data.data !== undefined ? response.data.data : []
                );
                setNumberOfFounds(
                    response.data.data !== undefined
                        ? response.data.data.length
                        : 0
                );
                setIsLoading(false);
                setPage(0);
            });
        } else {
            handleFilterProducts(page === 0);
        }
    };
    const handleFilterProducts = (isStartPage) => {
        if (!isStartScreen) {
            setIsLoading(true);
            productService
                .getAllProducts({ ...filters, page: page })
                .then((response) => {
                    if (isStartPage) {
                        setProducts(
                            response?.data?.data?.results !== undefined
                                ? response?.data?.data?.results
                                : []
                        );
                    } else {
                        setProducts(
                            response?.data?.data?.results !== undefined
                                ? [...searchedProductsTest, ...response?.data?.data?.results]
                                : searchedProductsTest
                        );
                    }
                    setIsLoading(
                        !!(
                            response?.data?.data?.results &&
                            response?.data?.data?.results.length > 0
                        )
                    );
                    setNumberOfFounds(
                        response?.data?.data?.count !== undefined
                            ? response?.data?.data?.count
                            : 0
                    );
                })
                .then(() => {
                    auctionService
                        .getAllAuction({ ...filters, page: page })
                        .then((response) => {
                            addProducts(response?.data?.data?.results);
                        });
                });
        }
    };

    useEffect(() => {
        handleSearchProducts();
    }, [searchedText]);
    useEffect(() => {
        if (page !== 0) {
            handleFilterProducts(page === 0);
        }
        setIsStartScreen(false);
    }, [page]);
    useEffect(() => {
        setPage(0);
    }, [filters]);
    React.useEffect(() => {
        productService.getLastAddedProducts().then((response) => {
            addLastAddedProducts(response.data.data !== undefined ? response.data.data : [] );
        });
    }, []);
    return (
        <View style={styles.container}>
            <FiltersModal
                closeModal={handleCloseFilters}
                isVisible={filtersAreVisible}
                handleFilterProducts={handleFilterProducts}
                setFilters={setFilters}
            />
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />

            <Header
                setSearchedText={setSearchedText}
                handleSearchProducts={handleSearchProducts}
                handleOpenFilters={handleOpenFilters}
            />
            <View style={{ flex: 1 }}>
                {/*{searchedText === "" && searchedProductsTest.length === 0 ? (*/}
                    <ScrollView showsVerticalScrollIndicator={false} style={{height: '100%'}}>
                        <View>
                            <SpecialProducts />
                            <LastAddedProducts lastAddedProducts={lastAddedProducts} />
                        </View>
                    </ScrollView>
                {/*) : (*/}
                    <SearchedProducts
                        searchedProducts={searchedProductsTest}
                        setPage={setPage}
                        isLoading={isLoading}
                        numberOfFounds={searchedProductsTest.length}
                    />
                {/*)}*/}
            </View>

        </View>
    );
};
const mapStateToProps = (state) => {
    return {
        searchedProductsTest: state.product.products,
        lastAddedProducts: state.product.lastAddedProducts,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setProducts: (data) => dispatch(setProducts(data)),
        addProducts: (data) => dispatch(addProducts(data)),
        addLastAddedProducts: (data) => dispatch(addLastAddedProducts(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
