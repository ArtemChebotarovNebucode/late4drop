import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    specialContainer: {
        alignItems: "center", marginTop: 10
    },
    title: {
        fontFamily: "Montserrat_700Bold",
        marginBottom: 10
    },
});