import React, { useState } from "react";
import { FlatList, Text, View } from "react-native";

import ProductContainer from "../../../components/partials/ProductContainer/ProductContainer";

import { styles } from "./SpecialProducts.Styles";
import ProductService from "../../../services/productService";

const SpecialProducts = () => {
    const productService = new ProductService();
    const [products, setProducts] = useState([]);

    React.useEffect(() => {
        productService.getLateForDropChoiceProducts().then((response) => {
            setProducts(response.data.data);
        });
    }, []);
    return (
        <View style={styles.specialContainer}>
            <Text style={styles.title}>WYBÓR LATE4DROP</Text>

            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                data={products}
                renderItem={({ item }) => (
                    <ProductContainer
                        special
                        productImg={
                            item?.images?.find((obj) => obj.size === "big").url
                        }
                        productTitle={item.title}
                        productPrice={item.price}
                        productOldPrice={item.oldPrice}
                        productSize={item.sizeName}
                        productLikesCount={0}
                    />
                )}
                keyExtractor={(product) => product.id}
            />
        </View>
    );
};

export default SpecialProducts;
