import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: 'center',
        marginTop: 10,
        flex: 1
    },
    productsContainer: {
        flexDirection: "column",
        marginBottom: 20,
        justifyContent: "center",
        alignItems: 'center',
        flex: 1,
        width: "100%"
    }
});