import React, { useState } from "react";
import { ActivityIndicator, FlatList, Text, View } from "react-native";

import { styles } from "./SearchedProducts.Styles";
import ProductContainer from "../../../components/partials/ProductContainer/ProductContainer";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";
import Colors from "../../../constants/Colors";

const SearchedProducts = ({
    navigation,
    searchedProducts,
    setPage,
    isLoading,
    numberOfFounds,
}) => {
    const { currentUser } = useAuth();
    const [likedProductsIds, setLikedProductsIds] = useState(
        currentUser?.likedProductsIds ?? []
    );
    const handleEndResearch = () => {
        if (searchedProducts.length >= 40) {
            setPage((prev) => prev + 1);
        }
    };
    const getListFooterComponent = () => {
        return (
            <ActivityIndicator
                animating={isLoading}
                size={"small"}
                color={Colors.BLUE_TEXT}
            />
        );
    };
    return (
        <View style={styles.container}>
            <Text
                style={{ fontFamily: "Montserrat_700Bold", marginBottom: 10 }}
            >
              Znalezione {numberOfFounds}
            </Text>

            <View style={styles.productsContainer}>
                <FlatList
                    style={{ width: "100%" }}
                    columnWrapperStyle={{ justifyContent: "space-around" }}
                    vertical
                    showsVerticalScrollIndicator={false}
                    legacyImplementation={false}
                    data={searchedProducts}
                    numColumns={2}
                    ListFooterComponent={getListFooterComponent()}
                    onEndReached={() => handleEndResearch()}
                    renderItem={({ item }) => (
                        <ProductContainer
                            key={item.id}
                            productImg={
                                item?.images?.find((obj) => obj.size === "big")
                                    .url
                            }
                            productTitle={item.title}
                            productPrice={item?.minPrice === undefined  ? item?.price : item?.minPrice }
                            productOldPrice={item?.oldPrice  ? item?.oldPrice : item?.minPrice }
                            productSize={item.sizeName}
                            productLikesCount={0}
                            productId={item.id}
                            productImagesCount={item?.images?.length}
                            additionalStyles={{ marginBottom: 10 }}
                            isProductLike={likedProductsIds.find(
                                (item) => item === item.id
                            )}
                            isAuction={item?.minPrice !== undefined}
                        />
                    )}
                    keyExtractor={(product) => product?.minPrice === undefined ? product?.id?.toString() : product?.id?.toString() + product.minPrice}
                />
            </View>
        </View>
    );
};

export default SearchedProducts;
