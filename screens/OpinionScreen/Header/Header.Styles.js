import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";
import Offsets from "../../../constants/Offsets";

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
    },
    text: {
        fontSize: 14,
        color: Colors.WHITE,
        textAlign: "center",
        paddingVertical: 12,
        backgroundColor: "#C4C4C4",
        width: 227,
        fontFamily: "Montserrat_400Regular",
        letterSpacing: 11,
    },
    header: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        top: 0,
        marginTop: Offsets.TOP_OFFSET,
        padding: 20,
    },
});
