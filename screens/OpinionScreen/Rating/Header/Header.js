import React from "react";
import { View, Text } from "react-native";
import { styles } from "./Header.Styles";
import Colors from "../../../../constants/Colors";
import SvgUri from "expo-svg-uri";
import StarRating from "react-native-star-rating";

const Header = ({ averageRating }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{averageRating}</Text>
        <View style={styles.stars}>
            <StarRating
                disabled={true}
                maxStars={5}
                rating={averageRating}
                fullStarColor={Colors.YELLOW_BACKGROUND}
                starSize={20}
                emptyStarColor={Colors.GRAY_BORDER}
                containerStyle={{
                    alignSelf: "flex-start",
                }}
                starStyle={{
                    marginRight: 2,
                }}
            />
        </View>

    </View>
  );
};

export default Header;
