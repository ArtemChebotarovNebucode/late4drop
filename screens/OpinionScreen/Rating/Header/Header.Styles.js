import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: 72,
    marginBottom: 8
  },
  text: {
    fontSize: 60,
    fontFamily: "Montserrat_700Bold",
  },
  stars: {
    paddingTop: 10
  },
});
