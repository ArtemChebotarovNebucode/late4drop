import {StyleSheet} from "react-native";
import Colors from "../../../../../constants/Colors";

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        height: 22,
        marginBottom: 8,
        alignItems: 'center',
    },
    text: {
        color: Colors.GRAY_TEXT,
        paddingLeft: 2,
        paddingRight: 7,
        fontFamily: 'Montserrat_700Bold'
    },
    lineWrapper: {
        height: 4,
        flex: 1,
        backgroundColor: Colors.GRAY_BACKGROUND,
        borderRadius: 2,
        marginRight: 9
    },
    line: {
        backgroundColor: Colors.BLUE_TEXT,
        position: 'absolute',
        zIndex:2,
        height: 4,
        borderRadius: 2
    },
    percents: {
        fontSize: 16,
        color: Colors.LIGHT_BLACK_TEXT,
        fontFamily: 'Montserrat_700Bold'
    }
});
