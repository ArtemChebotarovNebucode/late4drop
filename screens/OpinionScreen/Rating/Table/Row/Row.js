import React from "react";
import { View, Text } from "react-native";
import { styles } from "./Row.Styles";
import SvgUri from "expo-svg-uri";
import Colors from "../../../../../constants/Colors";

const Row = ({ index, quantityRating, rating }) => {
  const percents = Math.round((rating * 100) / quantityRating) || 0;
  return (
    <View style={styles.wrapper}>
      <SvgUri
        width={12}
        height={12}
        fill={Colors.YELLOW_BACKGROUND}
        source={require("../../../../../assets/icons/star-icon.svg")}
      />
      <Text style={styles.text}>{5 - index}</Text>
      <View style={styles.lineWrapper}>
        <View style={[styles.line, { width: `${percents}%` }]} />
      </View>
      <Text style={styles.percents}>{percents}%</Text>
    </View>
  );
};

export default Row;
