import React from "react";
import { View } from "react-native";
import Row from "./Row/Row";
import { ratings } from "../../ratings";

const Table = () => {

  // TODO: when will appear star rating - to integrate

  const quantityRating = ratings.reduce((prev, next) => {
    return prev + next.quantity;
  }, 0);

  return (
    <View>
      {[...Array(5)].map((item, index) => (
        <Row
          key={index}
          index={index}
          quantityRating={quantityRating}
          rating={ratings[index].quantity}
        />
      ))}
    </View>
  );
};

export default Table;
