import React from "react";
import {View} from "react-native";
import Header from "./Header/Header";
import Table from "./Table/Table";

const Rating = ({averageRating}) => {
    return (
        <View style={{paddingBottom: 40}}>
            <Header averageRating={averageRating} />
            <Table />
        </View>
    )
}

export default Rating;