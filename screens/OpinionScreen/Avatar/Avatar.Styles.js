import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 30,
  },
  image: {
    width: 133,
    height: 133,
    borderRadius: 70
  },
  text: {
    fontSize: 14,
    color: Colors.BLACK_TEXT,
    fontFamily: "Montserrat_400Regular",
    paddingTop: 8,
  },
});
