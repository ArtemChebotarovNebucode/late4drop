import React from "react";
import { View, Image, Text } from "react-native";
import { styles } from "./Avatar.Styles";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";

const Avatar = ({imageUrl, name}) => {

    return (
        <View style={styles.container}>
            <Image
                style={styles.image}
                source={imageUrl ? { uri: imageUrl } : require('../../../assets/images/profile-photo-standard.png')}
            />
            <Text style={styles.text}>{name || "No Name"}</Text>
        </View>
    );
};

export default Avatar;
