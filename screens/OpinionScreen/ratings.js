export const ratings = [
  { rating: 5, quantity: 0 },
  { rating: 4, quantity: 0 },
  { rating: 3, quantity: 0 },
  { rating: 2, quantity: 0 },
  { rating: 1, quantity: 0 },
];
