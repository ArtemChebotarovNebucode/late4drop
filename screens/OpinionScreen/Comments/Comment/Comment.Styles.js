import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginTop: 26,
    marginBottom: 17,
  },
  text: {
    fontFamily: "Montserrat_700Bold",
    fontSize: 14,
    color: Colors.LIGHT_BLACK_TEXT,
  },

  ratingContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 6,
    marginBottom: 13,
  },
  day: {
    fontFamily: "Montserrat_400Regular",
    fontSize: 14,
    color: Colors.GRAY_TEXT,
    marginHorizontal: 5,
  },
  comment: {
    fontSize: 14,
    fontFamily: "Montserrat_400Regular",
  },
});
