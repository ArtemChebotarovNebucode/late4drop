import React from "react";
import { Image, Text, View } from "react-native";
import { styles } from "./Comment.Styles";
import StarRating from "react-native-star-rating";
import Colors from "../../../../constants/Colors";

const Comment = ({
  userAvatar,
  userName,
  userRating,
  commentDate,
  comment,
}) => {
  return (
    <View style={styles.container}>
      <Image
        source={userAvatar}
        style={{ marginRight: 18, width: 38, height: 38 }}
      />
      <View>
        <Text style={styles.text}>{userName}</Text>
        <View style={styles.ratingContainer}>
          <StarRating
            disabled={true}
            maxStars={5}
            rating={userRating}
            fullStarColor={Colors.YELLOW_BACKGROUND}
            emptyStarColor={Colors.GRAY_BORDER}
            starSize={12}
            starStyle={{
              marginRight: 2,
            }}
          />
          <Text style={styles.day}>
            {`${Math.ceil(
              (new Date() - new Date(commentDate)) / 8.64e7
            )} dni temu`}
          </Text>
        </View>
        <View style={{width: '95%'}}>
          <Text style={styles.comment}>{comment}</Text>
        </View>
      </View>
    </View>
  );
};

export default Comment;
