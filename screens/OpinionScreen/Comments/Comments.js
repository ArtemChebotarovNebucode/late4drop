import React from "react";
import { Text, View } from "react-native";
import { styles } from "./Comments.Styles";
import Comment from "./Comment/Comment";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";

const Comments = ({ isAnotherUser }) => {
    const { currentUser } = useAuth();

    return (
        <View>
            {currentUser?.opinions?.length === 0 ? (
                <View
                    style={{
                        width: "100%",
                        height: 100,
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Text style={styles.infoMessage}>
                        {isAnotherUser
                            ? "Dany użytkownik nie ma żadnych opinii. Żeby wystawić opinie musisz coś kupić.”"
                            : "Aktualnie nie masz żadnych opinii."}
                    </Text>
                </View>
            ) : (
                // TODO: when will appear opinions - to integrate
                <View>
                    <Comment
                        userAvatar={require("../../../assets/images/profile-photo.png")}
                        userName={"Jan Kowalski"}
                        userRating={4}
                        commentDate={"2021-02-26"}
                        comment={
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed odio non magna efficitur malesuada"
                        }
                    />
                    <Comment
                        userAvatar={require("../../../assets/images/profile-photo.png")}
                        userName={"Jan Kowalski"}
                        userRating={3}
                        commentDate={"2021-02-24"}
                        comment={
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed odio non magna efficitur malesuada"
                        }
                    />
                </View>
            )}
        </View>
    );
};

export default Comments;
