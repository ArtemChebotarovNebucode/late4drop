import { Dimensions, StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  stickyNav: {
    backgroundColor: Colors.BLUE,
    width: "60%",
    bottom: Dimensions.get("window").height / 20,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 100,
  },
  wrapper: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",

    height: 60,
  },
  text: {
    color: Colors.WHITE,
    fontSize: 12,
    fontFamily: "Montserrat_400Regular",
    marginHorizontal: 30
  },
  numberWrapper: {
    backgroundColor: Colors.WHITE,
    height: 38,
    width: 38,
    borderRadius: 19,
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    right: 10,
  },
  number: {
    fontSize: 16,
    fontFamily: "Montserrat_700Bold",
  },
});
