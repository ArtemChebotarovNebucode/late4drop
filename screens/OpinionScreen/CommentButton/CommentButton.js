import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { styles } from "./CommentButton.Styles";

const CommentButton = ({ openModal }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => openModal()} style={styles.stickyNav}>
        <View style={styles.wrapper}>
          <Text style={styles.text}>WYSTAW OPINIĘ</Text>
          <View style={styles.numberWrapper}>
            <Text style={styles.number}>12</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CommentButton;
