import React, { useEffect, useState } from "react";
import { ScrollView, View } from "react-native";
import Header from "./Header/Header";
import { styles } from "./OpinionScreen.Styles";
import Avatar from "./Avatar/Avatar";
import Rating from "./Rating/Rating";
import Comments from "./Comments/Comments";
import CommentButton from "./CommentButton/CommentButton";
import OpinionModal from "../../components/modals/OpinionModal/OpinionModal";
import MessageModal from "../../components/modals/MessageModal/MessageModal";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Colors from "../../constants/Colors";
import Offsets from "../../constants/Offsets";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";

const OpinionScreen = ({ navigation, route  }) => {
    const [averageRating, setAverageRating] = useState(0);
    const [isOpinionVisible, setIsOpinionVisible] = useState(false);
    const [isMessageVisible, setIsMessageVisible] = useState(false);
    const { currentUser } = useAuth();

    const { userId, userInfo, isAnotherUser  } = route.params;


    const openOpinionModal = () => {
        setIsOpinionVisible(true);
    };
    const openMessageModal = () => {
        setIsOpinionVisible(false);
        setIsMessageVisible(true);
    };
    const closeOpinionModal = () => {
        setIsOpinionVisible(false);
    };
    const closeMessageModal = () => {
        setIsMessageVisible(false);
    };

    return (
        <View style={styles.wrapper}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header navigation={navigation} />
            <ScrollView
                style={{ width: "100%" }}
                showsVerticalScrollIndicator={false}
            >
                <View style={styles.container}>
                    <Avatar
                        imageUrl={userInfo?.photoURL}
                        name={userInfo?.displayName}
                    />
                    <Rating averageRating={averageRating} />

                    <Comments isAnotherUser={isAnotherUser} />
                </View>
                <View
                    style={
                        Platform.OS === "ios"
                            ? { height: Offsets.BOTTOM_OFFSET - 10 }
                            : { height: Offsets.BOTTOM_OFFSET + 60 }
                    }
                />
            </ScrollView>
            {currentUser?.id  !== userId ? <CommentButton openModal={openOpinionModal} /> : <></>}


            <OpinionModal
                closeModal={closeOpinionModal}
                isVisible={isOpinionVisible}
                averageRating={averageRating}
                openMessageModal={openMessageModal}
                imageUrl={userInfo?.photoURL}
                name={userInfo?.displayName}
            />
            <MessageModal
                closeModal={closeMessageModal}
                isVisible={isMessageVisible}
                userId={userInfo?.id}
                imageUrl={userInfo?.photoURL}
                name={userInfo?.displayName}
            />
        </View>
    );
};

export default OpinionScreen;
