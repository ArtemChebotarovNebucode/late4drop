import React from "react";
import { View, StyleSheet, Text } from "react-native";
import Colors from "../../../constants/Colors";

const Delivery = ({ delivery, trackNumber }) => {
    return (
        <View>
            <Text style={styles.name}>PRZEWOŹNIK I NUMER PRZESYŁKI</Text>
            <Text style={styles.text}>{delivery}</Text>
            <Text style={styles.text}>{trackNumber}</Text>
        </View>
    );
};

export const styles = StyleSheet.create({
    name: {
        fontSize: 12,
        fontFamily: "Montserrat_700Bold",
        color: Colors.GRAY_TEXT,
        marginBottom: 10
    },
    text: {
        fontSize: 12,
        fontFamily: "Montserrat_500Medium",
        color: Colors.GRAY_TEXT,
        marginBottom: 3
    },
});

export default Delivery;
