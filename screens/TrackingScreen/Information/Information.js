import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Colors from "../../../constants/Colors";
import Item from "./Item/Item";
import Delivery from "../Delivery/Delivery";

const Information = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.name}>STATUS PRZESYŁKI</Text>
            <Item
                status={"Oczekuje na nadanie"}
                isCompleted={true}
                date={new Date()}
            />
            <Item status={"W drodze"} isCompleted={true} date={new Date()} />
            <Item status={"Weryfikacja Late4drop"} isCompleted={true} date={new Date()} />
            <Item status={"Wyslana ponownie"} isCompleted={true} date={new Date()} />
            <Item
                status={"Oczekuje na odbiór"}
                isCompleted={true}
                date={new Date()}
            />
            <Item status={"Doręczona"} isCompleted={false} date={new Date()} />
            <Delivery
                delivery={"InPost"}
                trackNumber={"632742389423846239423423"}
            />
        </View>
    );
};

export const styles = StyleSheet.create({
    name: {
        fontSize: 12,
        fontFamily: "Montserrat_700Bold",
        color: Colors.GRAY_TEXT,
        marginBottom: 20,
    },
});

export default Information;