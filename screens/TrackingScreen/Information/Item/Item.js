import React from "react";
import { View, StyleSheet, Text } from "react-native";
import SvgUri from "expo-svg-uri";
import Colors from "../../../../constants/Colors";

const Item = ({ status, isCompleted, date }) => {
    const iconPath = isCompleted
        ? require("../../../../assets/icons/filled-circle-icon.svg")
        : require("../../../../assets/icons/circle-icon.svg");
    const getDate = () => {
        return `${date.toLocaleString()}`;
    };

    const getLine = () => {
        return status !== "Doręczona" ? (
            <View>
                <View style={styles.line} />
            </View>
        ) : (
            <></>
        );
    };

    return (
        <View style={styles.container}>
            <View style={styles.status}>
                <SvgUri width={15} height={15} source={iconPath} />
                {getLine()}
            </View>
            <View>
                <Text style={styles.text}>{status}</Text>
                {isCompleted ? <Text style={styles.text}>{getDate()}</Text> : <></>}
            </View>
        </View>
    );
};

export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        height: 65,
    },
    status: {
        marginRight: 13,
        alignItems: "center",
    },
    text: {
        fontSize: 12,
        fontFamily: "Montserrat_500Medium",
        color: Colors.GRAY_TEXT,
        paddingBottom: 6,
    },
    line: {
        marginTop: 3,
        width: 1,
        height: "80%",
        backgroundColor: Colors.BLUE,
    },
});

export default Item;
