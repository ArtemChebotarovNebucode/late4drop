import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import Header from "./Header/Header";
import Information from "./Information/Information";
import Offsets from "../../constants/Offsets";

const TrackingScreen = () => {
    return (
        <View style={styles.wrapper}>
            <Header />
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <Information />
                <View style={{height: Offsets.BOTTOM_OFFSET + 20}} />
             </ScrollView>
        </View>
    );
};

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        marginHorizontal: 20,
        paddingTop: 20,
    },
});

export default TrackingScreen;
