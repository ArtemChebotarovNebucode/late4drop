import React from "react";
import {View, StyleSheet, Text, Platform} from "react-native";
import HeaderBar from "../../../components/partials/HeaderBar/HeaderBar";
import Colors from "../../../constants/Colors";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import Constants from "expo-constants";
import { useNavigation } from "@react-navigation/native";

const Header = () => {
    const navigation = useNavigation();

    return (
        <View>
            <HeaderBar
                backgroundColor={Colors.BLACK_BACKGROUND}
                barStyle={"light-content"}
            >
                <View style={styles.wrapper}>
                    <CircledButton
                        iconType={"svg"}
                        iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                        iconHeight={15}
                        iconWidth={15}
                        backgroundColor={Colors.GRAY_BACKGROUND}
                        fill={Colors.BLUE}
                        onPress={() => navigation.goBack()}
                    />
                    <Text style={styles.text}>TWOJA PRZESYŁKA</Text>
                </View>
            </HeaderBar>
        </View>
    )
}

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
        paddingTop: Platform.OS === "android" ? Constants.statusBarHeight : 0,
    },
    text: {
        fontSize: 22,
        color: Colors.WHITE,
        marginTop: 0,
        marginRight: "auto",
        marginBottom: 0,
        marginLeft: "auto",
        fontFamily: "Montserrat_700Bold",
    },
});

export default Header;