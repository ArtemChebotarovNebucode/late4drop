import React from "react";
import {View} from "react-native";
import {Item, Title} from "../Info";
import Colors from "../../../constants/Colors";

const Other = () => {
    return (
        <View>
            <Title title={"CZEGO NIE OBEJMUJE OCHRONA"} />
            <View>
                <Item text={"Transakcje zakończone poza Legit and late4drop (transakcje osobiste, wymiany itp.)"} fill={Colors.BLUE} />
                <Item text={"Produkty wysłane bez pełnego śledzenia online przesyłki "} fill={Colors.BLUE} />
                <Item text={"Elmenty zgłoszone po 90 dniach od daty zakupu"} fill={Colors.BLUE} />
            </View>
        </View>
    )
}

export default Other;