import { Dimensions, StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        marginHorizontal: 20,
        paddingTop: 10,
    },
});
