import React from "react";
import { View } from "react-native";
import { Title, Item } from "../Info";
import Colors from "../../../constants/Colors";

const ScopeInfo = () => {
    return (
        <View>
            <Title title={"ZAKRES OCHRONY"} />
            <View>
                <Item text={"Kupujący twierdzi, że przedmiot nie został odebrany, ale przesyłka została \n" +
                "oznaczona jako dostarczona"} fill={Colors.BLUE} />
                <Item text={"Kupujący twierdzi, że produkt nie jest zgodny z opisem, ale jest"} fill={Colors.BLUE} />
                <Item text={"Kupujący twierdzi, że przedmiot jest nieautentyczny, aale przedmiot jest autentyczny"} fill={Colors.BLUE} />
            </View>
        </View>
    );
};

export default ScopeInfo;
