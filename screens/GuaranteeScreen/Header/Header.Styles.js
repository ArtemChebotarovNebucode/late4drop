import { Dimensions, StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

const windowWidth = Dimensions.get("window").width;

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
    },
    text: {
        fontSize: windowWidth > 360 ? 18 : 15,
        color: Colors.WHITE,
        marginTop: 0,
        marginRight: "auto",
        marginBottom: 0,
        marginLeft: "auto",
        fontFamily: "Montserrat_700Bold",
    },
});
