import React from "react";
import {View, Text} from "react-native";
import {styles} from './Title.Styles'
import SvgUri from "expo-svg-uri";

const Title = () => {
    return (
        <View style={styles.container}>
            <SvgUri width={58} height={48} source={require('../../../assets/icons/guarantee-icon.svg')} />
            <Text style={styles.text}>OCHRONA SPRZEDAJĄCYCH</Text>
        </View>
    )
}

export default Title;