import { StyleSheet, Dimensions } from "react-native";
import Colors from "../../../constants/Colors";

const windowWidth = Dimensions.get("window").width;

export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_LINE,
        paddingHorizontal: 8,
        paddingVertical: 12,
        width: "100%",
        marginTop: 12,
        marginBottom: 34,
    },
    text: {
        fontSize: windowWidth > 360 ? 16 : 13,
        fontFamily: "Montserrat_400Regular",
        color: Colors.BLACK_TEXT,
        marginTop: 0,
        marginRight: "auto",
        marginBottom: 0,
        marginLeft: "auto",
    },
});
