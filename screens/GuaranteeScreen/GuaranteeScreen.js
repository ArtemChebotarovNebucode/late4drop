import React from "react";
import { ScrollView, View } from "react-native";
import { styles } from "./GuaranteeScreen.Styles";
import Header from "./Header/Header";
import Title from "./Title/Title";
import ScopeInfo from "./ScopeInfo/ScopeInfo";
import Other from "./Other/Other";

const GuaranteeScreen = () => {
    return (
        <View style={styles.wrapper}>
            <Header />
            <ScrollView
                style={{ width: "100%" }}
                showsVerticalScrollIndicator={false}
            >
                <View style={styles.container}>
                    <Title />
                    <ScopeInfo />
                    <Other />
                </View>
            </ScrollView>
        </View>
    );
};

export default GuaranteeScreen;