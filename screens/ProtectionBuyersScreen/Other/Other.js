import React from "react";
import {View} from "react-native";
import {Item, Title} from "../Info";
import Colors from "../../../constants/Colors";

const Other = () => {
    return (
        <View>
            <Title title={"CZEGO NIE OBEJMUJE OCHRONA"} />
            <View>
                <Item text={"Transakcje zakończone poza Legit and late4drop (transakcje osobiste, wymiany itp.)"} fill={Colors.BLUE} />
                <Item text={"Przedmioty, które nie pasują (rozmiar)"} fill={Colors.BLUE} />
                <Item text={"Przedmioty, które zdecydowałeś, że nie chcesz już po zakupie"} fill={Colors.BLUE} />
                <Item text={"Roszczenia, które zostały przedwcześnie zamknięte lub zostały złożone nieprawidłowo"} fill={Colors.BLUE} />
                <Item text={"Przedmioty, które zdecydowałeś, że nie chcesz już po zakupie"} fill={Colors.BLUE} />
            </View>
        </View>
    )
}

export default Other;