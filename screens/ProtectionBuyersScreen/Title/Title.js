import React from "react";
import {View, Text} from "react-native";
import {styles} from './Title.Styles'
import SvgUri from "expo-svg-uri";

const Title = () => {
    return (
        <View style={styles.container}>
            <SvgUri width={58} height={48} source={require('../../../assets/icons/buyers-protection-icon.svg')} />
            <Text style={styles.text}>OCHRONA KUPUJĄCYCH</Text>
        </View>
    )
}

export default Title;