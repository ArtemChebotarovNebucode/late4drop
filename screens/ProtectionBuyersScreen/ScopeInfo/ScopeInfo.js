import React from "react";
import { View } from "react-native";
import { Title, Item } from "../Info";
import Colors from "../../../constants/Colors";

const ScopeInfo = () => {
    return (
        <View>
            <Title title={"ZAKRES OCHRONY"} />
            <View>
                <Item text={"Produkty nie zostały dostarczone"} fill={Colors.BLUE} />
                <Item text={"Pozycje niezgodne z opisem"} fill={Colors.BLUE} />
                <Item text={"Przedmioty nieautentyczne"} fill={Colors.BLUE} />
            </View>
        </View>
    );
};

export default ScopeInfo;
