import React from "react";
import {Text, View} from "react-native";
import {styles} from './Header.Styles'
import HeaderBar from "../../../components/partials/HeaderBar/HeaderBar";
import Colors from "../../../constants/Colors";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import { useNavigation } from "@react-navigation/core";

const Header = () => {
  const navigation = useNavigation();

    return (
        <View>
            <HeaderBar backgroundColor={Colors.BLACK_BACKGROUND} barStyle={"light-content"}>
                <View style={styles.wrapper}>
                    <CircledButton
                        iconType={"svg"}
                        iconSource={require("../../../assets/icons/arrow-left.svg")}
                        iconHeight={15}
                        iconWidth={15}
                        backgroundColor={Colors.GRAY_BACKGROUND}
                        fill={Colors.BLUE}
                        onPress={() => navigation.goBack()}
                    />
                    <Text style={styles.text}>OCHRONA KUPUJĄCYCH</Text>
                </View>
            </HeaderBar>
        </View>
    )
}

export default Header;