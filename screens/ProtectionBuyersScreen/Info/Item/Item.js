import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";
import SvgUri from "expo-svg-uri";

const Item = ({ text, fill }) => {
    return (
        <View style={styles.container}>
            <SvgUri
                style={styles.icon}
                fill={fill}
                width={15}
                height={2}
                source={require("../../../../assets/icons/pink-line-icon.svg")}
            />
            <Text style={styles.text}>{text}</Text>
        </View>
    );
};

export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        marginBottom: 24,
    },
    text: {
        fontSize: 14,
        color: Colors.GRAY_LABEL,
        fontFamily: "Montserrat_500Medium",
        flex: 1,
        flexWrap: "wrap",
    },
    icon: {
        marginTop: 7,
        marginRight: 12
    }
});

export default Item;
