import React from "react";
import {View, Text, StyleSheet} from "react-native";
import Colors from "../../../../constants/Colors";

const Title = ({title}) => {
    return (
        <View>
            <Text style={styles.text}>{title}</Text>
        </View>
    )
}

export const styles = StyleSheet.create({
    text: {
        fontSize: 18,
        color: Colors.BLACK_BACKGROUND,
        fontFamily: 'Montserrat_400Regular',
        marginBottom: 33
    }
});

export default Title;
