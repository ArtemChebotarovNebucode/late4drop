import React, { useEffect, useState } from "react";
import { View, StyleSheet, SafeAreaView } from "react-native";
import Header from "./Header/Header";
import Chat from "./Chat/Chat";
import Colors from "../../constants/Colors";
import InputBox from "./Chat/InputBox/InputBox";
import ChatService from "../../services/chatService";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";

const ChatScreen = ({ route }) => {
    const { chatId, recipientId, photoURL, name } = route.params;
    const charService = new ChatService();
    const { userToken } = useAuth();
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState("");
    const [scroll, setScroll] = useState(null);

    const scrollToEnd = () => {
        scroll?.scrollToEnd({ animated: false });
    };

    const handleSendMessage = (message) => {
        charService
            .sendMessage(userToken, recipientId, message)
            .then((response) => {
                setMessages((prev) => [...prev, response?.data?.data?.message]);
                setMessage("");
                scrollToEnd()
            });
    };

    useEffect(() => {
        charService.getChatById(userToken, chatId).then((response) => {
            setMessages(response.data.data);
        });
    }, []);
    return (
        <View style={styles.wrapper}>
            <Header photoURL={photoURL} name={name}/>
            <SafeAreaView style={styles.container}>
                <Chat
                    messages={messages}
                    scroll={scroll}
                    scrollToEnd={scrollToEnd}
                    setScroll={setScroll}
                />
            </SafeAreaView>
            <InputBox
                handleSendMessage={handleSendMessage}
                message={message}
                setMessage={setMessage}
            />
        </View>
    );
};
export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        // backgroundColor: Colors.BLACK_BACKGROUND,
    },
    container: {
        flex: 1,
    },
});

export default ChatScreen;
