import React from "react";
import { Image, Platform, StyleSheet, Text, View } from "react-native";
import HeaderBar from "../../../components/partials/HeaderBar/HeaderBar";
import Colors from "../../../constants/Colors";
import Constants from "expo-constants";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import { useNavigation } from "@react-navigation/core";

const Header = ({photoURL, name}) => {
    const navigation = useNavigation();

    return (
        <View>
            <HeaderBar
                backgroundColor={Colors.BLACK_BACKGROUND}
                barStyle={"light-content"}
            >
                <View style={styles.wrapper}>
                    <View>
                        <CircledButton
                            iconType={"svg"}
                            iconSource={require("../../../assets/icons/arrow-left.svg")}
                            iconHeight={15}
                            iconWidth={15}
                            additionalStyle={styles.icon}
                            backgroundColor={Colors.GRAY_BACKGROUND}
                            fill={Colors.BLUE}
                            onPress={() => navigation.goBack()}
                        />
                    </View>
                    <View style={styles.container}>
                        <Text style={styles.text}>{name}</Text>
                    </View>

                    <Image
                        style={styles.image}
                        source={photoURL ? {uri: photoURL} : require('../../../assets/images/profile-photo-standard.png')}
                    />
                </View>
            </HeaderBar>
        </View>
    );
};
export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingTop: Platform.OS === "android" ? Constants.statusBarHeight : 0,
    },
    container: {
        alignItems: "center",
        marginTop: 0,
        marginRight: "auto",
        marginBottom: 0,
        marginLeft: "auto",
        flexDirection: "row",
    },
    image: {
        width: 37,
        height: 37,
        borderRadius: 20
    },
    text: {
        fontSize: 16,
        color: Colors.WHITE,
        fontFamily: "Montserrat_700Bold",
    },
    icon: {
    },
});

export default Header;