import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import Colors from "../../../../constants/Colors";
import { useAuth } from "../../../../contexts/AuthProvider/AuthProvider";

const Message = ({ message, user }) => {
    const { currentUser } = useAuth();
    
    const userId = currentUser?.id;
    const isUserMessage = () => {
        return user.id === userId;
    };
    const getUserIcon = () => {
        return isUserMessage() ? (
            <></>
        ) : (
            <Image
                style={styles.image}
                source={user?.photoURL ? {uri: user?.photoURL} : require('../../../../assets/images/profile-photo-standard.png') }
            />
        );
    };

    return (
        <View style={{ flexDirection: "row" }}>
            {getUserIcon()}
            <View
                style={
                    isUserMessage()
                        ? styles.containerUser
                        : styles.containerSender
                }
            >
                <Text
                    style={[
                        styles.text,
                        {
                            color: isUserMessage()
                                ? Colors.WHITE
                                : Colors.BLACK_TEXT,
                        },
                    ]}
                >
                    {message}
                </Text>
            </View>
        </View>
    );
};
export const styles = StyleSheet.create({
    containerSender: {
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: Colors.PORCELAIN_BACKGROUND,
        marginBottom: 15,
        paddingHorizontal: 15,
        maxWidth: "80%",
        paddingVertical: 20,
        marginRight: 50,
    },
    containerUser: {
        borderBottomLeftRadius: 15,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        backgroundColor: Colors.BLUE,
        marginBottom: 15,
        paddingHorizontal: 10,
        paddingVertical: 15,
        maxWidth: "80%",
        marginLeft: "auto",
        marginRight: 12,
    },
    wrapper: {
        flexDirection: "row",
    },
    text: {
        fontSize: 14,
        fontFamily: "Montserrat_400Regular",
    },
    image: {
        width: 35,
        height: 35,
        marginLeft: 4,
        marginRight: 7,
    },
});

export default Message;
