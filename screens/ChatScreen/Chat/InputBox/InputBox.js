import React, { useState } from "react";
import {
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    TouchableOpacity,
    StyleSheet,
    TextInput,
    View,
} from "react-native";
import Colors from "../../../../constants/Colors";
import SvgUri from "expo-svg-uri";

const InputBox = ({ handleSendMessage, message, setMessage }) => {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : null}
        >
            <SafeAreaView style={styles.container}>
                <View style={styles.wrapper}>
                    <TextInput
                        multiline
                        textAlign={"left"}
                        value={message}
                        onChangeText={(text) => setMessage(text)}
                        placeholder="Wyślij wiadomość..."
                        style={styles.input}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            handleSendMessage(message);
                        }}
                        style={styles.icon}
                    >
                        <SvgUri
                            width={32}
                            height={32}
                            source={require("../../../../assets/icons/send-icon.svg")}
                        />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        </KeyboardAvoidingView>
    );
};

export const styles = StyleSheet.create({
    container: {
        // backgroundColor: Colors.BLACK_BACKGROUND,
        minHeight: 30,
        maxHeight: 150,
        paddingVertical: 2,
        height: "auto",
        marginBottom: 3
    },
    wrapper: {
        flexDirection: "row",
        paddingHorizontal: 12,
        alignItems: "center",
        height: "auto",
    },
    input: {
        backgroundColor: Colors.WHITE,
        width: "100%",
        height: 'auto',
        borderWidth: 1,
        justifyContent: 'center',
        borderRadius: 20,
        borderColor: Colors.GRAY_LINE,
        fontFamily: "Montserrat_400Regular",
        paddingHorizontal: 14,
        paddingTop:  10,
        paddingBottom: 10
    },
    icon: {
        position: "absolute",
        right: 20,
    },
});

export default InputBox;
