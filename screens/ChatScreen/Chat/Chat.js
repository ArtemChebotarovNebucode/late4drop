import React, { useState } from "react";
import { View, FlatList, StyleSheet, ScrollView } from "react-native";
import Message from "./Message/Message";
import Colors from "../../../constants/Colors";

const Chat = ({messages, scroll,setScroll, scrollToEnd}) => {


    React.useEffect(() => {
        if (scroll) {
            setTimeout(() => {
                scrollToEnd()
            }, 1000);
        }
    }, [scroll]);
    return (
        <View style={styles.container}>
            <FlatList
                ref={(ref) => setScroll(ref)}
                showsVerticalScrollIndicator={false}
                data={messages}
                renderItem={({ item }) => (
                    <Message message={item.message} user={{id: item?.userId, name: item?.name, photoURL: item?.photoURL}} />
                )}
                keyExtractor={(message) => message?.createdAt + message.userId}
            />
        </View>
    );
};

export const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        paddingTop: 20,
        backgroundColor: Colors.WHITE,
    },
});

export default Chat;
