import {Dimensions, StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    container: {
        flex: 1,
        paddingTop: 10,
    },

});
