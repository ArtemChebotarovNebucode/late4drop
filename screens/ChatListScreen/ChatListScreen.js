import React from "react";
import {ScrollView, View} from "react-native";
import {styles} from './ChatListScreen.Styles'
import Header from "./Header/Header";
import ChatList from "./ChatList/ChatList";

const ChatListScreen = () => {
    return (
        <View style={styles.wrapper}>
            <Header />
                <View style={styles.container}>
                    <ChatList />
                </View>
        </View>
    )
}

export default ChatListScreen;