import {StyleSheet} from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
    },
    text: {
        fontSize: 18,
        color: Colors.WHITE,
        marginTop: 0,
        marginRight: "auto",
        marginBottom: 0,
        marginLeft: "auto",
        fontFamily: "Montserrat_700Bold",
    },
    screenTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 18,
        color: '#FFFFFF',
    },
});
