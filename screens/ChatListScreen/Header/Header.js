import React from "react";
import {Text, View} from "react-native";
import {styles} from './Header.Styles'
import HeaderBar from "../../../components/partials/HeaderBar/HeaderBar";
import Colors from "../../../constants/Colors";

const Header = () => {
    return (
        <View>
            <HeaderBar backgroundColor={Colors.BLACK_BACKGROUND} barStyle={"light-content"}>
                <View style={styles.wrapper}>

                    <Text style={styles.text}>WIADOMOŚCI</Text>
                </View>
            </HeaderBar>
        </View>
    )
}

export default Header;