import React from "react";
import {
    View,
    StyleSheet,
    Image,
    Text,
    Platform,
    TouchableOpacity,
} from "react-native";
import Colors from "../../../../constants/Colors";
import { useNavigation } from "@react-navigation/core";

const Chat = ({ chat }) => {
    const navigation = useNavigation();

    const getDate = () => {
        if (Platform.OS === "android") {
            const months = [
                "Styczeń",
                "Luty",
                "Marzec",
                "Kwiecień",
                "Maj",
                "Czerwiec",
                "Lipiec",
                "Sierpień",
                "Wrzesień",
                "Październik",
                "Listopad",
                "Grudzień",
            ];
            const date = new Date(chat?.lastMessageDate);
            const day =
                date.getDay() < 10 ? `0${date.getDay()}` : date.getDay();
            const month = months[date.getMonth()].slice(0, 3);
            const year = date.getFullYear();

            return `${day} ${month} ${year}`;
        } else {
            const date = new Date(chat?.lastMessageDate);
            return `${date.getDate()} ${date.toLocaleString("default", {
                month: "short",
            })}`;
        }
    };
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() =>
                navigation.navigate("Chat", {
                    chatId: chat?.id,
                    recipientId: chat?.userId,
                    photoURL: chat?.photoURL,
                    name: chat?.name
                })
            }
        >
            <View style={styles.wrapper}>
                <Image
                    style={styles.image}
                    source={
                        chat.photoURL
                            ? { uri: chat.photoURL }
                            : require("../../../../assets/images/profile-photo-standard.png")
                    }
                />

                <View>
                    <Text numberOfLines={1} style={styles.message}>
                        {chat?.name}
                    </Text>
                    <Text style={styles.link}>{chat?.email}</Text>
                </View>
            </View>
            <Text style={styles.date}>{getDate()}</Text>
        </TouchableOpacity>
    );
};
export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        height: 70,
        justifyContent: "space-between",
        borderRadius: 2,
        alignItems: "center",
        paddingHorizontal: 12,
    },
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
    },
    image: {
        width: 49,
        height: 49,
        marginRight: 10,
        borderRadius: 25,
    },
    message: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 12,
        color: Colors.BLACK_TEXT,
        paddingBottom: 3,
        width: 163,
    },
    link: {
        fontSize: 10,
        fontFamily: "Montserrat_400Regular",
        color: Colors.SANTAS_GRAY_TEXT,
    },
    date: {
        fontSize: 10,
        fontFamily: "Montserrat_400Regular",
        color: Colors.SANTAS_GRAY_TEXT,
    },
});

export default Chat;
