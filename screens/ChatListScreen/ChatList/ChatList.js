import React, {useEffect, useState} from "react";
import { View, FlatList } from "react-native";
import Chat from "./Chat/Chat";
import Colors from "../../../constants/Colors";
import ChatService from "../../../services/chatService";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";

const ChatList = () => {
    const chatService = new ChatService();
    const { userToken, currentUser } = useAuth();
    const [chats, setChats] = useState([])
    const separatorComponent = () => {
        return (
            <View style={{ width: "100%", alignItems: "center" }}>
                <View
                    style={{
                        width: "92%",
                        height: 1,
                        backgroundColor: Colors.GRAY_LINE,
                    }}
                />
            </View>
        );
    };
    useEffect(() => {
        chatService.getUserChats(userToken).then((response) => {
            setChats(response?.data?.data?.filter((item) => item.userId !== currentUser?.id))
        })
    }, [])
    return (
        <View style={{flex: 1}}>
            <FlatList
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                data={chats}
                ItemSeparatorComponent={separatorComponent}
                ListFooterComponent={separatorComponent}
                renderItem={({ item, index }) => <Chat chat={item} />}
                keyExtractor={(chat) => chat?.id.toString()}
            />
        </View>
    );
};

export default ChatList;
