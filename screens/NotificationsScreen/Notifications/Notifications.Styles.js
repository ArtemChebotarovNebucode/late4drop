import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
  sectionName: {
    fontSize: 12,
    fontFamily: "Montserrat_400Regular",
    color: Colors.WATERLOO_GRAY_TEXT,
    marginBottom: 7,
    marginTop: 15,
  },
  wrapper: {
    alignItems: "center",
    justifyContent: "center",
  },
  separator: {
    backgroundColor: Colors.SANTAS_GRAY_LINE,
    width: "90%",
    height: 1,
    opacity: 0.2,
  },
});
