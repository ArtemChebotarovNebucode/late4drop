import React from "react";
import { View, SectionList, Text } from "react-native";
import { styles } from "./Notifications.Styles";
import { notificationList } from "./notificationList";
import Notification from "./Notification/Notification";
import Offsets from "../../../constants/Offsets";

const Notifications = () => {
  const NotificationSeparator = () => {
    return (
      <View style={styles.wrapper}>
        <View style={styles.separator} />
      </View>
    );
  };
  const FooterComponentSeparator = () => {
      return (
          <View style={{height: 20}}/>
      )
  }
  return (
    <View>
      <SectionList
        sections={notificationList}
        keyExtractor={(item) => item.id}
        ListFooterComponent={FooterComponentSeparator}
        ItemSeparatorComponent={NotificationSeparator}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => <Notification notification={item} />}
        renderSectionHeader={({ section: { title } }) => (
          <Text style={styles.sectionName}>{title}</Text>
        )}
      />
    </View>
  );
};

export default Notifications;
