import {StyleSheet} from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        paddingHorizontal: 16,
        paddingVertical: 18,
    },
    info: {
        paddingLeft: 12,
        justifyContent: 'center',
        flexGrow: 1,
        flex: 1,
    },
    name: {
        fontSize: 15,
        fontFamily: 'Montserrat_400Regular',
        color: Colors.BLUE,
        paddingBottom: 10
    },
    text: {
        fontSize: 10,
        fontFamily: 'Montserrat_400Regular',
        color: Colors.WATERLOO_GRAY_TEXT,

    }
});
