import React from "react";
import {View, Text} from "react-native";
import {styles} from './Notification.Styles'
import SvgUri from "expo-svg-uri";

const Notification = ({notification}) => {
    const getIconPath = () =>{
        switch (notification.category){
            case "Like":
                return require('../../../../assets/icons/profile-menu-options/heart-icon.svg')
            case "Comment":
                return require('../../../../assets/icons/profile-menu-options/commas-icon.svg')
            case "Purchase":
                return require('../../../../assets/icons/profile-menu-options/shopping-cart-icon.svg')
            default:
                break;
        }
    }
    return (
        <View style={styles.container}>
            <SvgUri width={24} height={20} source={getIconPath()} />
            <View style={styles.info}>
                <Text style={styles.name}>{notification.name}</Text>
                <Text style={styles.text}>{notification.text}</Text>
            </View>
        </View>
    )
}


export default Notification;