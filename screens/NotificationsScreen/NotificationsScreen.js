import React from "react";
import {View} from "react-native";
import {styles} from './NotificationsScreen.Styles'
import Header from "./Header/Header";
import Notifications from "./Notifications/Notifications";

const NotificationsScreen = () => {
    return (
        <View style={styles.wrapper}>
            <Header />
            <View style={styles.container}>
                <Notifications />
            </View>
        </View>
    )
}

export default NotificationsScreen;