import {Platform, StyleSheet} from "react-native";
import Constants from "expo-constants";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        marginHorizontal: 24,
        paddingTop: 10,
        flex: 1,
    },
});