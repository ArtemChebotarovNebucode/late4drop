import React from "react";
import { Text, View } from "react-native";
import Colors from "../../../constants/Colors";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import { HeaderBar } from "../../../components";
import { styles } from "./Header.Styles";
import { useNavigation } from "@react-navigation/core";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";

const Header = () => {
  const navigation = useNavigation();

  return (
    <View>
      <HeaderBar
        backgroundColor={Colors.BLACK_BACKGROUND}
        barStyle={"light-content"}
      >
        <View style={styles.wrapper}>
          <CircledButton
            iconType={"svg"}
            iconSource={require("../../../assets/icons/black-cross-icon.svg")}
            iconHeight={15}
            iconWidth={15}
            backgroundColor={Colors.GRAY_BACKGROUND}
            fill={Colors.BLUE}
            onPress={() => navigation.goBack()}
          />
          <ImageBlurLoading
            source={require("../../../assets/images/logo-white.png")}
            style={{ width: 180, height: 26 }}
          />
        </View>
      </HeaderBar>
    </View>
  );
};

export default Header;
