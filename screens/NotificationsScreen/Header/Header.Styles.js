import {  StyleSheet , Platform} from "react-native";
import Colors from "../../../constants/Colors";
import Constants from "expo-constants";

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: 'center',
        paddingTop: Platform.OS === 'android' ? Constants.statusBarHeight : 0,
        justifyContent: 'space-between'
    },
    text: {
        fontSize: 14,
        color: Colors.WHITE,
        textAlign: 'center',
        paddingVertical: 12,
        backgroundColor: "#C4C4C4",
        width: 227,
        fontFamily: "Montserrat_400Regular",
        letterSpacing: 11
    }
});
