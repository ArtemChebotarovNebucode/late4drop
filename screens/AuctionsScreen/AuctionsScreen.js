import React, { useEffect, useState } from "react";
import { ScrollView, View, Text, TouchableOpacity } from "react-native";

import Header from "./Header/Header";
import Products from "./Products/Products";

import Colors from "../../constants/Colors";
import { styles } from "./AuctionsScreen.Styles";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Offsets from "../../constants/Offsets";
import UserService from "../../services/userService";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";
import { connect } from "react-redux";
import { setAuctions } from "../../store/actions/productAction";

const AuctionsScreen = (props) => {
    const userService = new UserService();
    const { refreshMyActiveProducts, currentUser, activeProducts } = useAuth();

    const [usersProducts, setUsersProducts] = useState([]);
    const { auctionsItems, setAuctions, navigation, route } = props;

    const { userId, userInfo } = route.params;

    useEffect(() => {
        // userService.getActiveProducts(userId).then((data) => {
        //     setAuctions(data.data.data);
        //     refreshMyActiveProducts(currentUser.id)
        // });
    }, []);

    return (
        <View style={styles.container}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header
                navigation={navigation}
                userId={userId}
                userInfo={userInfo}
                usersProducts
            />
            <View style={{ height: "100%" }}>
                <Products
                    userId={userId}
                    usersProducts={activeProducts}
                    isLoading={false}
                    setPage={(a) => {}}
                    isAuctionScreen={true}
                />
                {/*<View style={{ height: Offsets.BOTTOM_OFFSET }} />*/}
            </View>
        </View>
    );
};

const mapStateToProps = (state) => {
    return {
        auctionsItems: state.product.auctions,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setAuctions: (data) => dispatch(setAuctions(data)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AuctionsScreen);
