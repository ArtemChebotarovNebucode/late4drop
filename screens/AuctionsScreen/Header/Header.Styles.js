import { Platform, StyleSheet } from "react-native";

import Colors from "../../../constants/Colors";
import Constants from "expo-constants";
import Offsets from "../../../constants/Offsets";

export const styles = StyleSheet.create({
    header: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        top: 0,
        marginTop: Offsets.TOP_OFFSET,
        paddingHorizontal: 20,
        paddingBottom: 20,
    },
    screenTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 18,
        color: "#FFFFFF",
    },
});
