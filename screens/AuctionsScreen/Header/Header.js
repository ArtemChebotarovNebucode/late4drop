import React from "react";
import { Text, View } from "react-native";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import SearchField from "../../../components/partials/SearchField/SearchField";

import Colors from "../../../constants/Colors";
import { styles } from "./Header.Styles";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";

const Header = ({ navigation, userId, userInfo, handleOnProductsSearch }) => {
    const { currentUser } = useAuth();

    return (
        <View style={styles.header}>
            <View
                style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 20,
                }}
            >
                <CircledButton
                    iconType={"svg"}
                    iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                    iconHeight={15}
                    iconWidth={15}
                    svgColor={Colors.BLUE}
                    backgroundColor={Colors.GRAY_BACKGROUND}
                    onPress={() => navigation.goBack()}
                    fill={Colors.BLUE}
                />
                <View
                    style={{
                        position: "absolute",
                        alignItems: "center",
                        width: "100%",
                    }}
                >
                    <Text style={styles.screenTitle}>
                        {currentUser?.id === userId ? (
                            "MOJE AUKCJE"
                        ) : userInfo.displayName ? (
                            <Text style={styles.profileName}>
                                {userInfo.displayName}
                            </Text>
                        ) : (
                            <Text style={styles.profileName}>No Name</Text>
                        )}
                    </Text>
                </View>
            </View>
        </View>
    );
};

export default Header;
