import {Dimensions, StyleSheet} from "react-native";
import Colors from "../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    current: {
        width: '70%',
        height: 40,
        backgroundColor: Colors.BLUE,
        bottom: Dimensions.get("window").height / 28,
        left: Dimensions.get("window").width / 2 - Dimensions.get("window").width * 0.35,
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",
        zIndex: 100,
    },
    text: {
        color: Colors.WHITE,
        fontSize: 14,
        fontFamily: 'Montserrat_400Regular',
        textTransform: 'uppercase'
    }
});