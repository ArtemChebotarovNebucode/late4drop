import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    productsContainer: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: 'center',
        flex: 1,
        paddingBottom: 0,
    },
    title: {
        fontFamily: "Montserrat_700Bold",
        marginBottom: 10,
        alignSelf:"center",
    },

});