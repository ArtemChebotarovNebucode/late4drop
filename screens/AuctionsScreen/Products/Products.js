import React, { useState } from "react";
import { ActivityIndicator, FlatList, View, Text, SafeAreaView } from "react-native";

import ProductContainer from "../../../components/partials/ProductContainer/ProductContainer";

import { styles } from "./Products.Styles";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";
import Colors from "../../../constants/Colors";
import { connect } from "react-redux";

const Products = ({
    usersProducts,
    setPage,
    isLoading,
                      isAuctionScreen
}) => {
    const { currentUser, likedProducts } = useAuth();

    const handleEndResearch = () => {
        if (usersProducts?.length >= 40) {
            setPage((prev) => prev + 1);
        }
    };
    const getListFooterComponent = () => {
        return (
            <ActivityIndicator
                animating={isLoading}
                size={"small"}
                color={Colors.BLUE_TEXT}
            />
        );
    };

    return (
        <SafeAreaView style={styles.productsContainer}>
            <FlatList
                style={{ width: "100%", paddingTop: 20 }}
                columnWrapperStyle={{ justifyContent: "space-around" }}
                vertical
                showsVerticalScrollIndicator={false}
                legacyImplementation={false}
                data={usersProducts}
                numColumns={2}
                ListFooterComponent={getListFooterComponent()}
                onEndReached={() => handleEndResearch()}
                renderItem={({ item }) => (
                    <ProductContainer
                        key={item.id}
                        productImg={
                            item?.images?.find((obj) => obj.size === "big").url
                        }
                        productTitle={item.title}
                        productPrice={item?.minPrice === undefined  ? item?.price : item?.minPrice }
                        productOldPrice={item.oldPrice}
                        productSize={item.sizeName}
                        productId={item.id}
                        productImagesCount={item?.images?.length}
                        additionalStyles={{ marginBottom: 10 }}
                        isProductLike={likedProducts?.includes(item.id)}
                        isAuction={item?.minPrice !== undefined}
                        isSold={item?.title === "MEN stone"}
                    />
                )}
                keyExtractor={(product) =>
                    product?.minPrice === undefined
                        ? product?.id?.toString()
                        : product?.id?.toString() + product.minPrice
                }
            />

        </SafeAreaView>
    );
};

export default Products;
