import React from "react";
import { Text, View } from "react-native";

import { styles } from "./Header.Styles";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";
import { useNavigation } from "@react-navigation/native";
import CircledButton from "../../../../components/buttons/CircledButton/CircledButton";
import Colors from "../../../../constants/Colors";
import BlueOutlineButton from "../BlueOutlineButton/BlueOutlineButton";
import { useAuth } from "../../../../contexts/AuthProvider/AuthProvider";
import UserService from "../../../../services/userService";

const Header = ({ userInfo, activeProducts, followedCount, followersCount, openMessageModal }) => {
    const userService = new UserService();

    const navigation = useNavigation();
    const { currentUser, refreshMyInfo, userToken } = useAuth();

    const handleOnFollowUser = async () => {
        if (currentUser?.followedUsersIds.includes(userInfo.id)) {
            await userService.stopFollowingById(userInfo.id);
        } else {
            await userService.startFollowingById(userInfo.id);
        }
        await refreshMyInfo(userToken);
    };

    return (
        <View style={styles.header}>
            <View style={{ marginTop: 20 }}>
                <CircledButton
                    iconType={"svg"}
                    iconSource={require("../../../../assets/icons/black-cross-icon.svg")}
                    iconHeight={15}
                    iconWidth={15}
                    backgroundColor={Colors.GRAY_BACKGROUND}
                    fill={Colors.BLUE}
                    onPress={() => navigation.goBack()}
                />
            </View>
            <View style={styles.profileMainInfo}>
                <View style={{ flex: 1, justifyContent: "center" }}>
                    {userInfo.displayName ? (
                        <Text style={styles.profileName}>
                            {userInfo.displayName}
                        </Text>
                    ) : (
                        <Text style={styles.profileName}>No Name</Text>
                    )}
                    <Text style={styles.profileEmail}>
                        @{userInfo.username}
                    </Text>
                </View>
                <View
                    style={{
                        right: 0,
                        justifyContent: "center",
                        flexDirection: "row",
                    }}
                >
                    <ImageBlurLoading
                        source={
                            userInfo?.photoURL
                                ? { uri: userInfo?.photoURL }
                                : require("../../../../assets/images/profile-photo-standard.png")
                        }
                        style={{ width: 100, height: 100, borderRadius: 50 }}
                    />
                </View>
            </View>

            <View style={styles.profileStats}>
                <View style={{ marginVertical: 10, flexDirection: "row" }}>
                    <View
                        style={{
                            borderColor: "red",
                            flex: 1,
                            flexDirection: "column",
                            alignItems: "center",
                        }}
                    >
                        <Text style={styles.count}>{activeProducts?.length}</Text>
                        <Text style={styles.countTitle}>publikacji</Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "column",
                            alignItems: "center",
                        }}
                    >
                        <Text style={styles.count}>
                            {followersCount}
                        </Text>
                        <Text style={styles.countTitle}>obserwujących</Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "column",
                            alignItems: "center",
                        }}
                    >
                        <Text style={styles.count}>
                            {followedCount}
                        </Text>
                        <Text style={styles.countTitle}>obserwacji</Text>
                    </View>
                </View>

                <BlueOutlineButton
                    icon={
                        !currentUser?.followedUsersIds.includes(userInfo.id)
                            ? require("../../../../assets/icons/blue-heart-icon.svg")
                            : require("../../../../assets/icons/blue-heart-icon-filled.svg")
                    }
                    onPress={handleOnFollowUser}
                >
                    {!currentUser?.followedUsersIds.includes(userInfo.id)
                        ? "Obserwuj"
                        : "Obserwujesz"}
                </BlueOutlineButton>
                <BlueOutlineButton
                    onPress={openMessageModal}
                >
                    Wyślij Wiadomość
                </BlueOutlineButton>
            </View>
        </View>
    );
};

export default Header;
