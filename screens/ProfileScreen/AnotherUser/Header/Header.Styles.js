import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";
import Offsets from "../../../../constants/Offsets";

export const styles = StyleSheet.create({
    header: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        top: 0,
        marginTop: Offsets.TOP_OFFSET,
        paddingHorizontal: 20,
        paddingBottom: 20,
    },
    profileName: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 22,
        color: "#FFFFFF",
        flexWrap: "wrap",
    },
    profileEmail: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 14,
        color: "#FFFFFF",
        flexWrap: "wrap",
    },
    profileMainInfo: {
        flexDirection: "row",
        alignItems: "center",
    },
    profileStats: {
        marginTop: 10,
        backgroundColor: "#FFFFFF",
    },
    gradeText: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 16,
        marginLeft: 15,
    },
    grayMultiFuncTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 12,
        color: Colors.GRAY_TEXT,
    },
    arrowButton: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#E9E9EC",
        borderRadius: 50,
        alignSelf: "flex-start",
        marginLeft: 10,
        padding: 2,
    },
    blackMultiFuncTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 12,
        flexWrap: "wrap",
    },
    blueCircle: {
        backgroundColor: Colors.BLUE,
        borderRadius: 50,
        width: 12,
        height: 12,
        marginLeft: 10,
    },
    myOrders: {
        marginVertical: 30,
        height: 80,
        flexDirection: "row",
        marginHorizontal: 20,
    },
    productImg: {
        resizeMode: ImageResizeMode.cover,
        borderRadius: 5,
        flex: 1,
        aspectRatio: 3/4,
        marginRight: 5,
    },
    productNameUnder: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 12,
        color: Colors.GRAY_TEXT,
    },
    productsSign: {
        flexDirection: "column",
        justifyContent: "center",
        marginLeft: 10,
        flex: 1,
        alignSelf: "center"
    },
    mapBackground: {
        width: "100%",
        height: 86,
    },
    profileStatsFirstSection: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginVertical: 30,
        marginHorizontal: 20,
    },
    profileStatsSecondSection: {
        marginTop: 30,
        flexDirection: "row",
        justifyContent: "space-between",
        marginHorizontal: 20,
    },
    profileStatsThirdSection: {
        borderTopWidth: 1,
        borderColor: "#F1F1F3",
    },
    profileStatsThirdSectionInner: {
        position: "absolute",
        marginHorizontal: 20,
        flexDirection: "row",
        alignItems: "center",
        height: "100%",
        width: "90%",
    },
    navigateButton: {
        backgroundColor: Colors.BLUE,
        paddingVertical: 10,
        paddingHorizontal: 15,
        right: 0,
    },
    count: {
        fontFamily: "Montserrat_700Bold",
        color: "#000"
    },
    countTitle: {
        fontFamily: "Montserrat_400Regular",
        color: "#000"
    }
});
