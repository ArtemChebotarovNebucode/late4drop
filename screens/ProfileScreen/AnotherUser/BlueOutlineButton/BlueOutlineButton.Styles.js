import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    button: {
        paddingHorizontal: 35,
        paddingVertical: 5,
        margin: 10,

        borderWidth: 1,
        borderColor: "#0B6DFF",
        backgroundColor: "#0bc6ff1a",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    },

    icon: {
        marginRight: 5,
    },

    text: {
        color: "#0B6DFF",
        fontFamily: "Montserrat_400Regular",
        textTransform: "uppercase",
    },
});
