import React from 'react';
import { Image, Text, TouchableOpacity } from 'react-native';
import { styles } from './BlueOutlineButton.Styles';
import SvgUri from "expo-svg-uri";

const BlueOutlineButton = (props) => {
	const { children, icon, onPress } = props;
	return (
		<TouchableOpacity style={styles.button} onPress={onPress}>
			{icon && <SvgUri style={styles.icon} source={icon} width={18} height={18} />}
			<Text style={styles.text}>{children}</Text>
		</TouchableOpacity>
	);
};

export default BlueOutlineButton;
