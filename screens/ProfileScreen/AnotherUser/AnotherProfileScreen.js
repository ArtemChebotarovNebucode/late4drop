import React, { useEffect, useState } from "react";
import { ScrollView, View } from "react-native";

import Header from "./Header/Header";
import MenuOption from "./MenuOption/MenuOption";
import StyledStatusBar from "../../../components/partials/StyledStatusBar/StyledStatusBar";

import Colors from "../../../constants/Colors";
import { styles } from "./AnotherProfileScreen.Styles";
import Offsets from "../../../constants/Offsets";
import UserService from "../../../services/userService";
import MessageModal from "../../../components/modals/MessageModal/MessageModal";

const AnotherProfileScreen = ({ navigation, route }) => {
    const userService = new UserService();
    const { userId } = route.params;

    const [userInfo, setUserInfo] = useState();
    const [followedCount, setFollowedCount] = useState();
    const [followersCount, setFollowersCount] = useState();
    const [activeProducts, setActiveProducts] = useState();
    const [isMessageVisible, setIsMessageVisible] = useState(false);

    const closeMessageModal = () => {
        setIsMessageVisible(false);
    };
    const openMessageModal = () => {
        setIsMessageVisible(true);
    };

    useEffect(() => {
        const initial = async () => {
            const usersInfoFetched = await userService.getAnotherUserInfo(
                userId
            );
            const followedCountFetched = await userService.getFollowedUsersCount(
                userId
            );
            const followersCountFetched = await userService.getFollowedByCount(
                userId
            );
            const activeProductsFetched = await userService.getActiveProducts(
                userId
            );

            setUserInfo(usersInfoFetched.data.data);
            setFollowedCount(followedCountFetched.data.data.count);
            setFollowersCount(followersCountFetched.data.data.count);
            setActiveProducts(activeProductsFetched.data.data);
        };
        initial();
    }, []);

    return (
        <View style={styles.container}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />

            <ScrollView showsVerticalScrollIndicator={false}>
                {userInfo ? (
                    <Header
                        userInfo={userInfo}
                        followedCount={followedCount}
                        followersCount={followersCount}
                        activeProducts={activeProducts}
                        openMessageModal={openMessageModal}
                    />
                ) : null}

                <View style={styles.profileMenu}>
                    <View>
                        <MenuOption
                            iconSource={require("../../../assets/icons/profile-menu-options/plus-icon.svg")}
                            optionTitle={"Aukcje użytkownika"}
                            optionDescription={"Wyświetl aukcje"}
                            iconWidth={24}
                            iconHeight={24}
                            onPress={() =>
                                navigation.navigate("Auctions", {
                                    userId: userId,
                                    userInfo: userInfo,
                                })
                            }
                        />

                        <MenuOption
                            iconSource={require("../../../assets/icons/profile-menu-options/commas-icon.svg")}
                            optionTitle={"Opinie użytkownika"}
                            optionDescription={"Przeczytaj swoje opinie"}
                            iconWidth={24}
                            iconHeight={17}
                            onPress={() => navigation.navigate("Opinion", {
                                userId: userId,
                                userInfo: userInfo,
                                isAnotherUser: true
                            })}
                        />
                    </View>
                </View>

                <View style={{ height: Offsets.BOTTOM_OFFSET - 40 }} />
            </ScrollView>
            <MessageModal
                closeModal={closeMessageModal}
                isVisible={isMessageVisible}
                userId={userId}
                imageUrl={userInfo?.photoURL}
                name={userInfo?.displayName}
            />
        </View>
    );
};

export default AnotherProfileScreen;