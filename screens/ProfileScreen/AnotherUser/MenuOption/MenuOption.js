import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import SvgUri from "expo-svg-uri";

import { styles } from "./MenuOption.Styles";

const MenuOption = ({
    iconSource,
    optionTitle,
    optionDescription,
    iconWidth,
    iconHeight,
    onPress,
}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <View style={styles.optionIntro}>
                {iconSource ? (
                    <SvgUri
                        width={iconWidth}
                        height={iconHeight}
                        source={iconSource}
                    />
                ) : null}

                <View style={iconSource ? { marginLeft: 10 } : {}}>
                    <Text style={styles.optionTitle}>{optionTitle}</Text>
                    <Text style={styles.optionDescription}>
                        {optionDescription}
                    </Text>
                </View>
            </View>

            <SvgUri
                width={24}
                height={24}
                source={require("../../../../assets/icons/right-arrow.svg")}
            />
        </TouchableOpacity>
    );
};

export default MenuOption;
