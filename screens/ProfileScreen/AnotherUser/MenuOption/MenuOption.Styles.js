import {StyleSheet} from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 20,
        marginBottom: 10
    },
    optionIntro: {
        flexDirection: "row",
        alignItems: "center",
    },
    optionTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 16
    },
    optionDescription: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 16,
        color: Colors.GRAY_TEXT
    }
});