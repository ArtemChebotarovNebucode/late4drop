import React, { useEffect, useState } from "react";
import { AsyncStorage, ScrollView, View } from "react-native";

import Header from "./Header/Header";
import MenuOption from "./MenuOption/MenuOption";
import StyledStatusBar from "../../../components/partials/StyledStatusBar/StyledStatusBar";

import Colors from "../../../constants/Colors";
import { styles } from "./ProfileScreen.Styles";
import Offsets from "../../../constants/Offsets";
import CustomAlert from "../../../components/modals/CustomAlert/CustomAlert";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import EditProfileModal from "../../../components/modals/EditProfileModal/EditProfileModal";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";
import UserService from "../../../services/userService";

const ProfileScreen = ({ navigation }) => {
    const [isAlertVisible, setIsAlertVisible] = useState(false);
    const [isHelpCalled, setIsHelpCalled] = useState(false);
    const [isEditCalled, setIsEditCalled] = useState(false);

    const { logout, currentUser, activeProducts, userToken } = useAuth();

    const userService = new UserService();

    const [boughtProducts, setBoughtProducts] = useState([]);

    useEffect(() => {
        const initial = async () => {
            await userService
                .getMyBoughtProducts()
                .then((res) => setBoughtProducts(res.data.data));
        };
        initial();
    }, []);

    const handleLogout = () => {
        logout().then(() => {
            AsyncStorage.removeItem("ACCESS_TOKEN").then(() =>
                navigation.replace("Login")
            );
        });
    };

    return (
        <View style={styles.container}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />

            <ScrollView showsVerticalScrollIndicator={false}>
                {currentUser ? (
                    <Header
                        currentUser={currentUser}
                        activeProducts={activeProducts}
                        boughtProducts={boughtProducts}
                    />
                ) : null}

                <View style={styles.profileMenu}>
                    {!isHelpCalled ? (
                        <View>
                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/person-icon.svg")}
                                optionTitle={"Edycja profilu"}
                                optionDescription={"Zmień informacje o sobie"}
                                iconWidth={23}
                                iconHeight={24}
                                onPress={() => setIsEditCalled(true)}
                            />

                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/plus-icon.svg")}
                                optionTitle={"Moje aukcje"}
                                optionDescription={"Wyświetl aukcje"}
                                iconWidth={24}
                                iconHeight={24}
                                onPress={() =>
                                    navigation.navigate("Auctions", {
                                        userId: currentUser?.id,
                                    })
                                }
                            />

                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/commas-icon.svg")}
                                optionTitle={"Opinie"}
                                optionDescription={"Przeczytaj swoje opinie"}
                                iconWidth={24}
                                iconHeight={17}
                                onPress={() =>
                                    navigation.navigate("Opinion", {
                                        userId: currentUser?.id,
                                        userInfo: currentUser,
                                    })
                                }
                            />
                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/cash-pos.svg")}
                                optionTitle={"Moje zamówienia"}
                                optionDescription={"Wyświetl zamówienia"}
                                iconWidth={24}
                                iconHeight={17}
                                onPress={() =>
                                    navigation.navigate("WebView", {
                                        url: `https://late4drop.com/user-profile/${currentUser?.id}/orders`,
                                        isPayments: false,
                                    })
                                }
                            />

                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/heart-icon.svg")}
                                optionTitle={"Ulubione przedmioty"}
                                optionDescription={"Wyświetl"}
                                iconWidth={24}
                                iconHeight={24}
                                onPress={() =>
                                    navigation.navigate("FavouriteProducts")
                                }
                            />

                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/card-icon.svg")}
                                optionTitle={"Sprzedane"}
                                optionDescription={"Wyświetl"}
                                iconWidth={24}
                                iconHeight={24}
                                onPress={() =>
                                    navigation.navigate("SoldProducts")
                                }
                            />

                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/wallet-icon.svg")}
                                optionTitle={"Rozliczenia"}
                                optionDescription={"Wyświetl"}
                                iconWidth={24}
                                iconHeight={24}
                                onPress={() =>
                                    navigation.navigate("Calculations")
                                }
                            />

                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/question-icon.svg")}
                                optionTitle={"Pomoc"}
                                optionDescription={"Wyświetl"}
                                iconWidth={24}
                                iconHeight={24}
                                onPress={() => setIsHelpCalled(true)}
                            />

                            <MenuOption
                                iconSource={require("../../../assets/icons/profile-menu-options/exit-icon.svg")}
                                optionTitle={"Wyloguj się"}
                                optionDescription={"Zmień konto"}
                                iconWidth={24}
                                iconHeight={21.83}
                                onPress={() => setIsAlertVisible(true)}
                            />
                        </View>
                    ) : (
                        <View>
                            <CircledButton
                                iconSource={require("../../../assets/icons/arrow-left.svg")}
                                fill={Colors.BLUE}
                                iconHeight={15}
                                iconType={"svg"}
                                iconWidth={15}
                                backgroundColor={Colors.GRAY_BACKGROUND}
                                additionalStyle={{
                                    width: 30,
                                    height: 30,
                                    marginLeft: 20,
                                    marginBottom: 20,
                                }}
                                onPress={() => setIsHelpCalled(false)}
                            />

                            <MenuOption
                                optionTitle={"FAQ"}
                                optionDescription={"Wyświetl FAQ"}
                                onPress={() => navigation.navigate("WebView", {
                                    url: "https://late4drop.com/faq/",
                                    isPayments: false,
                                })}
                            />

                            <MenuOption
                                optionTitle={"Regulamin"}
                                optionDescription={"Dowiedz się więcej"}
                                onPress={() => navigation.navigate("WebView", {
                                    url: "https://late4drop.com/regulations/",
                                    isPayments: false,
                                })}
                            />

                            <MenuOption
                                optionTitle={"Ochrona kupujących"}
                                optionDescription={"Wyświetl"}
                                onPress={() =>
                                    navigation.navigate("ProtectionBuyers")
                                }
                            />

                            <MenuOption
                                optionTitle={"Ochrona sprzedających"}
                                optionDescription={"Wyświetl"}
                                onPress={() => navigation.navigate("Guarantee")}
                            />
                        </View>
                    )}
                </View>

                <View style={{ height: Offsets.BOTTOM_OFFSET - 40 }} />
                <CustomAlert
                    isVisible={isAlertVisible}
                    message={"Na pewno chcesz się wylogować?"}
                    primaryLabel={"TAK"}
                    secondaryLabel={"NIE"}
                    primaryOnPress={() => {
                        handleLogout();
                        setIsAlertVisible(false);
                    }}
                    secondaryOnPress={() => setIsAlertVisible(false)}
                />
            </ScrollView>

            <EditProfileModal
                isVisible={isEditCalled}
                closeModal={() => setIsEditCalled(false)}
            />
        </View>
    );
};

export default ProfileScreen;
