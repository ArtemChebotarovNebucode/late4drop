import React from "react";
import { ImageBackground, Text, TouchableOpacity, View } from "react-native";
import SvgUri from "expo-svg-uri";

import { styles } from "./Header.Styles";
import { products } from "../../../../data/dummyProductsData";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";
import { useNavigation } from "@react-navigation/native";
import UserService from "../../../../services/userService";

const Header = ({ currentUser, activeProducts, boughtProducts }) => {
    const userService = new UserService();

    const navigation = useNavigation();

    return (
        <View style={styles.header}>
            <View style={styles.profileMainInfo}>
                <View style={{ flex: 1, justifyContent: "center" }}>
                    {currentUser?.displayName ? (
                        <Text style={styles.profileName}>
                            {currentUser?.displayName}
                        </Text>
                    ) : (
                        <Text style={styles.profileName}>No Name</Text>
                    )}
                    <Text style={styles.profileEmail}>{currentUser?.email}</Text>
                </View>
                <View
                    style={{
                        right: 0,
                        justifyContent: "center",
                        flexDirection: "row",
                    }}
                >
                    <ImageBlurLoading
                        source={
                            currentUser?.photoURL
                                ? { uri: currentUser?.photoURL }
                                : require("../../../../assets/images/profile-photo-standard.png")
                        }
                        style={{ width: 100, height: 100, borderRadius: 50 }}
                    />
                </View>
            </View>

            <View style={styles.profileStats}>
                {/*<View style={styles.profileStatsFirstSection}>*/}
                {/*    <View*/}
                {/*        style={{ flexDirection: "row", alignItems: "center" }}*/}
                {/*    >*/}
                {/*        <SvgUri*/}
                {/*            width={22}*/}
                {/*            height={22}*/}
                {/*            source={require("../../../../assets/icons/cup-icon.svg")}*/}
                {/*        />*/}

                {/*        <Text style={styles.gradeText}>*/}
                {/*            {currentUser.numberOfOpinions === 0 ? "0" : "TODO"}*/}
                {/*        </Text>*/}
                {/*    </View>*/}

                {/*    <TouchableOpacity*/}
                {/*        style={{ flexDirection: "row" }}*/}
                {/*        onPress={() => navigation.navigate("ChatList")}*/}
                {/*    >*/}
                {/*        <Text style={styles.grayMultiFuncTitle}>*/}
                {/*            MESSAGES*/}
                {/*        </Text>*/}
                {/*        <View style={styles.arrowButton}>*/}
                {/*            <SvgUri*/}
                {/*                width={12}*/}
                {/*                height={12}*/}
                {/*                source={require("../../../../assets/icons/right-arrow.svg")}*/}
                {/*            />*/}
                {/*        </View>*/}
                {/*    </TouchableOpacity>*/}
                {/*</View>*/}
                <View style={{ marginTop: 10, flexDirection: "row" }}>
                    <View
                        style={{
                            borderColor: "red",
                            flex: 1,
                            flexDirection: "column",
                            alignItems: "center",
                        }}
                    >
                        <Text style={styles.count}>
                            {activeProducts?.length}
                        </Text>
                        <Text style={styles.countTitle}>publikacji</Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "column",
                            alignItems: "center",
                        }}
                    >
                        <Text style={styles.count}>
                            {currentUser?.followersIds.length}
                        </Text>
                        <Text style={styles.countTitle}>obserwujących</Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "column",
                            alignItems: "center",
                        }}
                    >
                        <Text style={styles.count}>
                            {currentUser?.followedUsersIds.length}
                        </Text>
                        <Text style={styles.countTitle}>obserwacji</Text>
                    </View>
                </View>

                {boughtProducts.length !== 0 ? (
                    <View>
                        {/*<View style={styles.profileStatsSecondSection}>*/}
                        {/*    <Text style={styles.blackMultiFuncTitle}>*/}
                        {/*        MOJE ZAMÓWIENIA*/}
                        {/*    </Text>*/}

                        {/*    <TouchableOpacity*/}
                        {/*        style={{*/}
                        {/*            flexDirection: "row",*/}
                        {/*            alignItems: "center",*/}
                        {/*        }}*/}
                        {/*        onPress={() => navigation.navigate("Tracking")}*/}
                        {/*    >*/}
                        {/*        <Text style={styles.grayMultiFuncTitle}>*/}
                        {/*            REALIZACJA*/}
                        {/*        </Text>*/}

                        {/*        <View style={styles.blueCircle} />*/}
                        {/*    </TouchableOpacity>*/}
                        {/*</View>*/}

                        <TouchableOpacity
                            style={styles.myOrders}
                            onPress={() => navigation.navigate("MyOrders")}
                        >
                            <View
                                style={{
                                    flexDirection: "row",
                                    flex: 1,
                                    alignSelf: "center",
                                }}
                            >
                                {products.map((product, index) => {
                                    return index < 3 ? (
                                        <ImageBlurLoading
                                            key={product.id}
                                            source={product.img}
                                            style={
                                                index === 2
                                                    ? {
                                                          ...styles.productImg,
                                                          marginRight: 0,
                                                      }
                                                    : styles.productImg
                                            }
                                        />
                                    ) : null;
                                })}
                            </View>

                            <View style={styles.productsSign}>
                                <Text style={styles.blackMultiFuncTitle}>
                                    {products[0].name}
                                </Text>
                                <Text style={styles.productNameUnder}>
                                    i {products.length - 3} inne przedmioty
                                </Text>
                            </View>
                        </TouchableOpacity>

                        {/*<View style={styles.profileStatsThirdSection}>*/}
                        {/*    <ImageBackground*/}
                        {/*        imageStyle={{*/}
                        {/*            borderBottomLeftRadius: 15,*/}
                        {/*            borderBottomRightRadius: 15,*/}
                        {/*        }}*/}
                        {/*        style={styles.mapBackground}*/}
                        {/*        source={require("../../../../assets/images/map.png")}*/}
                        {/*    />*/}
                        {/*    <View style={styles.profileStatsThirdSectionInner}>*/}
                        {/*        <View*/}
                        {/*            style={{*/}
                        {/*                flexDirection: "row",*/}
                        {/*                alignItems: "center",*/}
                        {/*                flex: 2,*/}
                        {/*            }}*/}
                        {/*        >*/}
                        {/*            <SvgUri*/}
                        {/*                width={22}*/}
                        {/*                height={22}*/}
                        {/*                source={require("../../../../assets/icons/pack-icon.svg")}*/}
                        {/*            />*/}
                        
                        {/*            <View style={{ marginLeft: 20, flex: 1 }}>*/}
                        {/*                <Text*/}
                        {/*                    style={styles.blackMultiFuncTitle}*/}
                        {/*                >*/}
                        {/*                    Paczka w drodze!*/}
                        {/*                </Text>*/}
                        {/*                <Text*/}
                        {/*                    style={{*/}
                        {/*                        ...styles.productNameUnder,*/}
                        {/*                        fontSize: 14,*/}
                        {/*                        flexWrap: "wrap",*/}
                        {/*                    }}*/}
                        {/*                >*/}
                        {/*                    Przyjedzie za 2 dni*/}
                        {/*                </Text>*/}
                        {/*            </View>*/}
                        {/*        </View>*/}
                        
                        {/*        <View*/}
                        {/*            style={{*/}
                        {/*                flexDirection: "row",*/}
                        {/*                alignContent: "center",*/}
                        {/*                flex: 1,*/}
                        {/*                justifyContent: "flex-end",*/}
                        {/*            }}*/}
                        {/*        >*/}
                        {/*            <TouchableOpacity*/}
                        {/*                style={styles.navigateButton}*/}
                        {/*                onPress={() =>*/}
                        {/*                    navigation.navigate("Tracking")*/}
                        {/*                }*/}
                        {/*            >*/}
                        {/*                <Text*/}
                        {/*                    style={{*/}
                        {/*                        ...styles.productNameUnder,*/}
                        {/*                        color: "#FFFFFF",*/}
                        {/*                    }}*/}
                        {/*                >*/}
                        {/*                    Nawiguj*/}
                        {/*                </Text>*/}
                        {/*            </TouchableOpacity>*/}
                        {/*        </View>*/}
                        {/*    </View>*/}
                        {/*</View>*/}
                    </View>
                ) : (
                    <View style={{ height: 10 }} />
                )}
            </View>
        </View>
    );
};

export default Header;
