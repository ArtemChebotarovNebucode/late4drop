import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	container: {
		marginTop: 20,
	},
	vertical: {
		display: 'flex',
		justifyContent: 'space-between',
		flexDirection: 'row',
		marginBottom: 5,
	},
	text: {
		fontFamily: 'Montserrat_500Medium',
		fontSize: 14,
	},
});
