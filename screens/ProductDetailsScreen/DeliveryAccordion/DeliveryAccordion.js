import React from "react";
import { Text, View } from "react-native";
import UserInfo from "../UserInfo/UserInfo";
import { styles } from "./DeliveryAccordion.Styles";

const DeliveryAccordion = ({
    deliveryData,
    userAvatar,
    userName,
    userRating,
    userOpinions,
    userId,
}) => {
    return (
        <View>
            <View style={styles.container}>
                <View style={styles.vertical}>
                    <Text style={styles.text}>Polska</Text>
                    <Text style={styles.text}>{deliveryData.poland} zł</Text>
                </View>
                <View style={styles.vertical}>
                    <Text style={styles.text}>Europa</Text>
                    <Text style={styles.text}>{deliveryData.europe} zł</Text>
                </View>
            </View>
            <UserInfo
                userAvatar={userAvatar}
                userName={userName}
                userRating={userRating}
                userOpinions={userOpinions}
                userId={userId}
            />
        </View>
    );
};

export default DeliveryAccordion;
