import React from 'react';
import { Image, Text, TouchableOpacity } from 'react-native';
import { styles } from './TertiaryButton.Styles';
import SvgUri from "expo-svg-uri";

const TertiaryButton = ({ children, icon, iconWidth, iconHeight, onPress }) => {
	return (
		<TouchableOpacity style={styles.button} onPress={onPress}>
			<SvgUri source={icon} width={iconWidth} height={iconHeight} style={styles.icon} />
			<Text style={styles.text}>{children}</Text>
		</TouchableOpacity>
	);
};

export default TertiaryButton;
