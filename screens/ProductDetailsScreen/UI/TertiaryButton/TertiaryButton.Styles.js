import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	button: {
		borderColor: '#DCE0EE',
		flex: 1,
		alignItems: 'center',
		justifyContent: 'flex-start',
		marginBottom: 17,
		borderWidth: 1,
		paddingHorizontal: 7,
		flexDirection: 'row',
		minHeight: 48,
	},
	icon: {
		marginRight: 8,
	},
	text: {
		color: '#212123',
		fontFamily: 'Montserrat_500Medium',
		fontSize: 14,
	},
});
