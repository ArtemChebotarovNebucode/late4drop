import { StyleSheet } from 'react-native';
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
	button: {
		borderColor: Colors.BLUE,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 17,
		borderWidth: 1,
		minHeight: 48,
	},
	text: {
		color: Colors.BLUE,
		fontFamily: 'Montserrat_400Regular',
		fontSize: 14,
		textTransform: 'uppercase',
	},
});
