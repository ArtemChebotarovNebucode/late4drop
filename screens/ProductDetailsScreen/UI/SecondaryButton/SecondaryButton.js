import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './SecondaryButton.Styles';

const SecondaryButton = ({ onPress, children }) => {
	return (
		<TouchableOpacity style={styles.button} onPress={onPress}>
			<Text style={styles.text}>{children}</Text>
		</TouchableOpacity>
	);
};

export default SecondaryButton;
