import { StyleSheet } from 'react-native';
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
	button: {
		backgroundColor: Colors.BLUE,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 17,
		minHeight: 48,
	},
	text: {
		color: 'white',
		fontFamily: 'Montserrat_400Regular',
		fontSize: 14,
		textTransform: 'uppercase',
	},
});
