import React from 'react';
import { Text } from 'react-native';
import { styles } from './PriceText.Styles';

const PriceText = ({ children, original }) => {
	if (original) {
		return <Text style={styles.original}>{children} zł</Text>;
	} else {
		return <Text style={styles.current}>{children} zł</Text>;
	}
};

export default PriceText;
