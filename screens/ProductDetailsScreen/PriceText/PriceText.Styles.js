import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	current: {
		fontFamily: 'Montserrat_400Regular',
		fontSize: 18,
		color: '#212123',
		marginRight: 23,
	},

	original: {
		fontFamily: 'Montserrat_400Regular',
		fontSize: 18,
		color: '#5E5E68',
		textDecorationLine: 'line-through',
		textDecorationColor: '#5E5E68',
	},
});
