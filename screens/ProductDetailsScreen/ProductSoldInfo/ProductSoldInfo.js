import React from 'react';
import { Text, View } from 'react-native';
import { styles } from './ProductSoldInfo.Styles';

const ProductSoldInfo = () => {
	return (
		<View style={styles.container}>
			<Text style={styles.text}>Przedmiot sprzedany!</Text>
		</View>
	);
};

export default ProductSoldInfo;
