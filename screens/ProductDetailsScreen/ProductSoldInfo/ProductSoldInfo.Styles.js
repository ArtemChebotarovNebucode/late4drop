import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	container: {
		borderWidth: 1,
		borderColor: '#dce0ee',
		paddingVertical: 15,
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 15,
	},
	text: {
		fontSize: 14,
		fontFamily: 'Montserrat_400Regular',
		textTransform: 'uppercase',
	},
});
