import React from 'react';
import { Text, View } from 'react-native';
import { styles } from './PinkBadge.Styles';

const PinkBadge = () => {
	return (
		<View style={styles.container}>
			<Text style={styles.textBig}>Wybór</Text>
			<Text style={styles.text}> Late4Drop</Text>
		</View>
	);
};

export default PinkBadge;
