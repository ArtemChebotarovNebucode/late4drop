import { StyleSheet } from 'react-native';
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.BLUE,
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row',
		alignSelf: 'flex-start',
		paddingVertical: 5,
		paddingHorizontal: 5,
		borderRadius: 5,
		marginBottom: 15,
	},

	textBig: {
		color: 'white',
		textTransform: 'uppercase',
		fontFamily: 'Montserrat_500Medium',
		fontSize: 12,
	},
	text: {
		color: 'white',
		fontFamily: 'Montserrat_500Medium',
		fontSize: 12,
	},
});
