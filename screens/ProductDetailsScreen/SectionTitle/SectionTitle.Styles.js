import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	title: {
		fontFamily: 'Montserrat_700Bold',
		fontSize: 15,
		textTransform: 'uppercase',
		marginBottom: 20,
	},
});
