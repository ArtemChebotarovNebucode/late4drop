import React from 'react';
import { Text } from 'react-native';
import { styles } from './SectionTitle.Styles';

const SectionTitle = (props) => {
	const { children } = props;
	return (
		<Text style={styles.title} {...props}>
			{children}
		</Text>
	);
};

export default SectionTitle;
