import React from "react";
import { Text, View } from "react-native";
import { styles } from "./ProductTag.Styles";

const ProductTag = ({ title, condition, color, borderColor, textColor }) => {
    return (
        <View
            style={
                borderColor
                    ? { ...styles.productTag, borderColor: borderColor }
                    : styles.productTag
            }
        >
            <Text
                style={
                    textColor
                        ? { ...styles.text, color: textColor }
                        : styles.text
                }
            >
                {condition}
                {color}
                {title}
            </Text>
        </View>
    );
};

export default ProductTag;
