import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	productTag: {
		paddingVertical: 8,
		paddingHorizontal: 9,
		borderWidth: 1,
		borderColor: '#5E5E68',
		alignSelf: 'flex-start',
		borderRadius: 4,
		marginRight: 10,
	},

	text: {
		fontFamily: 'Montserrat_500Medium',
		fontSize: 14,
		color: '#5E5E68',
	},
});
