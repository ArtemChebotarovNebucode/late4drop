import React, { useRef } from "react";

import { Dimensions, FlatList, TouchableOpacity, View } from "react-native";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";

const ProductCarousel = (props) => {
    const { images } = props;
    const { width: windowWidth, height: windowHeight } = Dimensions.get(
        "window"
    );

    const refContainer = useRef(null);

    const handleScroll = (data) => {
        const index = images?.findIndex((item) => item.id === data.id);
        refContainer.current.scrollToIndex({ animated: true, index });
    };

    const Slide = ({ data }) => (
        <View
            key={data.id}
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: windowHeight * 0.45,
                width: windowWidth - 40,
                minHeight: 330,
            }}
        >
            <ImageBlurLoading
                source={{uri: data.url}}
                style={{ width: windowWidth - 60, flex: 1 }}
            />
        </View>
    );
    const SlideSmall = ({ data }) => (
        <TouchableOpacity
            key={data.id}
            style={{ position: "relative" }}
            onPress={() => handleScroll(data)}
        >
            <ImageBlurLoading
                source={{uri: data.url}}
                style={{
                    resizeMode: ImageResizeMode.contain,
                    width: windowWidth / 4,
                    height: windowWidth / 4,
                    marginRight: 10,
                }}
            />
        </TouchableOpacity>
    );
    return (
        <View style={{ marginVertical: 12, flex: 1 }}>
            <FlatList
                ref={refContainer}
                data={images}
                renderItem={({ item }) => {
                    return <Slide data={item} />;
                }}
                pagingEnabled={true}
                horizontal
                keyExtractor={(item) => item.id.toString()}
                showsHorizontalScrollIndicator={false}
            />

            <FlatList
                style={{ marginTop: 12, paddingBottom: 10 }}
                data={images}
                renderItem={({ item }) => {
                    return <SlideSmall data={item} />;
                }}
                horizontal
                keyExtractor={(item) => item.id.toString()}
            />
        </View>
    );
};

export default ProductCarousel;
