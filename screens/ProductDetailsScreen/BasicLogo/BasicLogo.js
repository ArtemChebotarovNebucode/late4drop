import React from "react";
import { View } from "react-native";
import { styles } from "./BasicLogo.Styles";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";

const BasicLogo = (props) => {
    return (
        <View style={styles.wrapper}>
            <ImageBlurLoading
                source={require("../../../assets/images/logo-white.png")}
                style={{ width: 180, height: 26 }}
            />
        </View>
    );
};

export default BasicLogo;
