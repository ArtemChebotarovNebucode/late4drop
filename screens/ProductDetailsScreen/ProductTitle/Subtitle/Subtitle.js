import React from 'react';
import { Text } from 'react-native';
import { styles } from './Subtitle.Styles';

const Subtitle = (props) => {
	return <Text style={styles.subtitle}>{props.children}</Text>;
};

export default Subtitle;
