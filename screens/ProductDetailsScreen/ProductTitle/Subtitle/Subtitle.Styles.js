import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	subtitle: {
		fontFamily: 'Montserrat_700Bold',
		fontSize: 18,
		marginBottom: 15,
	},
});
