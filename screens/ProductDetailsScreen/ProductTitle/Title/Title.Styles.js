import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	title: {
		fontFamily: 'Montserrat_400Regular',
		fontSize: 18,
	},
});
