import React, { useRef, useState } from "react";

import {
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Modal,
} from "react-native";
import DeliveryAccordion from "./DeliveryAccordion/DeliveryAccordion";
import Description from "./Description/Description";
import PinkBadge from "./PinkBadge/PinkBadge";
import PriceText from "./PriceText/PriceText";
import ProductCarousel from "./ProductCarousel/ProductCarousel";
import ProductDetailsHeader from "./ProductDetailsHeader/ProductDetailsHeader";
import ProductSoldInfo from "./ProductSoldInfo/ProductSoldInfo";
import ProductTag from "./ProductTag/ProductTag";
import Subtitle from "./ProductTitle/Subtitle/Subtitle";
import Title from "./ProductTitle/Title/Title";
import SectionTitle from "./SectionTitle/SectionTitle";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import PrimaryButton from "./UI/PrimaryButton/PrimaryButton";
import SecondaryButton from "./UI/SecondaryButton/SecondaryButton";
import TertiaryButton from "./UI/TertiaryButton/TertiaryButton";

import { product } from "../../data/productDataExample";
import Colors from "../../constants/Colors";
import BasketModal from "../../components/modals/BasketModal/BasketModal";
import Auction from "../../components/modals/AuctionModal/Auction";
import CustomAlert from "../../components/modals/CustomAlert/CustomAlert";
import BuyNowModal from "../../components/modals/BuyNowModal/BuyNowModal";
import { useNavigation } from "@react-navigation/core";
import ProductService from "../../services/productService";
import SvgUri from "expo-svg-uri";
import UserService from "../../services/userService";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";

import { connect } from "react-redux";
import {
    addLastSearchingProducts,
    deleteProduct,
    deleteAuction
} from "../../store/actions/productAction";
import AuctionService from "../../services/auctionService";
import Color from "../../components/modals/FiltersModal/Color/Color";
import { colors } from "../../data/dummyColorsData";

const ProductDetailsScreen = ({
    route,
    addLastSearchingProducts,
    deleteProduct,
                                  deleteAuctionItems,
}) => {
    const productService = new ProductService();
    const userService = new UserService();
    const auctionService = new AuctionService();

    const [testProduct, setTestProduct] = useState(null);
    const [images, setImages] = useState([]);
    const [bankUrl, setBankUrl] = useState("https://tpay.com/");
    const { type, productId, isJustCreated } = route.params;
    const { userToken, currentUser, refreshMyActiveProducts, handleLikeProduct, likedProducts } = useAuth();

    const [isVisibleBuyNow, setIsVisibleBuyNow] = useState(false);
    const [isVisibleBasket, setIsVisibleBasket] = useState(false);
    const [isVisibleAuction, setIsVisibleAuction] = useState(false);
    const [isVerificationChecked, setIsVerificationChecked] = useState(false);

    const [isCongratsAlertOpen, setIsCongratsAlertOpen] = useState(false);
    const [isLiked, setIsLiked] = useState(false);

    const openAuctionModal = () => {
        setIsVisibleAuction(true);
    };
    const closeAuctionModal = () => {
        setIsVisibleAuction(false);
    };
    const openBasketModalModal = (verificationChecked) => {
        setIsVisibleBasket(true);
        setIsVerificationChecked(verificationChecked);
    };
    const closeBasketModal = () => {
        setIsVisibleBasket(false);
        setIsVisibleBuyNow(true)
    };
    const openBuyNowModal = () => {
        setIsVisibleBuyNow(true);
    };
    const closeBuyNowModal = () => {
        setIsVisibleBuyNow(false);
    };
    const handleLiked = (like) => {
        setIsLiked(like);
        if (like) {
            userService.startLikingById(userToken, testProduct?.id).then(() => handleLikeProduct(testProduct?.id));
        } else {
            userService
                .stopLikingById(userToken, testProduct?.id)
                .then((result) => {handleLikeProduct(testProduct?.id)});
        }
   };
    const scrollViewRef = useRef(null);

    const handleDeleteProduct = () => {
        productService
            .deleteProduct(testProduct?.id, userToken)
            .then((response) => {
                if (response.data.success) {
                    deleteProduct(response.data.data);
                    // deleteAuctionItems(response.data.data);
                    refreshMyActiveProducts(currentUser.id)
                    navigation.goBack();
                }
            });
        userService
            .stopLikingById(userToken, testProduct?.id)
            .then(() => {})
        handleLikeProduct(testProduct?.id)
    };
    const getColor = () => {
        const colorName = testProduct?.categories[2]?.split(" ")[1];
        const color = colors.find((item) => item.name === colorName);
        return color ? color.color : "#0066cc";
    };
    const navigation = useNavigation();
    const getAuctionAndTime = () => {
        const dayNow = new Date();
        const dateNowTemp = new Date();
        const date = new Date(dateNowTemp.setDate(dateNowTemp.getDate() + 7));
        const distance = date.getTime() - dayNow.getTime();
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor(
            (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

        return (
            <Text
                style={{
                    fontSize: 12,
                    color: Colors.WHITE,
                    fontFamily: "Montserrat_500Medium",
                    paddingVertical: 5,
                    paddingHorizontal: 5,
                }}
            >
                {days}d {hours}h {minutes}m
            </Text>
        );
    };
    React.useEffect(() => {
        setIsLiked(
            likedProducts?.includes(productId)
        );
        if (type === "BUY_NOW") {
            productService.getProductById(productId).then((response) => {
                setTestProduct(response.data.data);
                addLastSearchingProducts(response.data.data);
            });
        } else if (type === "AUCTION") {
            auctionService.getAuctionById(productId).then((response) => {
                setTestProduct(response.data.data);
                if (response.data.data !== undefined)
                    addLastSearchingProducts(response.data.data);
            });
        }

        return setTestProduct(null);
    }, []);
    console.log(testProduct)

    return (
        <View style={{ flex: 1 }}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <ProductDetailsHeader isJustCreated={isJustCreated || false} />
            <ScrollView
                style={{
                    paddingHorizontal: 20,
                    flex: 1,
                    paddingTop: 20,
                }}
                ref={scrollViewRef}
                showsVerticalScrollIndicator={false}
            >
                {testProduct?.sold && <ProductSoldInfo />}
                <ProductCarousel
                    images={
                        testProduct?.images
                            ? testProduct?.images.filter(
                                  (item) => item.size === "org"
                              )
                            : []
                    }
                />
                <View style={{ flexDirection: "row" }}>
                    {testProduct?.isSkrrChoice && <PinkBadge />}
                    {type === "AUCTION" ? (
                        <View
                            style={{
                                width: 126,
                                height: 25,
                                backgroundColor: Colors.RED_BACKGROUND,
                                borderRadius: 4,
                                alignItems: "center",
                                justifyContent: "center",
                                marginLeft: 5,
                            }}
                        >
                            {getAuctionAndTime()}
                        </View>
                    ) : (
                        <></>
                    )}
                </View>

                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                    }}
                >
                    <View>
                        <Title>{testProduct?.title ?? ""}</Title>
                        <Subtitle>{testProduct?.description ?? ""}</Subtitle>
                    </View>

                    <TouchableOpacity onPress={() => handleLiked(!isLiked)}>
                        <SvgUri
                            source={
                                !isLiked
                                    ? require("../../assets/icons/blue-heart-icon.svg")
                                    : require("../../assets/icons/blue-heart-icon-filled.svg")
                            }
                            height={22}
                            width={22}
                        />
                    </TouchableOpacity>
                </View>

                <View
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 28,
                    }}
                >
                    {testProduct?.categories && testProduct?.categories[0] && (
                        <ProductTag title={testProduct?.categories[0]} />
                    )}
                    {testProduct?.categories && testProduct?.categories[1] && (
                        <ProductTag
                            title={testProduct?.categories[1]}
                            condition={true}
                        />
                    )}
                    {testProduct?.categories && testProduct?.categories[2] && (
                        // <ProductTag
                        //     title={testProduct?.categories[2]}
                        //     color={true}
                        //     borderColor={"#ffe900"}
                        //     textColor={"#b300ff"}
                        // />
                        <Color
                            color={getColor()}
                            onPress={() => {}}
                            isActive={false}
                            borderNeeded={"#2558E4"}
                            additionStyles={{ width: 35, height: 35 }}
                        />
                    )}
                </View>
                <View
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 20,
                    }}
                >
                    {testProduct?.price !== undefined ? (
                        <PriceText>
                            {parseInt(testProduct?.price ?? "0")}
                        </PriceText>
                    ) : (
                        <></>
                    )}

                    {testProduct?.oldPrice !== "0.00" &&
                        testProduct?.oldPrice !== undefined && (
                            <PriceText original={true}>
                                {parseInt(testProduct?.oldPrice ?? "0")}
                            </PriceText>
                        )}
                </View>
                {!testProduct?.sold && (
                    <>
                        {currentUser?.id !== testProduct?.userId ? (
                            type === "BUY_NOW" ? (
                                <PrimaryButton
                                    onPress={() => openBuyNowModal()}
                                >
                                    Kup
                                </PrimaryButton>
                            ) : type === "AUCTION" ? (
                                <SecondaryButton
                                    onPress={() => openAuctionModal()}
                                >
                                    Złóż ofertę
                                </SecondaryButton>
                            ) : null
                        ) : (
                            <PrimaryButton
                                onPress={() => handleDeleteProduct()}
                            >
                                Usuń
                            </PrimaryButton>
                        )}

                        <TertiaryButton
                            icon={require("../../assets/icons/shield-icon.svg")}
                            iconWidth={28}
                            iconHeight={23}
                            onPress={() =>
                                navigation.navigate("ProtectionBuyers")
                            }
                        >
                            Możliwa gwarancja kupującego
                        </TertiaryButton>
                    </>
                )}

                <SectionTitle>Opis</SectionTitle>
                <Description>{testProduct?.description}</Description>
                <View style={{ marginBottom: 40 }}>
                    <View
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "center",
                            paddingVertical: 5,
                        }}
                    >
                        <SectionTitle
                            style={{
                                marginBottom: 0,
                                fontFamily: "Montserrat_700Bold",
                                fontSize: 15,
                                textTransform: "uppercase",
                            }}
                        >
                            Dostawa
                        </SectionTitle>
                    </View>
                    <DeliveryAccordion
                        deliveryData={{
                            poland: testProduct?.shipmentToPoland,
                            europe: testProduct?.shipmentToEurope,
                        }}
                        userAvatar={testProduct?.userPhotoURL}
                        userName={testProduct?.userDisplayName}
                        userId={testProduct?.userId}
                        userRating={0}
                        userOpinions={currentUser?.opinions}
                    />
                </View>
                <CustomAlert
                    isVisible={isCongratsAlertOpen}
                    primaryLabel={"ŚLEDŹ PACZKĘ"}
                    secondaryLabel={"WRÓĆ DO AUKCJI"}
                    title={"GRATULACJE"}
                    message={
                        <Text>
                            Przedmiot{" "}
                            <Text style={{ fontFamily: "Montserrat_700Bold" }}>
                                {product.title}
                            </Text>{" "}
                            został przez Ciebie zakupiony od użytkownika{" "}
                            <Text style={{ fontFamily: "Montserrat_700Bold" }}>
                                {product.user_name}
                            </Text>
                            .
                        </Text>
                    }
                    primaryOnPress={() => {
                        setIsCongratsAlertOpen(false);
                        navigation.navigate("Tracking");
                    }}
                    secondaryOnPress={() => setIsCongratsAlertOpen(false)}
                />
            </ScrollView>
            <BuyNowModal
                closeModal={closeBuyNowModal}
                isVisible={isVisibleBuyNow}
                openBasketModal={openBasketModalModal}
                productId={productId}
                setBankUrl={setBankUrl}
            />
            <BasketModal
                closeModal={closeBasketModal}
                isVisible={isVisibleBasket}
                product={testProduct}
                setIsCongratsAlertOpen={setIsCongratsAlertOpen}
                isVerificationChecked={isVerificationChecked}
                bankUrl={bankUrl}
            />
            <Auction
                closeModal={closeAuctionModal}
                isVisible={isVisibleAuction}
                price={product.current_price}
            />
        </View>
    );
};
const mapDispatchToProps = (dispatch) => {
    return {
        addLastSearchingProducts: (data) =>
            dispatch(addLastSearchingProducts(data)),
        deleteAuctionItems: (data) => dispatch(deleteAuction(data)),
        deleteProduct: (data) => dispatch(deleteProduct(data)),
    };
};

export default connect(null, mapDispatchToProps)(ProductDetailsScreen);
