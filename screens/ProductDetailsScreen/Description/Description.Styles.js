import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	description: {
		fontFamily: 'Montserrat_500Medium',
		fontSize: 14,
		color: '#8F92A1',
		marginBottom: 25,
	},
});
