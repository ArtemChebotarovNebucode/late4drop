import React from 'react';
import { Text } from 'react-native';
import { styles } from './Description.Styles';

const Description = ({ children }) => {
	return <Text style={styles.description}>{children}</Text>;
};

export default Description;
