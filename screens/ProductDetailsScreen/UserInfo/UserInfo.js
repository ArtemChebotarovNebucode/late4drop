import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { styles } from "./UserInfo.Styles";
import StarRating from "react-native-star-rating";
import BlueOutlineButton from "../../ProfileScreen/AnotherUser/BlueOutlineButton/BlueOutlineButton";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";
import UserService from "../../../services/userService";
import { useNavigation } from "@react-navigation/native";

const UserInfo = (props) => {
    const navigation = useNavigation();

    const { currentUser } = useAuth();

    const { userAvatar, userName, userRating, userOpinions, userId } = props;

    return (
        <View style={styles.container}>
            {currentUser?.id !== userId ? (
                <TouchableOpacity
                    onPress={() =>
                        navigation.navigate("AnotherUser", { userId: userId })
                    }
                >
                    <Image source={userAvatar ? { uri: userAvatar } : require("../../../assets/images/profile-photo-standard.png")} style={styles.image} />
                </TouchableOpacity>
            ) : (
                <Image source={userAvatar ? { uri: userAvatar } : require("../../../assets/images/profile-photo-standard.png")} style={styles.image} />
            )}

            <View>
                <Text style={styles.text}>{userName ? userName : "No Name"}</Text>
                <View style={styles.ratingContainer}>
                    <StarRating
                        disabled={true}
                        maxStars={5}
                        rating={userRating}
                        fullStarColor="#FEC247"
                        starSize={20}
                        containerStyle={{
                            alignSelf: "flex-start",
                        }}
                        starStyle={{
                            marginRight: 2,
                        }}
                    />
                    <Text style={styles.rating}>
                        {(Math.round(userRating * 10) / 10).toFixed(1)}
                    </Text>
                    <Text style={styles.opinions}>
                        ({userOpinions.length} opinii)
                    </Text>
                </View>
            </View>
        </View>
    );
};

export default UserInfo;
