import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "row",
        marginTop: 26,
    },
    text: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 14,
        color: "#212123",
    },

    ratingContainer: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 6,
        marginBottom: 15,
    },
    rating: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 12,
        color: "#5E5E68",
        marginHorizontal: 5,
    },
    opinions: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 12,
        color: "#8C8C95",
    },
    image: {
        marginRight: 20,
        width: 70,
        height: 70,
		borderRadius: 35
    },
});
