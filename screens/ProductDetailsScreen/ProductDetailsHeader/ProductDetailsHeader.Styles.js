import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";
import Offsets from "../../../constants/Offsets";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        top: 0,
        marginTop: Offsets.TOP_OFFSET,
        padding: 20,
        flexDirection: "row",
        justifyContent: "space-between",
    },
});
