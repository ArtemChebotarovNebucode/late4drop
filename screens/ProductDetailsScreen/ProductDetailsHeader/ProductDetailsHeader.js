import React from "react";
import { View } from "react-native";
import BasicLogo from "../BasicLogo/BasicLogo";
import { styles } from "./ProductDetailsHeader.Styles";
import { useNavigation } from "@react-navigation/native";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";

const ProductDetailsHeader = ({ isJustCreated }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <CircledButton
                iconType={"svg"}
                iconSource={require("../../../assets/icons/arrow-left.svg")}
                iconHeight={15}
                iconWidth={15}
                svgColor={Colors.BLUE}
                backgroundColor={Colors.GRAY_BACKGROUND}
                onPress={() =>
                    isJustCreated
                        ? navigation.reset({
                              index: 0,
                              routes: [{ name: "BottomNav" }],
                          })
                        : navigation.goBack()
                }
                fill={Colors.BLUE}
            />
            <BasicLogo />
        </View>
    );
};

export default ProductDetailsHeader;
