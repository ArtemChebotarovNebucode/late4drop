import React, { useEffect, useState } from "react";
import {
    FlatList,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import { styles } from "./Information.Styles";
import PrimaryButton from "../../../components/buttons/PrimaryButton/PrimaryButton";
import DropDown from "../../../components/inputs/DropDown/DropDown";
import BrandsService from "../../../services/brandsService";
import CategoriesService from "../../../services/categoriesService";
import ColorService from "../../../services/colorService";
import SizeService from "../../../services/sizeService";
import ConditionService from "../../../services/conditionService";
import Color from "../../../components/modals/FiltersModal/Color/Color";
import { colors as dummyColors } from "../../../data/dummyColorsData";
import Size from "../../../components/modals/FiltersModal/Size/Size";
import Colors from "../../../constants/Colors";

const Information = ({ handleProductChange, handleCreateProduct, setIsProductCreating }) => {
    const brandsService = new BrandsService();
    const categoryService = new CategoriesService();
    const colorsService = new ColorService();
    const sizeService = new SizeService();
    const conditionService = new ConditionService();

    const [shoeSizesFetched, setShoeSizesFetched] = useState([]);
    const [clothingSizesFetched, setClothingSizesFetched] = useState([]);

    const [brands, setBrands] = useState([]);
    const [categories, setCategories] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState("");
    const [selectedBrand, setSelectedBrand] = useState("");
    const [isCategoryOpen, setIsCategoryOpen] = useState(false);
    const [searchedText, setSearchedText] = useState("");
    const [isBrandOpen, setIsBrandOpen] = useState(false);
    const [price, setPrice] = useState("");
    const [isOnFocus, setIsOnFocus] = useState(false);
    const [colors, setColors] = useState(
        dummyColors.map((color, index) => {
            return {
                ...color,
                id: index,
                active: false,
            };
        })
    );
    const [selectedColor, setSelectedColor] = useState("");
    const [isColorOpen, setIsColorOpen] = useState(false);
    const [sizes, setSizes] = useState([]);
    const [selectedSize, setSelectedSize] = useState([]);
    const [isSizeOpen, setIsSizeOpen] = useState(false);
    const [conditions, setConditions] = useState([]);
    const [selectedCondition, setSelectedCondition] = useState("");
    const [isConditionOpen, setIsConditionOpen] = useState(false);
    const [selectedTmpSize, setSelectedTmpSize] = useState("");
    const [activeColors, setActiveColors] = useState("");

    const handleOpenCategory = () => {
        setIsCategoryOpen(true);
    };
    const handleOpenBrand = () => {
        setIsBrandOpen(true);
    };
    const handleOnColorChoose = (e, color) => {
        setColors(
            [...colors].map((colorFetched) => {
                if (colorFetched?.id === color?.item?.id) {
                    return { ...colorFetched, active: !colorFetched.active };
                }
                return colorFetched;
            })
        );
        if (e !== null) setActiveColors("");
    };
    const handleSelectSize = (size) => {
        setSelectedTmpSize((prev) => (prev === size ? "" : size));
    };
    const handleCategoryChange = (category) => {
        setIsCategoryOpen(false);
        setSelectedCategory(category);
        handleProductChange("category", category);

        if (category !== "Obuwie") {
            setSizes(clothingSizesFetched);
            setSelectedSize(clothingSizesFetched[0]);
        } else {
            setSizes(shoeSizesFetched);
            setSelectedSize(shoeSizesFetched[0]);
        }
    };
    const handleAddActiveColor = (color) => {
        setActiveColors((prev) => (prev !== color ? color : ""));
        // handleOnColorChoose(null, "")
    };
    const createProduct = () => {
        setIsProductCreating(true)
    }

    const handleListFooterComponentStyle = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <TouchableOpacity onPress={() => handleAddActiveColor("moro")}>
                    <Text
                        style={{
                            marginRight: 10,
                            borderWidth: activeColors.includes("moro") ? 2 : 1,
                            borderColor: activeColors.includes("moro")
                                ? Colors.BLUE
                                : Colors.GRAY_LINE,
                            paddingVertical: 14,
                            paddingHorizontal: 12,
                            fontSize: 12,
                            fontFamily: "Montserrat_400Regular",
                        }}
                    >
                        moro
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => handleAddActiveColor("multi")}>
                    <Text
                        style={{
                            marginRight: 10,
                            borderWidth: activeColors.includes("multi") ? 2 : 1,
                            borderColor: activeColors.includes("multi")
                                ? Colors.BLUE
                                : Colors.GRAY_LINE,
                            paddingVertical: 14,
                            paddingHorizontal: 12,
                            fontSize: 12,
                            fontFamily: "Montserrat_400Regular",
                        }}
                    >
                        multi
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };
    const handlePriceChange = (price) => {
        if (!isNaN(price) && !isNaN(parseInt(price))) {
            setPrice(price);
            handleProductChange("price", parseInt(price));
        } else if (price === "") {
            setPrice("");
            handleProductChange("price", 0);
        }
    };
    const handleBrandChange = (brand) => {
        setSelectedBrand(brand);
        setSearchedText(brand);
        handleProductChange("brand", brand);
    };

    const handleColorChange = (color) => {
        setIsColorOpen(false);
        setSelectedColor(color);
        handleProductChange("color", color);
    };

    const handleSizeChange = (size) => {
        setIsSizeOpen(false);
        setSelectedSize(size);
        handleProductChange("size", size);
    };

    const handleConditionChange = (condition) => {
        setIsConditionOpen(false);
        setSelectedCondition(condition);
        handleProductChange("condition", condition);
    };

    useEffect(() => {
        brandsService.fetchBrands().then((response) => {
            const brands = response.data.data.reduce((prev, next) => {
                return prev.concat(next.name);
            }, []);
            setBrands(brands);
            // setSelectedBrand(brands[0]);
            handleProductChange("brand", brands[0]);
        });
        categoryService.fetchCategories().then((response) => {
            const categories = response?.data?.data?.reduce((prev, next) => {
                return prev.concat(next.name);
            }, []);
            setCategories(categories);
            setSelectedCategory(categories[0]);
            handleProductChange("category", categories[0]);
        });
        // colorsService.fetchColors().then((response) => {
        //     const colors = response?.data?.data?.reduce((prev, next) => {
        //         return prev.concat(next?.name);
        //     }, []);
        //     setColors(colors);
        //     setSelectedColor(colors[0]);
        //     handleProductChange("color", colors[0]);
        // });
        sizeService.fetchClothingSizes().then((response) => {
            const out = response?.data?.data?.reduce((prev, next) => {
                return prev.concat(next?.name);
            }, []);
            setClothingSizesFetched(out);
        });
        sizeService.fetchShoeSizes().then((response) => {
            const out = response?.data?.data?.reduce((prev, next) => {
                return prev.concat(next?.name);
            }, []);
            setShoeSizesFetched(out);
            setSizes(out);
            setSelectedSize(out[0]);
            handleProductChange("size", out[0]);
        });
        conditionService.fetchConditions().then((response) => {
            const conditionsFetched = response?.data?.data?.reduce(
                (prev, next) => {
                    return prev.concat(next?.name);
                },
                []
            );
            setConditions(conditionsFetched);
            setSelectedCondition(conditionsFetched[0]);
            handleProductChange("condition", conditionsFetched[0]);
        });
    }, []);
    useEffect(() => {
        handleProductChange("color", activeColors);
    }, [activeColors]);
    useEffect(() => {
        handleProductChange("size", selectedTmpSize);
    }, [selectedTmpSize]);

    return (
        <View style={styles.wrapper}>
            <Text style={styles.label}>Tytuł</Text>
            <TextInput
                onChangeText={(text) => handleProductChange("title", text)}
                style={styles.input}
                placeholder={"Tytuł"}
            />

            <Text style={styles.label}>Opis</Text>
            <TextInput
                multiline={true}
                numberOfLines={4}
                onChangeText={(text) =>
                    handleProductChange("description", text)
                }
                textAlignVertical={"top"}
                placeholder="Treść wiadomości"
                style={[styles.input, { height: 100, paddingTop: 14 }]}
            />

            <Text style={styles.label}>Kategorie</Text>
            <DropDown
                selectedValue={selectedCategory}
                handleValueChange={handleCategoryChange}
                items={categories}
                handleOpenValue={handleOpenCategory}
                isOpen={isCategoryOpen}
            />
            <Text style={styles.label}>Marka</Text>
            <View style={styles.brand}>
                <TextInput
                    onChangeText={(text) => setSearchedText(text)}
                    style={styles.input}
                    value={searchedText}
                    placeholder={"Marka"}
                />
                <View
                    style={[
                        styles.container,
                        {
                            display:
                                searchedText === "" ||
                                selectedBrand === searchedText
                                    ? "none"
                                    : "block",
                        },
                    ]}
                >
                    {brands
                        .filter(
                            (item) =>
                                item
                                    .toLocaleLowerCase()
                                    .indexOf(searchedText.toLocaleLowerCase()) >
                                -1
                        )
                        .slice(0, 5)
                        .map((item) => (
                            <TouchableOpacity
                                onPress={() => handleBrandChange(item)}
                                style={styles.autosuggest}
                            >
                                <Text style={styles.suggest}>{item}</Text>
                            </TouchableOpacity>
                        ))}
                </View>
            </View>

            <Text style={styles.label}>Kolor</Text>
            <FlatList
                horizontal
                data={colors}
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                ListFooterComponent={() => handleListFooterComponentStyle()}
                renderItem={(color) => {
                    return (
                        <Color
                            color={color.item.color}
                            onPress={(e) =>
                                handleAddActiveColor(color.item.name)
                            }
                            isActive={activeColors === color.item.name}
                            borderNeeded={color.item.color === "#FFF"}
                        />
                    );
                }}
                style={{ marginVertical: 10 }}
                keyExtractor={(color) => `${color.name}`}
            />

            {selectedCategory !== "Akcesoria" ? (
                <View>
                    <Text style={styles.label}>Rozmiar</Text>
                    <FlatList
                        horizontal
                        data={sizes}
                        showsHorizontalScrollIndicator={false}
                        legacyImplementation={false}
                        renderItem={({ item }) => {
                            return (
                                <Size
                                    name={item}
                                    onPress={() => handleSelectSize(item)}
                                    isActive={selectedTmpSize === item}
                                />
                            );
                        }}
                        style={{ marginVertical: 10 }}
                        keyExtractor={(size) => `${size}`}
                    />
                </View>
            ) : null}
            <Text style={styles.label}>Stan</Text>
            <DropDown
                selectedValue={selectedCondition}
                handleValueChange={handleConditionChange}
                items={conditions}
                handleOpenValue={() => setIsConditionOpen(true)}
                isOpen={isConditionOpen}
            />
            <Text style={styles.price}>Cena</Text>
            <TextInput
                value={isOnFocus ? price : price ? `${price} zł` : ""}
                onFocus={() => setIsOnFocus(true)}
                onBlur={() => setIsOnFocus(false)}
                keyboardType={"numeric"}
                onChangeText={handlePriceChange}
                style={styles.input}
                placeholder={"0 zł"}
            />

            <PrimaryButton
                onPress={() => {
                    createProduct();
                }}
                label={"DODAJ"}
                additionalStyle={{ marginTop: 25, marginBottom: 30 }}
            />
        </View>
    );
};
export default Information;
