import React from "react";
import { View, Text } from "react-native";
import Colors from "../../../constants/Colors";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import HeaderBar from "../../../components/partials/HeaderBar/HeaderBar";
import { styles } from './Header.Styles'
import StyledStatusBar from "../../../components/partials/StyledStatusBar/StyledStatusBar";

const Header = ({ onBackPress }) => {
  return (
    <View style={styles.header}>
      <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
      <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20 }}>
        <CircledButton
          iconType={"svg"}
          iconSource={require("../../../assets/icons/arrow-left.svg")}
          iconHeight={15}
          iconWidth={15}
          svgColor={Colors.BLUE}
          backgroundColor={Colors.GRAY_BACKGROUND}
          onPress={onBackPress}
          fill={Colors.BLUE}
        />
        <View style={{ position: 'absolute', alignItems: "center", width: "100%" }}>
          <Text style={styles.screenTitle}>UTWÓRZ OFERTĘ</Text>
        </View>
      </View>
    </View>
  );
};

export default Header;
