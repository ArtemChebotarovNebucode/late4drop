import React, { useEffect, useState } from "react";
import { Alert, ScrollView, View } from "react-native";
import { styles } from "./BuyNowScreen.Styles";
import Images from "./Images/Images";
import Header from "./Header/Header";
import Information from "./Information/Information";
import CameraSync from "./Camera/Camera";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";
import ProductService from "../../services/productService";

import { connect } from "react-redux";
import { addLastAddedProducts, addAuctionItems } from "../../store/actions/productAction";
import * as ImagePicker from "expo-image-picker";
import useDebounce from "../../hooks/Debounce";

const BuyNowScreen = ({ navigation, onBackPress, addLastAddedProducts, addAuctionItems }) => {
    const [isCameraOpen, setIsCameraOpen] = useState(false);
    const productService = new ProductService();
    const [images, setImages] = useState([]);
    const [isProductCreation, setIsProductCreating] = useState(false);
    const debouncedValue = useDebounce(isProductCreation, 2000);
    const { userToken, refreshMyActiveProducts, currentUser } = useAuth();
    const [product, setProduct] = useState({
        title: "",
        price: 0,
        description: "",
        brand: "",
        size: "XL",
        shipmentToEurope: 15,
        shipmentToPoland: 15,
        acceptOffers: true,
        condition: "",
        color: "",
        category: "",
        quantity: 1,
        photoStrings: [],
    });
    const handleProductChange = (name, value) => {
        setProduct((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const addImage = (image) => {
        setIsCameraOpen(false);
        setImages((prev) => [...prev, image]);
    };
    const removeImage = (uri) => {
        setImages((prev) => prev.filter((image) => image.uri !== uri));
    };
    const handleCreateProduct = () => {
        productService.addProduct(userToken, product).then((response) => {
            setIsProductCreating(false)
            if (response.data.data !== undefined) {
                addLastAddedProducts(response.data.data);
                // addAuctionItems(response.data.data)
                refreshMyActiveProducts(currentUser.id)
                navigation.navigate("ProductDetails", {
                    type: "BUY_NOW",
                    productId: response.data.data.id,
                    isJustCreated: true,
                });
            }
        });
    };
    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            base64: true,
            aspect: [4, 3],
            quality: 1,
        });
        if (!result.cancelled) {
            addImage(result);
        }
    };
    const handlePressAddImage = () => {
        Alert.alert(
          "Wybierz z",
          "",
          [
              {
                  text: "Galerii",
                  onPress: () => {
                      pickImage();
                  },
              },
              {
                  text: "Aparatu",
                  onPress: () => {
                      setIsCameraOpen(true);
                  },
              },
          ],
          { cancelable: false }
        );
    };

    useEffect(() => {
        if (isCameraOpen) {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }
    }, [isCameraOpen]);

    useEffect(() => {
        const baseImages = images.reduce((prev, next) => {
            return prev.concat(
              `data:image/png;base64,${next.base64.toString()}`
            );
        }, []);
        handleProductChange("photoStrings", baseImages);
    }, [images]);
    useEffect(() => {
        (async () => {
            const {
                status,
            } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        })();
    }, []);
    useEffect(() => {
        if(isProductCreation){
            handleCreateProduct()
        }
    }, [debouncedValue])

    return (
      <View style={styles.wrapper}>
          <Header onBackPress={onBackPress} />
          <ScrollView style={{ width: "100%" }}>
              <View style={styles.container}>
                  <Images
                    removeImage={removeImage}
                    images={images}
                    handlePressAddImage={handlePressAddImage}
                  />
                  <Information
                    handleProductChange={handleProductChange}
                    handleCreateProduct={handleCreateProduct}
                    setIsProductCreating={setIsProductCreating}
                  />
              </View>
          </ScrollView>
          {isCameraOpen ? (
            <CameraSync
              addImage={addImage}
              setIsCameraOpen={setIsCameraOpen}
              pickImage={pickImage}
            />
          ) : (
            <></>
          )}
      </View>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        addLastAddedProducts: (data) => dispatch(addLastAddedProducts(data)),
        addAuctionItems: (data) => dispatch(addAuctionItems(data))
    };
};

export default connect(null, mapDispatchToProps)(BuyNowScreen);
