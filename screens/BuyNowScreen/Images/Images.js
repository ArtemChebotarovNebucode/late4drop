import React from "react";
import { Alert, FlatList, Text, View } from "react-native";
import { styles } from "./Images.Styles";
import PrimaryButton from "../../../components/buttons/PrimaryButton/PrimaryButton";
import AddButton from "../../../components/buttons/AddButton/AddButton";
import CustomImage from "./CustomImage/CustomImage";

const Images = ({ removeImage, images, handlePressAddImage }) => {
    const takeImageHandler = async () => {
        handlePressAddImage();
    };

    const handleLongPressImage = (uri) => {
        Alert.alert(
            "Usunięcie zdjęcia",
            "Czy chcesz usunąć zdjęcie?",
            [
                {
                    text: "Nie",
                    onPress: () => {},
                    style: "cancel",
                },
                {
                    text: "Tak",
                    onPress: () => removeImage(uri),
                },
            ],
            { cancelable: false }
        );
    };
    return (
        <View>
            <Text style={styles.text}>Dodaj max 10 zdjeć</Text>
            <View style={styles.imagesWrapper}>
                {images.length === 0 ? (
                    <PrimaryButton
                        onPress={() => takeImageHandler()}
                        label={"DODAJ ZDJĘCIA"}
                        additionalStyle={{ height: 31, width: 200 }}
                    />
                ) : (
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        legacyImplementation={false}
                        data={[...images, { uri: "0" }]}
                        renderItem={({ item, index }) =>
                            index !== images?.length ? (
                                <CustomImage
                                    item={item}
                                    handleLongPressImage={handleLongPressImage}
                                    removeImage={removeImage}
                                />
                            ) : (
                                <AddButton
                                    takeImageHandler={takeImageHandler}
                                />
                            )
                        }
                        keyExtractor={(image) => image?.uri}
                    />
                )}
            </View>
            <Text style={styles.text}>Informacje ogólne</Text>
        </View>
    );
};

export default Images;
