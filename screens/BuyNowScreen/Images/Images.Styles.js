import {  StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";

export const styles = StyleSheet.create({
    text: {
        color: Colors.BLUE,
        fontFamily: "Montserrat_700Bold"
    },
    imagesWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 175,
        flexDirection: 'row'
    }
});
