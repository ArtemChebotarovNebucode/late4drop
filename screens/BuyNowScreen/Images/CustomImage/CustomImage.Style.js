import {  StyleSheet } from "react-native";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";

export const styles = StyleSheet.create({
    image: {
        width: 112,
        height: 120,
        resizeMode: ImageResizeMode.cover,
        marginRight: 10
    }
});
