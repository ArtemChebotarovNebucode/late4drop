import {StyleSheet} from "react-native";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";

export const styles = StyleSheet.create({
    image: {
        width: 47,
        height: 47,
        resizeMode: ImageResizeMode.cover,
        marginRight: 10,
        borderRadius: 3
    }
});
