import { StyleSheet, StatusBar} from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        position: "absolute",
        height: "100%",
        width: "100%",
    },
    camera: {
        flex: 1,
    },

    buttonContainer: {
        flex: 1,
        backgroundColor: "transparent",
        width: "100%",
        marginBottom: 20,
        position: "absolute",
        bottom: 0,
    },
    button: {
        alignItems: "center",
        width: "100%",
        justifyContent: "center",
    },
    wrapper: {
        flex: 1,
        backgroundColor: Colors.BLACK_BACKGROUND,
    },
    headerContainer: {
        flexDirection: "row",
        width: 70,
        justifyContent: "space-between",
    },
    header: {
        height: 64,
        width: "100%",
        backgroundColor: Colors.BLACK,
        opacity: 0.3,
        borderBottomLeftRadius: 14,
        borderBottomRightRadius: 14,
    },
    text: {
        color: Colors.WHITE,
        fontSize: 16,
        fontFamily: "Montserrat_700Bold",
    },
    headerWrapper: {
        position: "absolute",
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingVertical: StatusBar.currentHeight,
        paddingHorizontal: 20,
    },
    cameraWrapper: {
        borderWidth: 1,
        borderColor: Colors.WHITE,
        borderRadius: 36,
        padding: 8,
    },
});
