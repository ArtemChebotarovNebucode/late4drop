import { Platform, StyleSheet } from "react-native";
import Constants from "expo-constants";
import Colors from "../../constants/Colors";

export const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container: {
    marginHorizontal: 20,
    paddingTop: 10,
  },
});
