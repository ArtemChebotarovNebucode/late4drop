import React, { useState } from "react";
import { BuyNowScreen, CreateAuction } from "../index";
import Alert from "./Alert/Alert";

const SeparatorScreen = ({ navigation }) => {
    const handleOnPrimaryPress = () => {
        setContent(<BuyNowScreen navigation={navigation} onBackPress={handleOnBackPress}/>);
    };

    const handleOnSecondaryPress = () => {
        setContent(<CreateAuction navigation={navigation} onBackPress={handleOnBackPress}/>);
    };

    const initialContent = (
        <Alert
            handleOnSecondaryPress={handleOnSecondaryPress}
            handleOnPrimaryPress={handleOnPrimaryPress}
        />
    );

    const handleOnBackPress = () => {
        setContent(initialContent);
    }


    const [content, setContent] = useState(initialContent);

    return content;
};

export default SeparatorScreen;