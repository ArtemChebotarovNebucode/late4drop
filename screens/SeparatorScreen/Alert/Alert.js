import React from "react";
import { Text, View } from "react-native";
import { styles } from "./Alert.Styles";
import PrimaryButton from "../../../components/buttons/PrimaryButton/PrimaryButton";
import SecondaryButton from "../../../components/buttons/SecondaryButton/SecondaryButton";

const Alert = ({ handleOnPrimaryPress, handleOnSecondaryPress }) => {
  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <Text style={styles.messageWithoutTitle}>
          WYBIERZ FORMĘ SPRZEDAŻY
        </Text>
        <PrimaryButton
          label={"SPRZEDAŻ TRADYCYJNA"}
          onPress={handleOnPrimaryPress}
          additionalStyle={{ marginTop: 20 }}
        />
        <SecondaryButton
          label={"LICYTACJA"}
          onPress={handleOnSecondaryPress}
          additionalStyle={{ marginTop: 20 }}
        />
      </View>
    </View>
  );
};

export default Alert;