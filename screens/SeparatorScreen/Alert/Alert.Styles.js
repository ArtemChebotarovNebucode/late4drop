import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF",
        marginTop: 50,
        padding: 20,
    },
    wrapper: {
        flex: 1,
        backgroundColor: "#000000AA",
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 30,
    },
    message: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 18,
        textAlign: "center",
    },
    title: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 24,
        textAlign: "center",
        marginBottom: 10,
    },
    messageWithoutTitle: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 22,
        textAlign: "center",
    },
});