import React, { useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";

import SearchField from "../../../components/partials/SearchField/SearchField";

import { styles } from "./Header.Styles";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";
import { useNavigation } from "@react-navigation/core";
import FiltersModal from "../../../components/modals/FiltersModal/FiltersModal";
import SvgUri from "expo-svg-uri";

const Header = () => {
  const navigation = useNavigation();

  const [filtersAreVisible, setFiltersAreVisible] = useState(false);

  const handleOpenFilters = () => setFiltersAreVisible(true);
  const handleCloseFilters = () => setFiltersAreVisible(false);

  return (
    <View style={styles.header}>
      <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20 }}>
        <CircledButton
          iconType={"svg"}
          iconSource={require("../../../assets/icons/black-cross-icon.svg")}
          iconHeight={15}
          iconWidth={15}
          svgColor={Colors.BLUE}
          backgroundColor={Colors.GRAY_BACKGROUND}
          onPress={() => navigation.goBack()}
          fill={Colors.BLUE}
        />
        <View style={{ position: 'absolute', alignItems: "center", width: "100%" }}>
          <Text style={styles.screenTitle}>WYBÓR LATE4DROP</Text>
        </View>
      </View>
      <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20}}>
        <SearchField additionalStyles={{ flex: 15 }} />

        <TouchableOpacity
          style={{ flex: 1, marginLeft: 10, padding: 5 }}
          onPress={handleOpenFilters}
        >
          <SvgUri
            width={24}
            height={17.97}
            source={require("../../../assets/icons/filters-icon.svg")}
          />
        </TouchableOpacity>
      </View>

      <FiltersModal
        closeModal={handleCloseFilters}
        isVisible={filtersAreVisible}
        setFilters={setFiltersAreVisible}
      />
    </View>
  );
};

export default Header;
