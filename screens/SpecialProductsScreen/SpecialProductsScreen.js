import React, { useEffect, useState } from "react";
import { ScrollView, View } from "react-native";

import Colors from "../../constants/Colors";

import { styles } from "./SpecialProductsScreen.Styles";
import Header from "./Header/Header";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Offsets from "../../constants/Offsets";
import Products from "./Products/Products";
import ProductService from "../../services/productService";

const SpecialProductsScreen = ({ navigation }) => {
    const productService = new ProductService();
    const [specialProducts, setSpecialProducts] = useState([]);

    useEffect(() => {
        const initial = async () => {
            await productService
                .getSpecialProducts()
                .then((res) => setSpecialProducts(res?.data.data));
        };
        initial();
    }, []);

    return (
        <View style={styles.container}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header navigation={navigation} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <Products specialProducts={specialProducts}/>

                <View style={{ height: Offsets.BOTTOM_OFFSET + 120 }} />
            </ScrollView>
        </View>
    );
};

export default SpecialProductsScreen;
