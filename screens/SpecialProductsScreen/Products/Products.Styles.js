import {StyleSheet} from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    productsContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        marginVertical: 20,
        justifyContent: "space-around",
    },
    infoMessage: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 14,
        color: Colors.GRAY_TEXT,
    }
});