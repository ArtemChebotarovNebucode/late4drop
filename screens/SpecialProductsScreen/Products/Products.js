import React from "react";
import {
    Text,
    View
} from "react-native";

import ProductContainer from "../../../components/partials/ProductContainer/ProductContainer";

import { styles } from "./Products.Styles";
import { products } from "../../../data/dummyProductsData";


const Products = ({specialProducts}) => {
    return (
        <View style={styles.productsContainer}>
            {specialProducts.length !== 0 ? specialProducts.map(product => {
                return <ProductContainer
                    key={product.id}
                    productImg={product.img}
                    productTitle={product.name}
                    productPrice={product.price}
                    productSize={product.size}
                    productLikesCount={product.likesCount}
                    special
                />
            }) : (
              <View
                style={{
                    width: "100%",
                    height: "100%",
                    justifyContent: "center",
                    alignItems: "center",
                }}
              >
                  <Text style={styles.infoMessage}>Aktualnie nie mamy produktów do wyświetlenia.</Text>
              </View>
            )}
        </View>
    );
};

export default Products;
