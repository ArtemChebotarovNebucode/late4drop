import React from "react";
import {
    Image,
    Text, TouchableOpacity,
    View,
} from "react-native";

import CircledButton from "../../../components/buttons/CircledButton/CircledButton";

import Colors from "../../../constants/Colors";
import { styles } from "./Product.Styles";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";
import { useNavigation } from "@react-navigation/core";


const Product = ({ product, onPress }) => {

    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.product}>
                <View style={{flexDirection: "row", flex: 10}}>
                    <ImageBlurLoading style={styles.productImg} source={product.img} />


                    <View style={styles.productInfo}>
                        <Text style={styles.date}>{product.saleDate}</Text>

                        <Text style={styles.productTitle}>{product.name}</Text>

                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <Image
                                style={{width: 21, height: 21}}
                                source={require('../../../assets/images/profile-photo.png')}
                            />

                            <Text style={styles.personName}>Jonathan Henderson</Text>
                        </View>

                        <Text style={styles.productPrice}>{product.price}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default Product;
