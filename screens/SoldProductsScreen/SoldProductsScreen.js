import React, { useEffect, useState } from "react";
import { ScrollView, Text, View } from "react-native";

import Colors from "../../constants/Colors";
import { styles } from "./SoldProductsScreen.Styles";
import Header from "./Header/Header";
import Product from "./Product/Product";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Offsets from "../../constants/Offsets";
import UserService from "../../services/userService";

const SoldProductsScreen = ({ navigation }) => {
    const userService = new UserService();
    const [soldProducts, setSoldProducts] = useState([]);

    useEffect(() => {
        const initial = async () => {
            await userService.getMySoldProducts().then((res) => {
                setSoldProducts(res.data?.data);
            });
        };
        initial();
    }, []);

    return (
        <View style={styles.container}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header navigation={navigation} soldProducts={soldProducts} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.productsContainer}>
                    {soldProducts?.length !== 0 ? (
                        soldProducts?.map((product) => (
                            <Product
                                product={product}
                                key={product.id}
                                onPress={() =>
                                    navigation.navigate("ProductDetails", {
                                        type: "SOLD",
                                    })
                                }
                            />
                        ))
                    ) : (
                        <View
                            style={{
                                width: "100%",
                                height: "100%",
                                justifyContent: "center",
                                alignItems: "center",
                            }}
                        >
                            <Text style={styles.infoMessage}>Aktualnie nie masz sprzedanych produktów.</Text>
                        </View>
                    )}
                </View>

                <View style={{ height: Offsets.BOTTOM_OFFSET + 120 }} />
            </ScrollView>
        </View>
    );
};

export default SoldProductsScreen;
