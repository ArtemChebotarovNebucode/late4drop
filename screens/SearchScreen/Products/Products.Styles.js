import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    productsContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        marginVertical: 20,
        justifyContent: "space-around",
    },
});