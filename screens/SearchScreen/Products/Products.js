import React, { useState } from "react";
import { View } from "react-native";

import ProductContainer from "../../../components/partials/ProductContainer/ProductContainer";

import { styles } from "./Products.Styles";
import { products } from "../../../data/dummyProductsData";

const Products = () => {

    return (
        <View style={styles.productsContainer}>
            {products.map((product) => {
                return (
                    <ProductContainer
                        key={product.id}
                        productImg={product.img}
                        productTitle={product.name}
                        productPrice={product.price}
                        productSize={product.size}
                        productLikesCount={product.likesCount}
                    />
                );
            })}
        </View>
    );
};

export default Products;
