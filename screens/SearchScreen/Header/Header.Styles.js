import {Platform, StyleSheet} from "react-native";
import Constants from "expo-constants";

import Colors from "../../../constants/Colors";
import Offsets from "../../../constants/Offsets";

export const styles = StyleSheet.create({
    header: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        top: 0,
        marginTop: Offsets.TOP_OFFSET,
        padding: 20,
    },
    headerText: {
        fontFamily: "Montserrat_400Regular",
        color: "#FFFFFF",
        fontSize: 30
    },
});