import React from "react";
import {  View, TouchableOpacity } from "react-native";
import SvgUri from "expo-svg-uri";

import SearchField from "../../../components/partials/SearchField/SearchField";

import { styles } from "./Header.Styles";

const Header = ({
    handleOpenFilters,
    setSearchedText,
    handleSearchProducts,
}) => {
    return (
        <View style={styles.header}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <SearchField
                    setSearchedText={setSearchedText}
                    handleSearchProducts={handleSearchProducts}
                    additionalStyles={{ flex: 15 }}
                />

                <TouchableOpacity
                    style={{ flex: 1, marginLeft: 10, padding: 5 }}
                    onPress={handleOpenFilters}
                >
                    <SvgUri
                        width={24}
                        height={17.97}
                        source={require("../../../assets/icons/filters-icon.svg")}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default Header;
