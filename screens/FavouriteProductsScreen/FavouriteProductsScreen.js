import React, { useEffect, useState } from "react";
import { ScrollView, View } from "react-native";

import Colors from "../../constants/Colors";

import { styles } from "./FavouriteProductsScreen.Styles";
import Header from "./Header/Header";
import Product from "./Product/Product";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Offsets from "../../constants/Offsets";
import UserService from "../../services/userService";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";

const FavouriteProductsScreen = ({ navigation }) => {
    const [likedProducts, setLikedProducts] = useState([]);
    const userService = new UserService();
    const { currentUser, userToken, handleLikeProduct } = useAuth();

    const handleDeleteLikedProduct = (product) => {
        setLikedProducts((prev) =>
            prev.filter((item) => item.id !== product.id)
        );
        userService
            .stopLikingById(userToken, product.id)
            .then((response) => {})
            .catch((err) => {
                setLikedProducts((prev) => [...prev, product]);
            });
        handleLikeProduct(product.id)
    };

    useEffect(() => {
        userService.getLikedProducts(currentUser?.id).then((response) => {
            setLikedProducts(response.data.data ?? []);
        });
    }, []);

    return (
        <View style={styles.container}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header navigation={navigation} quantity={likedProducts?.length} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.productsContainer}>
                    {likedProducts.map((product) => (
                        <Product
                            product={product}
                            key={product.id}
                            onPress={() =>
                                navigation.navigate("ProductDetails", {
                                    type: "BUY_NOW",
                                    productId: product?.id,
                                })
                            }
                            handleDeleteLikedProduct={handleDeleteLikedProduct}
                        />
                    ))}
                </View>

                <View style={{ height: Offsets.BOTTOM_OFFSET + 120 }} />
            </ScrollView>
        </View>
    );
};

export default FavouriteProductsScreen;
