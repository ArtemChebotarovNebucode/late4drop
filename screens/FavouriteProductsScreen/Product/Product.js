import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";

import CircledButton from "../../../components/buttons/CircledButton/CircledButton";

import Colors from "../../../constants/Colors";
import { styles } from "./Product.Styles";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";
import { useNavigation } from "@react-navigation/core";

const Product = ({ product, onPress, handleDeleteLikedProduct }) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.product}>
                <View style={{ flexDirection: "row", flex: 10 }}>
                    <ImageBlurLoading
                        style={styles.productImg}
                        source={
                            product?.images?.length > 0 && product
                                ? {
                                      uri: product?.images?.find(
                                          (obj) => obj.size === "big"
                                      ).url,
                                  }
                                : require("../../../assets/images/profile-photo.png")
                        }
                    />

                    <View style={styles.productInfo}>
                        <Text style={styles.productTitle}>
                            {product?.title}
                        </Text>

                        <View
                            style={{
                                flexDirection: "row",
                                alignItems: "center",
                            }}
                        >
                            <Image
                                style={{ width: 21, height: 21 }}
                                source={require("../../../assets/images/profile-photo-standard.png")}
                            />

                            <Text style={styles.personName}>
                                No Name
                            </Text>
                        </View>

                        <Text style={styles.productPrice}>
                            {product?.price}
                        </Text>
                    </View>
                </View>

                <View style={{ flex: 1.7 }}>
                    <CircledButton
                        backgroundColor={"#FEF0F3"}
                        additionalStyle={{ margin: 3 }}
                        iconType={"svg"}
                        iconHeight={15}
                        iconWidth={15}
                        iconSource={require("../../../assets/icons/garbage-icon.svg")}
                        onPress={() => handleDeleteLikedProduct(product)}
                    />
                    <CircledButton
                        backgroundColor={Colors.BLUE}
                        additionalStyle={{ margin: 3 }}
                        iconType={"svg"}
                        iconHeight={15}
                        iconWidth={15}
                        iconSource={require("../../../assets/icons/arrow-right.svg")}
                        onPress={onPress}
                    />
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default Product;
