import {StyleSheet} from "react-native";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";

export const styles = StyleSheet.create({
    product: {
        flexDirection: "row",
        justifyContent: "space-between",
        height: 117,
        flex: 1,
        marginVertical: 5,
        marginHorizontal: 20,
    },
    productImg: {
        height: "100%",
        width: "25%",
        resizeMode: ImageResizeMode.cover,
    },
    productTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 16,
        marginBottom: 10,
        width: '75%'

    },
    productInfo: {
        marginHorizontal: 15,
        justifyContent: "space-between",
        marginVertical: 10
    },
    productPrice: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 22
    },
    personName: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 12,
        marginLeft: 7
    }
});