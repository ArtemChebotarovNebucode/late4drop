import React, { useState } from "react";
import { AsyncStorage, Text, View } from "react-native";

import Colors from "../../constants/Colors";
import CircledButton from "../../components/buttons/CircledButton/CircledButton";
import CustomTextInput from "../../components/inputs/CustomTextInput/CustomTextInput";
import PrimaryButton from "../../components/buttons/PrimaryButton/PrimaryButton";
import TouchableText from "../../components/buttons/TouchableText/TouchableText";

import { styles } from "./LoginScreen.Styles";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";
import { DismissKeyboard } from "../../components";

const LoginScreen = ({ navigation }) => {
    const [providedData, setProvidedData] = useState({
        email: "",
        password: "",
    });
    const { login } = useAuth();
    const [passwordError, setPasswordError] = useState("");
    const [emailError, setEmailError] = useState("");

    const setErrorMessage = (message) => {
        if (
            message ===
            "The password is invalid or the user does not have a password."
        )
            setPasswordError("Niepoprawne hasło.");
        else if (message === "The email address is badly formatted.")
            setEmailError("Adres e-mail jest źle sformatowany.");
        else if (
            message ===
            "There is no user record corresponding to this identifier. The user may have been deleted."
        )
            setEmailError("Użytkownik o podanym adresie e-mail nie istnieje.");
    };

    const handleOnRedirectToRegister = () => {
        navigation.navigate("Register");
    };

    const handleOnLogin = () => {
        setPasswordError("");
        setEmailError("");

        login(providedData.email, providedData.password)
            .then((resp) => {
                resp.user.getIdToken(false).then((token) => {
                    AsyncStorage.setItem("ACCESS_TOKEN", token);
                    navigation.replace("BottomNav");
                });
            })
            .catch((err) => {
                setErrorMessage(err.message);
            });
    };

    return (
        <DismissKeyboard>
            <View style={styles.container}>
                <Text style={styles.title}>ZALOGUJ SIĘ</Text>

                {/*<View style={styles.socialNetworkButtons}>*/}
                {/*    <CircledButton*/}
                {/*        iconType={"svg"}*/}
                {/*        iconSource={require("../../assets/icons/facebook-icon.svg")}*/}
                {/*        iconWidth={21}*/}
                {/*        iconHeight={21}*/}
                {/*        backgroundColor={"#3B5998"}*/}
                {/*        additionalStyle={{ marginRight: 10 }}*/}
                {/*    />*/}
                {/*    <CircledButton*/}
                {/*        iconType={"png"}*/}
                {/*        iconSource={require("../../assets/icons/google-icon.png")}*/}
                {/*        iconWidth={21}*/}
                {/*        iconHeight={21}*/}
                {/*        backgroundColor={"#FFFFFF"}*/}
                {/*        additionalStyle={{*/}
                {/*            borderColor: Colors.GRAY_BACKGROUND,*/}
                {/*            borderWidth: 2,*/}
                {/*        }}*/}
                {/*    />*/}
                {/*</View>*/}

                <View>
                    <CustomTextInput
                        inlineImageLeft={require("../../assets/icons/message-icon.svg")}
                        label={"Adres e-mail"}
                        textContentType={"email"}
                        placeholder={"Wpisz swój adres e-mail"}
                        onChangeText={(email) =>
                            setProvidedData({ ...providedData, email: email })
                        }
                    />
                    {emailError.length !== 0 ? (
                        <Text style={styles.error}>{emailError}</Text>
                    ) : null}

                    <CustomTextInput
                        additionalStyle={{ marginTop: 20 }}
                        inlineImageLeft={require("../../assets/icons/lock-icon.svg")}
                        label={"Hasło"}
                        textContentType={"password"}
                        placeholder={"Wpisz swoje hasło"}
                        onChangeText={(password) =>
                            setProvidedData({
                                ...providedData,
                                password: password,
                            })
                        }
                    />
                    {passwordError.length !== 0 ? (
                        <Text style={styles.error}>{passwordError}</Text>
                    ) : null}
                </View>

                <View style={styles.underInputs}>
                    <TouchableText
                        label={"Zapomniałeś hasła"}
                        onPress={() => {
                            navigation.navigate("ResetPassword");
                        }}
                    />
                </View>

                <PrimaryButton
                    additionalStyle={styles.primaryButton}
                    label={"ZALOGUJ SIĘ"}
                    onPress={() => handleOnLogin()}
                />

                <View style={styles.footer}>
                    <View style={{ flexDirection: "row" }}>
                        <Text
                            style={{
                                marginRight: 10,
                                fontFamily: "Montserrat_400Regular",
                                fontSize: 14,
                            }}
                        >
                            Nie masz jeszcze konta
                        </Text>

                        <TouchableText
                            label={"Zarejestruj się"}
                            onPress={handleOnRedirectToRegister}
                        />
                    </View>
                </View>
            </View>
        </DismissKeyboard>
    );
};


export default LoginScreen;
