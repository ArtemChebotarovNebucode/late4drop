import { StyleSheet, Platform } from "react-native";
import Colors from "../../../constants/Colors";
import Constants from "expo-constants";

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
        paddingTop: Platform.OS === "android" ? Constants.statusBarHeight : 0,
        position:'relative'
    },
    text: {
        fontSize: 18,
        color: Colors.WHITE,
        marginTop: 0,
        marginRight: "auto",
        marginBottom: 0,
        marginLeft: "auto",
        fontFamily: "Montserrat_700Bold",
    },
    icon:{
        position: 'absolute',
        top: -19
    }
});
