import React from "react";
import { Text, View } from "react-native";
import HeaderBar from "../../../components/partials/HeaderBar/HeaderBar";
import Colors from "../../../constants/Colors";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import { styles } from "./Header.Styles";
import { useNavigation } from "@react-navigation/core";

const Header = () => {
    const navigation = useNavigation();

  return (
    <View>
      <HeaderBar
        backgroundColor={Colors.BLACK_BACKGROUND}
        barStyle={"light-content"}
      >
        <View style={styles.wrapper}>
          <CircledButton
            iconType={"svg"}
            iconSource={require("../../../assets/icons/black-cross-icon.svg")}
            iconHeight={15}
            iconWidth={15}
            backgroundColor={Colors.GRAY_BACKGROUND}
            fill={Colors.BLUE}
            onPress={() => navigation.goBack()}
            additionalStyle={styles.icon}
          />
          <Text style={styles.text}>ROZLICZENIA</Text>
        </View>
      </HeaderBar>
    </View>
  );
};
export default Header;
