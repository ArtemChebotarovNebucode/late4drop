import {Platform, StyleSheet} from "react-native";
import Constants from "expo-constants";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        marginHorizontal: 20,
        paddingTop: 20,
        paddingBottom: 20

    },
});
