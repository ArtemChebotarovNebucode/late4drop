import React from "react";
import { ScrollView, View } from "react-native";
import { styles } from "./CalculationsScreen.Styles";
import Header from "./Header/Header";
import BuyNow from "../../components/partials/BuyNow/BuyNow";

const CalculationsScreen = ({navigation}) => {
    return (
        <View style={styles.wrapper}>
            <Header />
            <ScrollView style={{ width: "100%" }}>
                <View style={styles.container}>
                    <BuyNow isModal={false} navigation={navigation}/>
                </View>
            </ScrollView>
        </View>
    );
};

export default CalculationsScreen;
