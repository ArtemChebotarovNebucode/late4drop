import React from "react";

import {StyleSheet, View} from "react-native";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Colors from "../../constants/Colors";
import WebView from "react-native-webview";
import Header from "./Header/Header";

const WebViewScreen = ({navigation, route}) => {
    const {url, isPayments} = route.params;
    return (
        <View style={{ flex: 1 }}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header isPayments={isPayments}/>
            <View style={{ width: "100%", height:  "100%", paddingBottom: 50}}>
                    <WebView source={{ uri: url }} />
            </View>
        </View>
    );
};



export default WebViewScreen;
