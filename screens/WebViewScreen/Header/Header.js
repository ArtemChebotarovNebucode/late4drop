import React from "react";
import { StyleSheet, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";
import BasicLogo from "../../ProductDetailsScreen/BasicLogo/BasicLogo";
import Offsets from "../../../constants/Offsets";

const Header = ({ isPayments }) => {
    const navigation = useNavigation();

    return (
        <View style={isPayments ? styles.container : styles.containerOther}>
                <CircledButton
                    iconType={"svg"}
                    iconSource={require("../../../assets/icons/arrow-left.svg")}
                    iconHeight={15}
                    iconWidth={15}
                    svgColor={Colors.BLUE}
                    backgroundColor={Colors.GRAY_BACKGROUND}
                    onPress={() => navigation.goBack()}
                    fill={Colors.BLUE}
                />

            {isPayments ? <BasicLogo /> : <></>}
        </View>
    );
};
export const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        top: 0,
        marginTop: Offsets.TOP_OFFSET,
        padding: 20,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    containerOther: {
        marginTop: Offsets.TOP_OFFSET,
        position: 'relative',
        backgroundColor: Colors.BLACK_TEXT,
        padding: 5
    },
});

export default Header;
