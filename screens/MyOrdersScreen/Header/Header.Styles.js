import { StyleSheet } from "react-native";

import Colors from "../../../constants/Colors";
import Offsets from "../../../constants/Offsets";

export const styles = StyleSheet.create({
    header: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        top: 0,
        marginTop: Offsets.TOP_OFFSET,
        paddingHorizontal: 20,
        paddingBottom: 20,
    },
    screenTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 18,
        color: "#FFFFFF",
    },
    productsCount: {
        backgroundColor: Colors.BLUE,
        borderRadius: 20,
        padding: 10,
        alignSelf: "flex-start",
    },
    productsCountText: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 12,
        color: "#FFFFFF",
    },
});
