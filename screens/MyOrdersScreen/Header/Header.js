import React from "react";
import {
    Text,
    View,
} from "react-native";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";

import Colors from "../../../constants/Colors";
import { products } from "../../../data/dummyProductsData";
import { styles } from "./Header.Styles";


const Header = ({ navigation }) => {
    return (
        <View style={styles.header}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                <CircledButton
                    iconType={'svg'}
                    iconSource={require('../../../assets/icons/black-cross-icon.svg')}
                    iconHeight={15}
                    iconWidth={15}
                    svgColor={Colors.BLUE}
                    backgroundColor={Colors.GRAY_BACKGROUND}
                    onPress={() => navigation.goBack()}
                    fill={Colors.BLUE}
                />
                <View style={{position: 'absolute', alignItems: 'center', width: '100%'}}>
                    <Text style={styles.screenTitle}>MOJE ZAMÓWIENIA</Text>
                </View>
            </View>

            <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 5}}>
                <View style={styles.productsCount}>
                    <Text style={styles.productsCountText}>{`${products.length} przedmiotów`}</Text>
                </View>
            </View>
        </View>
    );
};

export default Header;
