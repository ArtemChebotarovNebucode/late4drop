import {StyleSheet} from "react-native";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    product: {
        flexDirection: "row",
        justifyContent: "space-between",
        height: 117,
        flex: 1,
        marginVertical: 5,
        marginHorizontal: 20
    },
    productImg: {
        height: "100%",
        width: "25%",
        resizeMode: ImageResizeMode.cover,
        borderRadius: 15,
    },
    productTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 16,
        marginBottom: 5
    },
    productInfo: {
        marginHorizontal: 15,
        justifyContent: "space-between",
        marginVertical: 10
    },
    productPrice: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 22
    },
    personName: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 12,
        marginLeft: 7
    },
    date: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 12,
        color: Colors.GRAY_TEXT
    },
    navigateButton: {
        backgroundColor: Colors.BLUE,
        paddingVertical: 10,
        paddingHorizontal: 15,
        alignSelf: "flex-start",
        marginBottom: 5
    },
    productNameUnder: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 12,
        color: Colors.GRAY_TEXT,
    }
});