import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import { styles } from "./Product.Styles";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";
import { useNavigation } from "@react-navigation/core";

const Product = ({ product, onPress }) => {
    const navigation = useNavigation();
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.product}>
                <View style={{ flexDirection: "row", flex: 10 }}>
                    <ImageBlurLoading
                        style={styles.productImg}
                        source={product.img}
                    />

                    <View style={styles.productInfo}>
                        <Text style={styles.date}>{product.saleDate}</Text>

                        <Text style={styles.productTitle}>{product.name}</Text>

                        <TouchableOpacity style={styles.navigateButton} onPress={() => navigation.navigate("Tracking")}>
                            <Text
                                style={{
                                    ...styles.productNameUnder,
                                    color: "#FFFFFF",
                                }}
                            >
                                Nawiguj
                            </Text>
                        </TouchableOpacity>

                        <Text style={styles.productPrice}>{product.price}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default Product;
