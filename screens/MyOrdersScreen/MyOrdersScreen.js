import React from "react";
import { View, ScrollView } from "react-native";

import Colors from "../../constants/Colors";

import { products } from "../../data/dummyProductsData";
import { styles } from "./MyOrdersScreen.Styles";
import Header from "./Header/Header";
import Product from "./Product/Product";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Offsets from "../../constants/Offsets";

const MyOrdersScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header navigation={navigation} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.productsContainer}>
                    {products.map((product) => (
                        <Product
                            product={product}
                            key={product.id}
                            onPress={() =>
                                navigation.navigate("ProductDetails", {type: "BUY_NOW"})
                            }
                        />
                    ))}
                </View>

                <View style={{ height: Offsets.BOTTOM_OFFSET + 120 }} />
            </ScrollView>
        </View>
    );
};

export default MyOrdersScreen;
