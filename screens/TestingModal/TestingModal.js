import React, { useState } from "react";
import { View, Button } from "react-native";
import Auction from "../../components/modals/AuctionModal/Auction";
import FilterModal from "../../components/modals/FilterModal/FilterModal";

const TestingModal = () => {
    const [isVisible, setIsVisible] = useState(false);
    const openModal = () => {
        setIsVisible(true);
    };
    const closeModal = () => {
        setIsVisible(false);
    };

    return (
        <View
            style={{
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "pink",
                flex: 1,
            }}
        >
            <Button title="Press" onPress={() => openModal()} />
            <FilterModal
                isVisible={isVisible}
                closeModal={closeModal}
            />
        </View>
    );
};

export default TestingModal;
