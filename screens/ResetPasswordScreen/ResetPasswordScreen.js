import React, { useState } from "react";
import { Text, View } from "react-native";
import CustomTextInput from "../../components/inputs/CustomTextInput/CustomTextInput";
import PrimaryButton from "../../components/buttons/PrimaryButton/PrimaryButton";
import TouchableText from "../../components/buttons/TouchableText/TouchableText";

import { styles } from "./ResetPasswordScreen.Styles";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";

const ResetPasswordScreen = ({ navigation }) => {
    const [pointerMessage, setPointerMessage] = useState("");

    const { resetPassword } = useAuth();

    const [email, setEmail] = useState("");

    const handleOnRedirectToLogin = () => {
        navigation.navigate("Login");
    };

    const handleOnPasswordReset = async () => {
        await resetPassword(email).then((res) => {
            if (
                res.data.errors?.message ===
                "Adres e-mail jest źle sformatowany."
            ) {
                setPointerMessage("Użytkownik o podanym adresie e-mail nie istnieje.");
            } else if (
                res.data.errors?.message ===
                'An internal error has occurred. Raw server response: "{"error":{"code":400,"message":"EMAIL_NOT_FOUND","errors":[{"message":"EMAIL_NOT_FOUND","domain":"global","reason":"invalid"}]}}"'
            ) {
                setPointerMessage(
                    "Link do resetu hasła został wysłany na wszkazany email."
                );
            } else if (!res.data.errors) {
                setPointerMessage(
                    "A link to reset your password has been sent to your email."
                );
            }
        });
    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>RESETOWANIE HASŁA</Text>

            <CustomTextInput
                inlineImageLeft={require("../../assets/icons/message-icon.svg")}
                label={"Adres e-mail"}
                textContentType={"email"}
                placeholder={"Wpisz swój adres e-mail"}
                additionalStyle={{ marginTop: 30 }}
                onChangeText={(text) => setEmail(text)}
            />
            {pointerMessage.length !== 0 ? (
                <Text
                    style={
                        pointerMessage !==
                        "Link do resetu hasła został wysłany na wszkazany email."
                            ? styles.error
                            : styles.success
                    }
                >
                    {pointerMessage}
                </Text>
            ) : null}

            <PrimaryButton
                additionalStyle={{ marginTop: 50 }}
                label={"WYŚLIJ"}
                onPress={handleOnPasswordReset}
            />

            <View style={styles.underInputs}>
                <TouchableText
                    label={"Cofnij"}
                    onPress={handleOnRedirectToLogin}
                />
            </View>
        </View>
    );
};

export default ResetPasswordScreen;
