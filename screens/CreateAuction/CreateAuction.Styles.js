import { Dimensions, Platform, StyleSheet } from "react-native";
import Constants from "expo-constants";
import Offsets from "../../constants/Offsets";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        marginHorizontal: 20,
        paddingTop: 10,
        paddingBottom: 20,
    },
});
