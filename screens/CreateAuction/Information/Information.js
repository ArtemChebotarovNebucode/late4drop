import React, { useEffect, useState } from "react";
import {
    FlatList,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Platform
} from "react-native";
import { styles } from "./Information.Styles";
import PrimaryButton from "../../../components/buttons/PrimaryButton/PrimaryButton";
import DropDown from "../../../components/inputs/DropDown/DropDown";
import BrandsService from "../../../services/brandsService";
import CategoriesService from "../../../services/categoriesService";
import ColorService from "../../../services/colorService";
import SizeService from "../../../services/sizeService";
import ConditionService from "../../../services/conditionService";
import Color from "../../../components/modals/FiltersModal/Color/Color";
import { colors as dummyColors } from "../../../data/dummyColorsData";
import Size from "../../../components/modals/FiltersModal/Size/Size";
import Colors from "../../../constants/Colors";
import DateTimePicker from '@react-native-community/datetimepicker';

const Information = ({ handleAuctionChange, handleCreateAuction, setIsAuctionCreating }) => {
    const brandsService = new BrandsService();
    const categoryService = new CategoriesService();
    const colorsService = new ColorService();
    const sizeService = new SizeService();
    const conditionService = new ConditionService();

    const [shoeSizesFetched, setShoeSizesFetched] = useState([]);
    const [clothingSizesFetched, setClothingSizesFetched] = useState([]);

    const [date, setDate] = useState(new Date())
    const [isDateOpen, setIsDateOpen] = useState(false)
    const [brands, setBrands] = useState([]);
    const [categories, setCategories] = useState([]);
    const [searchedText, setSearchedText] = useState("");
    const [selectedCategory, setSelectedCategory] = useState("");
    const [selectedBrand, setSelectedBrand] = useState("");
    const [isCategoryOpen, setIsCategoryOpen] = useState(false);
    const [isBrandOpen, setIsBrandOpen] = useState(false);
    const [price, setPrice] = useState("");
    const [isOnFocus, setIsOnFocus] = useState(false);
    const [colors, setColors] = useState(
        dummyColors.map((color, index) => {
            return {
                ...color,
                id: index,
                active: false,
            };
        })
    );
    const [selectedColor, setSelectedColor] = useState("");
    const [isColorOpen, setIsColorOpen] = useState(false);
    const [sizes, setSizes] = useState([]);
    const [selectedSize, setSelectedSize] = useState([]);
    const [isSizeOpen, setIsSizeOpen] = useState(false);
    const [conditions, setConditions] = useState([]);
    const [selectedCondition, setSelectedCondition] = useState("");
    const [isConditionOpen, setIsConditionOpen] = useState(false);
    const [selectedTmpSize, setSelectedTmpSize] = useState("");
    const [activeColors, setActiveColors] = useState("");
    let isPressed = false;

    const handleOpenCategory = () => {
        setIsCategoryOpen(true);
    };
    const handleAddActiveColor = (color) => {
        setActiveColors((prev) => (prev !== color ? color : ""));
    };

    const handleListFooterComponentStyle = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <TouchableOpacity onPress={() => handleAddActiveColor("moro")}>
                    <Text
                        style={{
                            marginRight: 10,
                            borderWidth: activeColors.includes("moro") ? 2 : 1,
                            borderColor: activeColors.includes("moro")
                                ? Colors.BLUE
                                : Colors.GRAY_LINE,
                            paddingVertical: 14,
                            paddingHorizontal: 12,
                            fontSize: 12,
                            fontFamily: "Montserrat_400Regular",
                        }}
                    >
                        moro
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => handleAddActiveColor("multi")}>
                    <Text
                        style={{
                            marginRight: 10,
                            borderWidth: activeColors.includes("multi") ? 2 : 1,
                            borderColor: activeColors.includes("multi")
                                ? Colors.BLUE
                                : Colors.GRAY_LINE,
                            paddingVertical: 14,
                            paddingHorizontal: 12,
                            fontSize: 12,
                            fontFamily: "Montserrat_400Regular",
                        }}
                    >
                        multi
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };
        const createAuction = () => {
            setIsAuctionCreating(true)
        }
    const handleCategoryChange = (category) => {
        setIsCategoryOpen(false);
        setSelectedCategory(category);
        handleAuctionChange("category", category);

        if (category !== "Obuwie") {
            setSizes(clothingSizesFetched);
            setSelectedSize(clothingSizesFetched[0]);
        } else {
            setSizes(shoeSizesFetched);
            setSelectedSize(shoeSizesFetched[0]);
        }
    };

    const handlePriceChange = (price) => {
        if (!isNaN(price) && !isNaN(parseInt(price))) {
            setPrice(price);
            handleAuctionChange("minPrice", parseInt(price));
        } else if (price === "") {
            setPrice("");
            handleAuctionChange("minPrice", 0);
        }
    };
    const onChangeDate = (event, selectedDate) => {
        setIsDateOpen(false)
        const currentDate = selectedDate || date;
        // setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const handleBrandChange = (brand) => {
        setSelectedBrand(brand);
        setSearchedText(brand);
        handleAuctionChange("brand", brand);
    };

    const handleColorChange = (color) => {
        setIsColorOpen(false);
        setSelectedColor(color);
        handleAuctionChange("color", color);
    };

    const handleSizeChange = (size) => {
        setIsSizeOpen(false);
        setSelectedSize(size);
        handleAuctionChange("size", size);
    };
    const handleSelectSize = (size) => {
        setSelectedTmpSize((prev) => (prev === size ? "" : size));
    };

    const handleConditionChange = (condition) => {
        setIsConditionOpen(false);
        setSelectedCondition(condition);
        handleAuctionChange("condition", condition);
    };
    const getDateStyle = () => {
        return `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`
    }

    useEffect(() => {
        brandsService.fetchBrands().then((response) => {
            const brands = response.data.data.reduce((prev, next) => {
                return prev.concat(next.name);
            }, []);
            setBrands(brands);
            setSelectedBrand(brands[0]);
            handleAuctionChange("brand", brands[0]);
        });
        categoryService.fetchCategories().then((response) => {
            const categories = response.data.data.reduce((prev, next) => {
                return prev.concat(next.name);
            }, []);
            setCategories(categories);
            setSelectedCategory(categories[0]);
            handleAuctionChange("category", categories[0]);
        });
        // colorsService.fetchColors().then((response) => {
        //     const colors = response.data.data.reduce((prev, next) => {
        //         return prev.concat({...next, active: false});
        //     }, []);
        //     setColors(colors);
        //     setSelectedColor(colors[0]);
        //     handleAuctionChange("color", colors[0]);
        //     console.log(colors)
        // });
        sizeService.fetchClothingSizes().then((response) => {
            const out = response.data.data.reduce((prev, next) => {
                return prev.concat(next.name);
            }, []);
            setClothingSizesFetched(out);
        });
        sizeService.fetchShoeSizes().then((response) => {
            const out = response.data.data.reduce((prev, next) => {
                return prev.concat(next.name);
            }, []);
            setShoeSizesFetched(out);
            setSizes(out);
            setSelectedSize(out[0]);
            handleAuctionChange("size", out[0]);
        });
        conditionService.fetchConditions().then((response) => {
            const conditionsFetched = response.data.data.reduce(
                (prev, next) => {
                    return prev.concat(next.name);
                },
                []
            );
            setConditions(conditionsFetched);
            setSelectedCondition(conditionsFetched[0]);
            handleAuctionChange("condition", conditionsFetched[0]);
        });
    }, []);

    useEffect(() => {
        handleAuctionChange("color", activeColors);
    }, [activeColors]);
    useEffect(() => {
        handleAuctionChange("size", selectedTmpSize);
    }, [selectedTmpSize]);
    useEffect(() => {
        handleAuctionChange("endsAt", date);
    }, [date]);

    return (
        <View style={styles.wrapper}>
            <Text style={styles.label}>Tytuł</Text>
            <TextInput
                onChangeText={(text) => handleAuctionChange("title", text)}
                style={styles.input}
                placeholder={"Tytuł"}
            />

            <Text style={styles.label}>Opis</Text>
            <TextInput
                multiline={true}
                numberOfLines={4}
                onChangeText={(text) =>
                    handleAuctionChange("description", text)
                }
                textAlignVertical={"top"}
                placeholder="Treść wiadomości"
                style={[styles.input, { height: 100, paddingTop: 14 }]}
            />

            <Text style={styles.label}>Kategorie</Text>
            <DropDown
                selectedValue={selectedCategory}
                handleValueChange={handleCategoryChange}
                items={categories}
                handleOpenValue={handleOpenCategory}
                isOpen={isCategoryOpen}
            />
            <Text style={styles.label}>Marka</Text>
            <View style={styles.brand}>
                <TextInput
                    onChangeText={(text) => setSearchedText(text)}
                    style={styles.input}
                    value={searchedText}
                    placeholder={"Marka"}
                />
                <View
                    style={[
                        styles.container,
                        {
                            display:
                                searchedText === "" ||
                                selectedBrand === searchedText
                                    ? "none"
                                    : "block",
                        },
                    ]}
                >
                    {brands
                        .filter(
                            (item) =>
                                item
                                    .toLocaleLowerCase()
                                    .indexOf(searchedText.toLocaleLowerCase()) >
                                -1
                        )
                        .slice(0, 5)
                        .map((item) => (
                            <TouchableOpacity
                                onPress={() => handleBrandChange(item)}
                                style={styles.autosuggest}
                            >
                                <Text style={styles.suggest}>{item}</Text>
                            </TouchableOpacity>
                        ))}
                </View>
            </View>
            <Text style={styles.label}>Kolor</Text>
            <FlatList
                horizontal
                data={colors}
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                ListFooterComponent={() => handleListFooterComponentStyle()}
                renderItem={(color) => {
                    return (
                        <Color
                            color={color.item.color}
                            onPress={(e) =>
                                handleAddActiveColor(color.item.name)
                            }
                            isActive={activeColors === color.item.name}
                            borderNeeded={color.item.color === "#FFF"}
                        />
                    );
                }}
                style={{ marginVertical: 10 }}
                keyExtractor={(color) => `${color.name}`}
            />

            {selectedCategory !== "Akcesoria" ? (
                <View>
                    <Text style={styles.label}>Rozmiar</Text>
                    <FlatList
                        horizontal
                        data={sizes}
                        showsHorizontalScrollIndicator={false}
                        legacyImplementation={false}
                        renderItem={({ item }) => {
                            return (
                                <Size
                                    name={item}
                                    onPress={() => handleSelectSize(item)}
                                    isActive={selectedTmpSize === item}
                                />
                            );
                        }}
                        style={{ marginVertical: 10 }}
                        keyExtractor={(size) => `${size}`}
                    />
                </View>
            ) : null}

            <Text style={styles.label}>Stan</Text>
            <DropDown
                selectedValue={selectedCondition}
                handleValueChange={handleConditionChange}
                items={conditions}
                handleOpenValue={() => setIsConditionOpen(true)}
                isOpen={isConditionOpen}
            />
            <Text style={[styles.label, {marginBottom: 10}]}>Data zakończenia</Text>
            {Platform.OS === 'ios' ? <View style={{position: 'relative'}}>
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={onChangeDate}
                    minimumDate={new Date()}
                    style={styles.calendar}
                />
                {/*<View style={{position: 'absolute', width: 120, height: 24, backgroundColor: Colors.WHITE, bottom: 13, left: 5}}></View>*/}
            </View> : <View>
                <TouchableOpacity onPress={() => setIsDateOpen(true)}>
                    <View style={[styles.input, {justifyContent: 'center'}]} >
                        <Text style={styles.text}>{getDateStyle()}</Text>
                    </View>
                </TouchableOpacity>

                {isDateOpen ?            <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={onChangeDate}
                    minimumDate={new Date()}
                    style={styles.calendar}
                /> : <></>}

            </View>}


            <Text style={styles.price}>Cena wywoławcza</Text>
            <TextInput
                value={isOnFocus ? price : price ? `${price} zł` : ""}
                onFocus={() => setIsOnFocus(true)}
                onBlur={() => setIsOnFocus(false)}
                keyboardType={"numeric"}
                onChangeText={handlePriceChange}
                style={styles.input}
                placeholder={"0 zł"}
            />

            <PrimaryButton
                label={"DODAJ"}
                additionalStyle={{ marginTop: 25 }}
                onPress={() => {
                    createAuction();
                }}
            />
        </View>
    );
};
export default Information;
