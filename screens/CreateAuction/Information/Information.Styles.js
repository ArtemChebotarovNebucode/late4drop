import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    wrapper: {
        paddingTop: 25,
    },
    calendar: {
        height: 50,
        backgroundColor: Colors.WHITE,
        borderWidth: 1,
        borderColor: Colors.GRAY_LINE,
        borderRadius: 2,
        color: Colors.BLACK,
    },
    input: {
        height: 44,
        width: "100%",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_LINE,
        marginTop: 10,
        marginBottom: 20,
        paddingHorizontal: 14,
        fontFamily: "Montserrat_400Regular",
    },
    label: {
        fontSize: 12,
        color: Colors.GRAY_LABEL,
        fontFamily: "Montserrat_500Medium",
    },
    price: {
        color: Colors.BLUE,
        fontFamily: "Montserrat_700Bold",
        marginTop: 25,
        marginBottom: 10,
    },
    container: {
        width: "100%",
        position: "absolute",
        height: "auto",
        backgroundColor: Colors.WHITE,
        top: 54,
        zIndex: 9999,
    },
    autosuggest: {
        width: "100%",
        height: 44,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: Colors.GRAY_LINE,
        justifyContent: "center",
        paddingLeft: 10,
    },
    suggest: {
        width: "100%",
        fontSize: 14,
        fontFamily: "Montserrat_500Medium",
    },
    brand: {
        position: "relative",
        width: "100%",
        height: "auto",
        zIndex: 1000,
    },
    text: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 12,
        color: Colors.BLACK
    }
});
