import React from "react";
import { TouchableOpacity, Image } from "react-native";
import { styles } from "./CustomImage.Style";
import CircledButton from "../../../../components/buttons/CircledButton/CircledButton";

const CustomImage = ({ item, handleLongPressImage, removeImage }) => {
  return (
    <TouchableOpacity
      style={{ position: "relative" }}
      onLongPress={() => handleLongPressImage(item.uri)}
    >
      <Image style={styles.image} source={{ uri: item.uri }} />
      <CircledButton
        iconType={"svg"}
        iconSource={require("../../../../assets/icons/close-icon.svg")}
        iconHeight={20}
        iconWidth={20}
        isCloseIcon={true}
        onPress={() => removeImage(item.uri)}
        backgroundColor="transparent"
      />
    </TouchableOpacity>
  );
};
export default CustomImage;
