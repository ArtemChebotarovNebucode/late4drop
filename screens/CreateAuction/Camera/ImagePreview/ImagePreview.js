import React from "react";
import { TouchableOpacity, Image} from "react-native";
import {styles} from './ImagePreview.Styles'
import * as ImageManipulator from "expo-image-manipulator";

const ImagePreview = ({ item, addImage }) => {
    const handleAddImage = () => {
        ImageManipulator.manipulateAsync(
            item.uri,
            [],
            { compress: 0.5, base64: true}
        ).then((result) => {
            item['base64'] = result.base64
            addImage(item)
        });
    };
    return (
        <TouchableOpacity onPress={() => handleAddImage()}>
            <Image style={styles.image} source={{ uri: item.uri }} />
        </TouchableOpacity>
    );
}

export default ImagePreview;