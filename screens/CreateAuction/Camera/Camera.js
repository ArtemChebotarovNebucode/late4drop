import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    SafeAreaView,
    FlatList,
} from "react-native";
import { styles } from "./Camera.Styles";
import { Camera } from "expo-camera";
import Colors from "../../../constants/Colors";
import CircledButton from "../../../components/buttons/CircledButton/CircledButton";
import SvgUri from "expo-svg-uri";
import * as MediaLibrary from "expo-media-library";
import ImagePreview from "./ImagePreview/ImagePreview";
import * as ImagePicker from "expo-image-picker";

const CameraSync = ({ addImage, setIsCameraOpen, pickImage }) => {
    const [hasPermission, setHasPermission] = useState(null);
    const [images, setImages] = useState([]);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [camera, setCamera] = useState(null);
    const [flashMode, setFlashMode] = useState(Camera.Constants.FlashMode.off);

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === "granted");
        })();
        (async () => {
            const { status } = await MediaLibrary.requestPermissionsAsync();
            if (status === "granted") {
                const data = await MediaLibrary.getAssetsAsync({
                    mediaType: ["photo"],
                });
                setImages(data.assets);
            }
        })();
    }, []);

    const addingImages = async () => {
        if (images.length !== 0) {
            const data = await MediaLibrary.getAssetsAsync({
                mediaType: ["photo"],
                base64: true,
                after: images[images.length - 1].id,
            });
            if (data.assets.length > 0) {
                setImages((prev) => {
                    return prev[prev.length - 1].id ===
                        data.assets[data.assets.length - 1].id
                        ? prev
                        : [...prev, ...data.assets];
                });
            }
        }
    };
    const handleMakePicture = async () => {
        if (camera) {
            const options = {
                quality: 0.5,
                base64: true,
                skipProcessing: true,
            };
            const data = await camera.takePictureAsync(options);
            if (data.uri) {
                addImage(data);

            }
        }
    };
    const getFlashIcon = () => {
        switch (flashMode) {
            case Camera.Constants.FlashMode.on:
                return require("../../../assets/icons/flash-icon-on.svg");
            case Camera.Constants.FlashMode.off:
                return require("../../../assets/icons/flash-icon-off.svg");
            case Camera.Constants.FlashMode.auto:
                return require("../../../assets/icons/flash-icon-auto.svg");
            default:
                return;
        }
    };
    const Header = () => {
        return (
            <View>
                <SafeAreaView style={styles.wrapper}>
                    <StatusBar
                        translucent
                        backgroundColor={Colors.BLACK_BACKGROUND}
                        barStyle={"light-content"}
                    />
                </SafeAreaView>
                <View>
                    <View style={styles.header} />
                    <View style={styles.headerWrapper}>
                        <CircledButton
                            iconType={"svg"}
                            iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                            iconHeight={15}
                            iconWidth={15}
                            onPress={() => setIsCameraOpen(false)}
                            backgroundColor={Colors.GRAY_BACKGROUND}
                            fill={Colors.BLUE}
                        />
                        <View style={styles.headerContainer}>
                            <TouchableOpacity
                                onPress={() => {
                                    setFlashMode(
                                        flashMode ===
                                            Camera.Constants.FlashMode.on
                                            ? Camera.Constants.FlashMode.off
                                            : flashMode ===
                                              Camera.Constants.FlashMode.off
                                            ? Camera.Constants.FlashMode.auto
                                            : Camera.Constants.FlashMode.on
                                    );
                                }}
                            >
                                <SvgUri
                                    width={20}
                                    height={20}
                                    source={getFlashIcon()}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {
                                    setType(
                                        type === Camera.Constants.Type.back
                                            ? Camera.Constants.Type.front
                                            : Camera.Constants.Type.back
                                    );
                                }}
                            >
                                <SvgUri
                                    width={19}
                                    height={19}
                                    source={require("../../../assets/icons/flip-icon.svg")}
                                />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => pickImage()}>
                            <Text style={styles.text}>WYBIERZ</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    };
    const Bottom = () => {
        return (
            <View style={styles.buttonContainer}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ padding: 10 }}>
                        <FlatList
                            horizontal
                            onEndReached={() => addingImages()}
                            showsHorizontalScrollIndicator={false}
                            legacyImplementation={false}
                            data={images}
                            renderItem={({ item, index }) => (
                                <ImagePreview addImage={addImage} item={item} />
                            )}
                            keyExtractor={(image) => image.id}
                        />
                    </View>
                </SafeAreaView>
                <TouchableOpacity
                    onPress={() => handleMakePicture()}
                    style={styles.button}
                >
                    <View style={styles.cameraWrapper}>
                        <SvgUri
                            width={36}
                            height={35}
                            source={require("../../../assets/icons/camera-icon.svg")}
                        />
                    </View>
                </TouchableOpacity>
            </View>
        );
    };
    return (
        <View style={styles.container}>
            <Camera
                flashMode={flashMode}
                ref={(ref) => setCamera(ref)}
                style={styles.camera}
                type={type}
            >
                {Header()}
                {Bottom()}
            </Camera>
        </View>
    );
};

export default CameraSync;
