import { StyleSheet, Platform, StatusBar } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    height: '100%',
    width: '100%'
  },
  camera: {
    flex: 1,
  },

  buttonContainer: {
    flex: 1,
    backgroundColor: "transparent",
    width: "100%",
    marginBottom: 20,
    position: 'absolute',
    bottom: 0
  },
  button: {
    alignItems: "center",
    width: "100%",
    justifyContent: "center",
  },
  wrapper: {
    flex: 1,
    backgroundColor: Colors.BLACK_BACKGROUND,
  },
  headerContainer: {
    flexDirection: "row",
    width: 70,
    justifyContent: "space-between",
  },
  header: {
    width: "100%",
    backgroundColor: Colors.BLACK,
  },
  text: {
    color: Colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat_700Bold",
  },
  headerWrapper: {
    position: "absolute",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 20 : 10,
    paddingHorizontal: 20,
  },
  cameraWrapper: {
    borderWidth: 1,
    borderColor: Colors.WHITE,
    borderRadius: 36,
    padding: 8,
  },
});
