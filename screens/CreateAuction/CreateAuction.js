import React, { useEffect, useState } from "react";
import { Alert, ScrollView, View } from "react-native";
import { styles } from "./CreateAuction.Styles";
import Images from "./Images/Images";
import Header from "./Header/Header";
import Information from "./Information/Information";
import StyledStatusBar from "../../components/partials/StyledStatusBar/StyledStatusBar";
import Colors from "../../constants/Colors";
import CameraSync from "./Camera/Camera";
import AuctionService from "../../services/auctionService";
import { useAuth } from "../../contexts/AuthProvider/AuthProvider";
import * as ImagePicker from "expo-image-picker";
import {connect} from "react-redux";
import {addAuctionItems} from "../../store/actions/productAction";
import useDebounce from "../../hooks/Debounce";

const CreateAuction = (props) => {
    const {addAuctionItems, navigation, onBackPress} = props;

    const [isCameraOpen, setIsCameraOpen] = useState(false);
    const auctionService = new AuctionService();
    const day = new Date();
    const [images, setImages] = useState([]);
    const [isAuctionCreation, setIsAuctionCreating] = useState(false);
    const debouncedValue = useDebounce(isAuctionCreation, 2000);
    const { userToken, refreshMyActiveProducts, currentUser } = useAuth();
    const [auction, setAuction] = useState({
        title: "",
        endsAt: new Date(day.setDate(day.getDate() + 7)),
        minPrice: 0,
        description: "",
        brand: "",
        size: "",
        shipmentToEurope: 15,
        shipmentToPoland: 15,
        condition: "",
        color: "",
        category: "",
        photoStrings: [],
    });
    console.log(auction)
    const handleAuctionChange = (name, value) => {
        setAuction((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const addImage = (image) => {
        setIsCameraOpen(false);
        setImages((prev) => [...prev, image]);
    };
    const removeImage = (uri) => {
        setImages((prev) => prev.filter((image) => image.uri !== uri));
    };
    const handleCreateAuction = () => {
            auctionService.addAuction(userToken, auction).then((response) => {
                setIsAuctionCreating(false)
                if (response.data.data !== undefined) {
                    // addAuctionItems(response.data.data)
                    refreshMyActiveProducts(currentUser.id)
                    navigation.navigate("ProductDetails", {
                        type: "AUCTION",
                        productId: response.data.data.id,
                        isJustCreated: true,
                    });
                }
            });
    };
    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            base64: true,
            aspect: [4, 3],
            quality: 1,
        });
        if (!result.cancelled) {
            addImage(result);
        }
    };
    const handlePressAddImage = () => {
        Alert.alert(
            "Wybierz z",
            "",
            [
                {
                    text: "Galerii",
                    onPress: () => {
                        pickImage();
                    },
                },
                {
                    text: "Aparatu",
                    onPress: () => {
                        setIsCameraOpen(true);
                    },
                },
            ],
            { cancelable: false }
        );
    };

    useEffect(() => {
        if (isCameraOpen) {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }
    }, [isCameraOpen]);
    useEffect(() => {
        const baseImages = images.reduce((prev, next) => {
            return prev.concat(
                `data:image/png;base64,${next.base64.toString()}`
            );
        }, []);
        handleAuctionChange("photoStrings", baseImages);
    }, [images]);
    useEffect(() => {
        (async () => {
            const {
                status,
            } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        })();
    }, []);

    useEffect(() => {
        if(isAuctionCreation){
            handleCreateAuction()
        }
    }, [debouncedValue])

    return (
        <View style={styles.wrapper}>
            <StyledStatusBar backgroundColor={Colors.BLACK_BACKGROUND} />
            <Header onBackPress={onBackPress} />
            <ScrollView
                style={{ width: "100%" }}
                showsVerticalScrollIndicator={false}
            >
                <View style={styles.container}>
                    <Images
                        removeImage={removeImage}
                        images={images}
                        handlePressAddImage={handlePressAddImage}
                    />
                    <Information
                        auction={auction}
                        handleAuctionChange={handleAuctionChange}
                        handleCreateAuction={handleCreateAuction}
                        setIsAuctionCreating={setIsAuctionCreating}
                    />
                </View>
            </ScrollView>
            {isCameraOpen ? (
                <CameraSync
                    addImage={addImage}
                    setIsCameraOpen={setIsCameraOpen}
                    pickImage={pickImage}
                />
            ) : (
                <></>
            )}
        </View>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        addAuctionItems: (data) => dispatch(addAuctionItems(data))
    };
};


export default connect(null, mapDispatchToProps)(CreateAuction);
