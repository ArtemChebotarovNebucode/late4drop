const initState = {
    products: [],
    searchedProducts: [],
    lastSearchingProducts: [],
    lastAddedProducts: [],
    auctions: [],
};

const productReducer = (state = initState, action) => {
    switch (action.type) {
        case "SET_PRODUCTS":
            return (state = {
                ...state,
                products: action.payload,
            });
        case "ADD_PRODUCTS":
            return (state = {
                ...state,
                products: state.products.concat(action.payload),
            });
        case "SET_SEARCHED_PRODUCTS":
            return (state = {
                ...state,
                searchedProducts: action.payload,
            });
        case "ADD_SEARCHED_PRODUCTS":
            return (state = {
                ...state,
                searchedProducts: state.searchedProducts.concat(action.payload),
            });
        case "ADD_LAST_SEARCHING_PRODUCTS":
            return (state = {
                ...state,
                lastSearchingProducts: state.lastSearchingProducts.concat(
                    action.payload
                ),
            });
        case "ADD_LAST_ADDED_PRODUCTS":
            return (state = {
                ...state,
                lastAddedProducts: state.lastAddedProducts.concat(
                    action.payload
                ),
            });
        case "DELETE_PRODUCT":
            return (state = {
                ...state,
                searchedProducts: state.searchedProducts.filter(
                    (item) => item.id !== action.payload.id
                ),
            });
        case "SET_AUCTIONS":
            return (state = {
                ...state,
                auctions: action.payload,
            });
        case "ADD_AUCTION":
            return (state = {
                ...state,
                auctions: state.auctions?.unshift(action.payload),
            });
        case "DELETE_AUCTION":
            return (state = {
                ...state,
                auctions: state.auctions?.filter(
                    (item) => item.id !== action.payload.id
                ),
            });
        default:
            return state;
    }
};

export default productReducer;
