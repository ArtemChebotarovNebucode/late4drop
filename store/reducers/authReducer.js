const initState = {
    loginResponse: null
};

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return state = {
                ...state,
                loginResponse: action.payload
            };
        case 'REGISTER':
            return state = {
                ...state,
                response: action.payload
            };
        default:
            return state;
    }
};

export default authReducer;
