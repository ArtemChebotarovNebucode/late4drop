const setProducts = (response) => ({
    type: "SET_PRODUCTS",
    payload: response
});
const setSearchedProducts = (response) => ({
    type: "SET_SEARCHED_PRODUCTS",
    payload: response
});
const addProducts = (response) => ({
    type: "ADD_PRODUCTS",
    payload: response
});
const addSearchedProducts = (response) => ({
    type: "ADD_SEARCHED_PRODUCTS",
    payload: response
});

const addLastSearchingProducts = (response) => ({
    type: "ADD_LAST_SEARCHING_PRODUCTS",
    payload: response
});
const addLastAddedProducts = (response) => ({
    type: "ADD_LAST_ADDED_PRODUCTS",
    payload: response
});
const deleteProduct = (response) => ({
    type: "DELETE_PRODUCT",
    payload: response
});
const setAuctions = (response) => ({
    type: "SET_AUCTIONS",
    payload: response
});
const addAuctionItems = (response) => ({
    type: "ADD_AUCTION",
    payload: response
});
const deleteAuction = (response) => ({
    type: "DELETE_AUCTION",
    payload: response
});


export { setProducts, addProducts, addLastSearchingProducts, addLastAddedProducts, setSearchedProducts, addSearchedProducts, deleteProduct, setAuctions, addAuctionItems, deleteAuction };
