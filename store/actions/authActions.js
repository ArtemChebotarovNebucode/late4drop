const loginUserAction = (response) => ({
   type: "LOGIN",
   payload: response
});
const registerUserAction = (response) => ({
   type: "REGISTER",
   payload: response
});


export { loginUserAction, registerUserAction };
