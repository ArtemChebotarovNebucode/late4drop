import {Dimensions, Platform} from "react-native";
import Constants from "expo-constants";

export default {
    TOP_OFFSET: Platform.OS === "android" ? Constants.statusBarHeight : 0,
    BOTTOM_OFFSET: Platform.OS === "android" ?
        Dimensions.get('window').height / 20 :
        Dimensions.get('window').height / 10,
}