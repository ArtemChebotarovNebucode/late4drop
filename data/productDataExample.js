export const product = {
    title: 'Vintage Souick Original Casual',
    subtitle: 'Wear Sweatshirt',
    sold: false,
    late4dropRecommended: true,
    size: 'L',
    condition: 'Używane',
    color: 'żółty',
    current_price: 200,
    original_price: 200,
    user_rating: 5.0,
    delivery: {
        poland: 15,
        europe: 35,
    },
    user_avatar: require('../assets/icons/avatar-icon.png'),
    user_name: 'Jonathan Henderson',
    description:
        'Aliquam elementum metus eu neque tincidunt maximus. Nunc rhoncus felis vel euismod dignissim. In et semper ante, ac venenatis justo. Phasellus ut auctor dolor, in mollis metus. Vivamus nec bibendum mauris. Mauris at orci a nisi ultrices rutrum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    images: [
        {
            id: 0,
            image: require('../assets/images/product-photos/photo_0.jpg'),
        },
        {
            id: 1,
            image: require('../assets/images/product-photos/photo_1.jpg'),
        },
        {
            id: 2,
            image: require('../assets/images/product-photos/photo_2.jpg'),
        },
        {
            id: 3,
            image: require('../assets/images/product-photos/photo_3.jpg'),
        },
        {
            id: 4,
            image: require('../assets/images/product-photos/photo_4.jpg'),
        },
    ],
    user_opinions: [
        {
            id: 0,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 1,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 2,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 3,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 4,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 5,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 6,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 7,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 8,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 9,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 10,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 11,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 12,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 13,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
        {
            id: 14,
            opinion:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        },
    ],
};