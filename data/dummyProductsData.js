export const products = [
    {
        id: '0',
        saleDate: "27.02.2021",
        name: 'Vintage Sweatshirt',
        price: '200 zł',
        size: 'L',
        likesCount: 78,
        color: 'czarny',
        img: require('../assets/images/product-photos/photo_0.jpg')
    },
    {
        id: '1',
        saleDate: "24.02.2021",
        name: 'Adidas Tee',
        price: '200 zł',
        size: 'M',
        likesCount: 12,
        color: 'bronzowy',
        img: require('../assets/images/adidas-tee.jpg')
    },
    {
        id: '2',
        saleDate: "18.02.2021",
        name: 'Nike Airmax \'95',
        price: '450 zł',
        size: '42',
        likesCount: 26,
        color: 'szary',
        img: require('../assets/images/nike-airmax.jpg')
    },
    {
        id: '3',
        saleDate: "12.02.2021",
        name: 'North Face jacket',
        price: '599 zł',
        size: 'L',
        likesCount: 3,
        color: 'czarny',
        img: require('../assets/images/northface-jacket.jpg')
    },
    {
        id: '4',
        saleDate: "09.02.2021",
        name: 'Supreme T-Shirt',
        price: '800 zł',
        size: 'L',
        likesCount: 19,
        color: 'biały',
        img: require('../assets/images/supreme-tshirt.jpg')
    },
    {
        id: '5',
        saleDate: "13.01.2021",
        name: 'Yeezy Boost 350',
        price: '900 zł',
        size: '45',
        likesCount: 56,
        color: 'zielony',
        img: require('../assets/images/yeezy-350.jpg')
    },
];