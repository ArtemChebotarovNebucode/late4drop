import firebase from 'firebase/app'
import 'firebase/auth'

const app = firebase.initializeApp({
    apiKey: "AIzaSyAjuWCt2kZjsCsHlRVrbfNNxG7t9-IaJfE",
    authDomain: "late4drop-d8e69.firebaseapp.com",
    projectId: "late4drop-d8e69",
    storageBucket: "late4drop-d8e69.appspot.com",
    messagingSenderId: "233061496203",
    appId: "1:233061496203:web:8b28ae96ea1f7bdfceab8e",
    measurementId: "G-TDDVNMH3JF"
});
// const app = firebase.initializeApp({
//     apiKey: "AIzaSyB-7_aRjBc4ciTq9N5hwnvW3Pa2YsBUmp4",
//     authDomain: "skrr-index-online.firebaseapp.com",
//     databaseURL: "https://skrr-store-online.firebaseio.com",
//     projectId: "skrr-index-online",
//     storageBucket: "skrr-index-online.appspot.com",
//     messagingSenderId: "1017507964561",
//     appId: "1:1017507964561:web:c76e30cbaa37eb8a13c130",
//     measurementId: "G-RWKVH6RMQB",
// });

export const auth = app.auth()
export default app