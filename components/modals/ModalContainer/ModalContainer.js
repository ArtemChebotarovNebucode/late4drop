import React from "react";
import {
    View,
    Modal,
    TouchableWithoutFeedback,
} from "react-native";
import { styles } from './ModalContainer.Styles'

const ModalContainer = ({ isVisible, closeModal, children }) => {
    const handleTouchOutside = (onTouch) => {
        const view = <View style={styles.wrapper} />;
        if (!onTouch) return view;

        return (
            <TouchableWithoutFeedback onPress={onTouch} style={styles.wrapper}>
                {view}
            </TouchableWithoutFeedback>
        );
    };
    return (
        <Modal transparent visible={isVisible} animationType="slide">
            <View
                style={styles.container}
            >
                {handleTouchOutside(closeModal)}
                {children}
            </View>
        </Modal>
    );
};

export default ModalContainer;
