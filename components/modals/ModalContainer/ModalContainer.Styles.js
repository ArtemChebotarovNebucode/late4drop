import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000AA",
    justifyContent: "flex-end",
  },
  wrapper: {
    flex: 1,
    width: "100%",
    maxHeight: "80%"
  },
});
