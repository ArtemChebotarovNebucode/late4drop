import {StyleSheet} from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    modal: {
        backgroundColor: "#fff",
        width: "100%",
        paddingHorizontal: 20,
        maxHeight: '95%'
    },
    wrapper: {
        flexDirection: "row",
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 25,
        position: 'relative'
    },
    header: {
        alignItems: "center",
        justifyContent: "center",
    },
    title:{
        fontSize: 18,
        color: Colors.SHARK_TEXT,
        marginTop: 0,
        marginRight: 'auto',
        marginBottom: 0,
        marginLeft: 'auto',
        fontFamily: "Montserrat_700Bold"
    },
    icon: {
        position: 'absolute',
        top: -17,
        width: 40,
        height: 40,
    }
});
