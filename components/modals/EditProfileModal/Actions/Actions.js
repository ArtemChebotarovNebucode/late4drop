import React, { useState } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../../../../constants/Colors";
import * as ImagePicker from "expo-image-picker";
import { useAuth } from "../../../../contexts/AuthProvider/AuthProvider";

const Actions = ({ setAvatar, avatar }) => {
    const { currentUser } = useAuth();

    const [avatarIsChosen, setAvatarIsChosen] = useState(false);

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [3, 3],
            quality: 1,
            base64: true,
        });
        if (!result.cancelled) {
            setAvatarIsChosen(true);
            setAvatar(result);
        }
    };
    return (
        <View style={styles.container}>
            <Image
                style={styles.image}
                source={
                    currentUser?.photoURL && !avatarIsChosen
                        ? { uri: currentUser?.photoURL }
                        : avatar
                }
            />
            <TouchableOpacity
                style={[styles.wrapper, { borderColor: Colors.BLUE_TEXT }]}
                onPress={pickImage}
            >
                <Text style={[styles.text, { color: Colors.BLUE_TEXT }]}>
                    ZMIEŃ
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => {
                    setAvatarIsChosen(true);
                    setAvatar(
                        require("../../../../assets/images/profile-photo-standard.png")
                    );
                }}
            >
                <Text
                    style={[
                        styles.text,
                        { color: Colors.RED_TEXT, marginBottom: 10 },
                    ]}
                >
                    USUŃ
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export const styles = StyleSheet.create({
    container: {
        alignItems: "center",
    },
    image: {
        width: 88,
        height: 88,
        marginBottom: 15,
        borderRadius: 50,
    },
    wrapper: {
        borderWidth: 1,
        paddingHorizontal: 29,
        paddingVertical: 15,
        borderRadius: 3,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
    },
    text: {
        fontSize: 12,
        fontFamily: "Montserrat_500Medium",
    },
});

export default Actions;