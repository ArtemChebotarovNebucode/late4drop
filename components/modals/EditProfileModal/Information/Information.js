import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import Colors from "../../../../constants/Colors";
import PrimaryButton from "../../../buttons/PrimaryButton/PrimaryButton";
import { useAuth } from "../../../../contexts/AuthProvider/AuthProvider";
import UserService from "../../../../services/userService";

const Information = ({ avatar, closeModal }) => {
    const userService = new UserService();
    const { currentUser, refreshMyInfo, userToken } = useAuth();

    const [providedData, setProvidedData] = useState({
        displayName: currentUser?.displayName ?? "",
        username: currentUser?.username ?? "",
        email: currentUser?.email ?? "",
        phoneNumber: currentUser?.phoneNumber ?? "",
        password: "",
    });

    const submitProfileEdition = async () => {
        await userService.changeAvatar(avatar.base64);
        await userService.editMyInfo({
            phoneNumber: providedData.phoneNumber,
            correspondenceEmail: providedData.email,
            bio: "",
            username: providedData.username,
            displayName: providedData.displayName,
        });
        await refreshMyInfo(userToken);
        closeModal();
    };

    return (
        <View>
            <Text style={styles.label}>Imię i nazwisko</Text>
            <TextInput
                style={styles.input}
                value={providedData.displayName}
                onChangeText={(displayName) =>
                    setProvidedData({
                        ...providedData,
                        displayName: displayName,
                    })
                }
                autoCapitalize="none"
                autoCorrect={false}
                placeholder={"Imię i nazwisko"}
            />

            <Text style={styles.label}>Nazwa użytkownika</Text>
            <TextInput
                style={styles.input}
                value={providedData.username}
                onChangeText={(username) =>
                    setProvidedData({ ...providedData, username: username })
                }
                autoCapitalize="none"
                autoCorrect={false}
                placeholder={"Nazwa"}
            />

            <Text style={styles.label}>Adres e-mail</Text>
            <TextInput
                style={styles.input}
                value={providedData.email}
                onChangeText={(email) =>
                    setProvidedData({ ...providedData, email: email })
                }
                editable={false}
                autoCapitalize="none"
                autoCorrect={false}
                placeholder={"Email"}
            />

            <Text style={styles.label}>Numer telefonu</Text>
            <TextInput
                style={styles.input}
                value={providedData.phoneNumber}
                onChangeText={(phoneNumber) =>
                    setProvidedData({
                        ...providedData,
                        phoneNumber: phoneNumber,
                    })
                }
                autoCapitalize="none"
                autoCorrect={false}
                placeholder={"Numer"}
                keyboardType={"numeric"}
            />

            <Text style={styles.label}>Hasło</Text>
            <View style={styles.wrapper}>
                <TextInput
                    secureTextEntry={true}
                    textContentType={"password"}
                    style={[styles.input, { flex: 4, marginRight: 3 }]}
                    onChangeText={(password) =>
                        setProvidedData({ ...providedData, password: password })
                    }
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder={"Hasło"}

                />
                <TouchableOpacity style={styles.container}>
                    <Text style={[styles.text, { color: Colors.BLUE_TEXT }]}>
                        ZMIEŃ
                    </Text>
                </TouchableOpacity>
            </View>

            <PrimaryButton
                additionalStyle={{ marginTop: 25, marginBottom: 180 }}
                label={"ZAPISZ"}
                onPress={submitProfileEdition}
            />
        </View>
    );
};

export const styles = StyleSheet.create({
    input: {
        height: 44,
        width: "100%",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_LINE,
        marginTop: 15,
        marginBottom: 15,
        paddingHorizontal: 14,
        fontFamily: "Montserrat_400Regular",
    },
    label: {
        fontSize: 12,
        color: Colors.GRAY_LABEL,
        fontFamily: "Montserrat_500Medium",
    },
    wrapper: {
        flexDirection: "row",
        flex: 1,
        alignItems: "center",
    },
    container: {
        borderWidth: 1,
        height: 44,
        borderRadius: 3,
        justifyContent: "center",
        alignItems: "center",
        borderColor: Colors.BLUE_TEXT,
        flex: 1,
    },
    text: {
        fontSize: 12,
        fontFamily: "Montserrat_500Medium",
    },
});

export default Information;
