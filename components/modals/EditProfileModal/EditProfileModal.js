import React, { useState } from "react";
import { ScrollView, Text, View } from "react-native";
import { styles } from "./EditProfileModal.Styles";
import CircledButton from "../../buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";
import ModalContainer from "../ModalContainer/ModalContainer";
import Actions from "./Actions/Actions";
import Information from "./Information/Information";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";

const EditProfileModal = ({ closeModal, isVisible}) => {
    const { currentUser, userToken } = useAuth();

    const [avatar, setAvatar] = useState({
        avatar: currentUser?.photoURL
          ? currentUser?.photoURL
          : require("../../../assets/images/profile-photo-standard.png")
        }
    );

    return (
        <ModalContainer closeModal={closeModal} isVisible={isVisible}>
            <View style={styles.modal}>
                <View style={styles.wrapper}>
                    <CircledButton
                        iconType={"svg"}
                        iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                        iconHeight={15}
                        iconWidth={15}
                        onPress={() => closeModal()}
                        backgroundColor={Colors.GRAY_BACKGROUND}
                        fill={Colors.BLUE}
                        additionalStyle={styles.icon}
                    />
                    <Text style={styles.title}>Edycja profilu</Text>
                </View>
                <ScrollView
                    style={{ width: "100%" }}
                    showsVerticalScrollIndicator={false}
                >
                    <Actions avatar={avatar} setAvatar={setAvatar} />
                    <Information avatar={avatar} closeModal={closeModal}/>
                </ScrollView>
            </View>
        </ModalContainer>
    );
};

export default EditProfileModal;
