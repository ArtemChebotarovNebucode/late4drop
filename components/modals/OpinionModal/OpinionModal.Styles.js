import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  modal: {
    backgroundColor: "#fff",
    width: "100%",
    paddingHorizontal: 20,
    maxHeight: "93%"
  },
  header: {
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 15
  },
  title: {
    fontSize: 18,
    fontFamily: "Montserrat_700Bold",
    paddingTop: 30,
    paddingBottom: 18,
  },
});
