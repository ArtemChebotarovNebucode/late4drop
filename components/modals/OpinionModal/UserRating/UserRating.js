import React from "react";
import { Text, View } from "react-native";
import { styles } from "./UserRating.Styles";
import Colors from "../../../../constants/Colors";
import StarRating from "react-native-star-rating";

const UserRating = ({ averageRating }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Ocena użytkownika</Text>
            <View style={{flexDirection: "row", alignContent: "center"}}>
                <StarRating
                    disabled={true}
                    maxStars={5}
                    rating={averageRating}
                    fullStarColor={Colors.YELLOW_BACKGROUND}
                    starSize={25}
                    emptyStarColor={Colors.GRAY_BORDER}
                    containerStyle={{
                        alignSelf: "flex-start",
                    }}
                    starStyle={{
                        marginRight: 2,
                    }}
                />
                <Text style={styles.rating}>
                    {(Math.round(averageRating * 10) / 10).toFixed(1)}
                </Text>
            </View>
        </View>
    );
};

export default UserRating;