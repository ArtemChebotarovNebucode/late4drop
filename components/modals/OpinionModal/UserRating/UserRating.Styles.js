import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom: 10,
        flexWrap: "wrap"
    },
    text: {
        fontSize: 14,
        fontFamily: "Montserrat_400Regular",
        color: Colors.GRAY_LABEL,
        marginBottom: 10
    },
    rating: {
        marginTop: 4,
        marginLeft: 5,
        fontSize: 16,
        fontFamily: "Montserrat_500Medium",
        color: Colors.GRAY_LABEL,
    },
});
