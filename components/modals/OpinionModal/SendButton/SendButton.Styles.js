import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.LIGHT_BLUE_BACKGROUND,
        borderWidth: 2,
        borderColor: Colors.BLUE_TEXT,
        borderRadius: 2,
        marginVertical: 15,
        width: 126,
        height: 26,
        alignItems: "center",
        justifyContent: "center",
    },
    text: {
        fontSize: 11,
        color: Colors.BLUE_TEXT,
        fontFamily: "Montserrat_400Regular",
    },
});
