import React from "react";
import {TouchableOpacity, Text} from "react-native";
import {styles} from './SendButton.Styles'

const SendButton = ({ openMessageModal }) => {
    return (
        <TouchableOpacity onPress={() => openMessageModal()} style={styles.container}>
            <Text style={styles.text}>Wyślij wiadomość</Text>
        </TouchableOpacity>
    )
}

export default SendButton;