import React from "react";
import { Platform, ScrollView, Text, View } from "react-native";
import { styles } from "./OpinionModal.Styles";
import ModalContainer from "../ModalContainer/ModalContainer";
import UserBlock from "./UserBlock/UserBlock";
import SendButton from "./SendButton/SendButton";
import UserRating from "./UserRating/UserRating";
import Sender from "./Sender/Sender";
import CommentBlock from "./CommentBlock/CommentBlock";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import { DismissKeyboard } from "../../index";
import Offsets from "../../../constants/Offsets";
import CircledButton from "../../buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";

const OpinionModal = ({
    closeModal,
    isVisible,
    averageRating,
    openMessageModal,
    imageUrl,
    name
}) => {
    return (
        <ModalContainer closeModal={closeModal} isVisible={isVisible}>
            <DismissKeyboard>
                <ScrollView style={styles.modal}>
                    <View style={styles.header}>
                        <Text style={styles.title}>Udziel opinii</Text>
                        <View
                            style={{
                                position: "absolute",
                                width: "100%",
                                flexDirection: "row",
                                alignItems: "center",
                                height: "100%",
                            }}
                        >
                            <CircledButton
                                iconType={"svg"}
                                iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                                iconHeight={15}
                                iconWidth={15}
                                fill={Colors.BLUE}
                                backgroundColor={Colors.GRAY_BACKGROUND}
                                additionalStyle={{ marginTop: 10 }}
                                onPress={closeModal}
                            />
                        </View>
                    </View>
                    <UserBlock imageUrl={imageUrl} name={name} />
                    <SendButton openMessageModal={openMessageModal} />
                    <UserRating averageRating={averageRating} />
                    <Sender />
                    <CommentBlock />
                    <PrimaryButton
                        onPress={() => closeModal()}
                        additionalStyle={{ marginBottom: 10 }}
                        label={"DODAJ OPINIĘ"}
                    />

                    <View
                        style={
                            Platform.OS === "android"
                                ? { height: Offsets.BOTTOM_OFFSET }
                                : {
                                      height: Offsets.BOTTOM_OFFSET - 40,
                                  }
                        }
                    />
                </ScrollView>
            </DismissKeyboard>
        </ModalContainer>
    );
};
export default OpinionModal;
