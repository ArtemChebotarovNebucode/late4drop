import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },
  image: {
    width: 40,
    height: 40,
  },
  text: {
    fontSize: 13,
    fontFamily: "Montserrat_500Medium",
    color: Colors.SANTAS_GRAY_TEXT,
    paddingLeft: 14,
  },
});
