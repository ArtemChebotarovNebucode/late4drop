import React from "react";
import {View, Image, Text} from "react-native";
import {styles} from './Sender.Styles'

const Sender = () => {
    return (
        <View style={styles.container}>
            <Image style={styles.image} source={require('../../../../assets/images/profile-photo2.png')}/>
            <Text style={styles.text}>@muhammad4533</Text>
        </View>
    )
}

export default Sender;