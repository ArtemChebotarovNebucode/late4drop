import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: Colors.LIGHT_GRAY_BACKGROUND,
    height: 60,
    borderRadius: 2,
    alignItems: "center",
  },
  image: {
    width: 45,
    height: 45,
    marginHorizontal: 15,
    borderRadius: 23
  },
  text: {
    fontSize: 14,
    color: Colors.BLACK_TEXT,
    fontFamily: "Montserrat_700Bold",
  },
});
