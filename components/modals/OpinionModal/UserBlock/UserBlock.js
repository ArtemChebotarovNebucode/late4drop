import React from "react";
import { View, Image, Text } from "react-native";
import { styles } from "./UserBlock.Styles";

const UserBlock = ({imageUrl, name}) => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={imageUrl ? { uri: imageUrl }
            : require("../../../../assets/images/profile-photo-standard.png")}
      />
      <Text style={styles.text}>{name}</Text>
    </View>
  );
};

export default UserBlock;
