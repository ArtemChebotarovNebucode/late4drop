import React from "react";
import { View, Text, TextInput } from "react-native";
import { styles } from "./CommentBlock.Styles";

const CommentBlock = () => {
  return (
    <View>
      <Text style={styles.text}>Komentarz do zamówienia</Text>
      <TextInput
        multiline={true}
        numberOfLines={8}
        textAlignVertical={"top"}
        placeholder="Treść komentarza"
        style={[styles.input, { height: 182, paddingTop: 14 }]}
      />
    </View>
  );
};

export default CommentBlock;
