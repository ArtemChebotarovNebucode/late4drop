import React from "react";
import { Modal, Text, View } from "react-native";
import { styles } from "./CustomAlert.Styles";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import SecondaryButton from "../../buttons/SecondaryButton/SecondaryButton";

const CustomAlert = ({
    isVisible,
    message,
    primaryLabel,
    secondaryLabel,
    primaryOnPress,
    secondaryOnPress,
    title,
}) => {
    return (
        <Modal transparent visible={isVisible}>
            <View style={styles.wrapper}>
                <View style={styles.container}>
                    {title ? <Text style={styles.title}>{title}</Text> : null}
                    <Text
                        style={
                            title ? styles.message : styles.messageWithoutTitle
                        }
                    >
                        {message}
                    </Text>
                    <PrimaryButton
                        label={primaryLabel}
                        onPress={primaryOnPress}
                        additionalStyle={{ marginTop: 20 }}
                    />
                    <SecondaryButton
                        label={secondaryLabel}
                        onPress={secondaryOnPress}
                        additionalStyle={{ marginTop: 20 }}
                    />
                </View>
            </View>
        </Modal>
    );
};

export default CustomAlert;
