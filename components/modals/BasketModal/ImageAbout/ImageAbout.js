import React from "react";
import { View, Image, Text } from "react-native";
import { styles } from "./ImageAbout.Styles";

const ImageAbout = ({ product }) => {
  return (
    <View style={styles.container}>
      <Image source={{uri: product.images[3].url}} style={styles.image} />
      <View style={styles.info}>
        <Text style={styles.title}>{product.title}</Text>
        <Text style={styles.subtitle}>{product.subtitle}</Text>
      </View>
    </View>
  );
};
export default ImageAbout;
