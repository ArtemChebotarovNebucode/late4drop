import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingBottom: 3,
  },
  image: {
    width: 82,
    height: 82,
  },
  info: {
    paddingLeft: 10,
    flexGrow: 1,
    flex: 1,
  },
  title: {
    fontSize: 18,
    fontFamily: "Montserrat_400Regular",
  },
  subtitle: {
    fontSize: 18,
    fontFamily: "Montserrat_700Bold",
    paddingTop: 8,
  },
});
