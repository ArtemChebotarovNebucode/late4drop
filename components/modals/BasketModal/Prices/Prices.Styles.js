import {StyleSheet} from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 1,
        borderColor: Colors.BLUE,
        marginBottom: 23

    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    text: {
        fontSize: 12,
        fontFamily: 'Montserrat_500Medium',
        color: Colors.DARK_GRAY_TEXT,
        paddingBottom: 25
    },
    textMain:{
        fontFamily: 'Montserrat_700Bold'
    }
});
