import React from "react";
import { View, Text } from "react-native";
import { styles } from "./Prices.Styles";

const Prices = ({ product, isVerificationChecked }) => {
    return (
        <View>
            <View style={styles.container}>
                <View style={styles.item}>
                    <Text style={styles.text}>Koszt przedmiotu</Text>
                    <Text
                        style={styles.text}
                    >{`${product?.price ?? "0"} zł`}</Text>
                </View>
                {isVerificationChecked ? (
                    <View style={styles.item}>
                        <Text style={styles.text}>Weryfikacja</Text>
                        <Text style={styles.text}>+15 zł</Text>
                    </View>
                ) : (
                    <></>
                )}

                <View style={styles.item}>
                    <Text style={styles.text}>Dostawa</Text>
                    <Text style={styles.text}>+15 zł</Text>
                </View>
            </View>
            <View style={styles.item}>
                <Text style={styles.textMain}>Suma</Text>
                <Text style={styles.textMain}>{parseInt(product?.price) + 15 + (isVerificationChecked ? + 15 : 0)} zł</Text>
            </View>
        </View>
    );
};

export default Prices;
