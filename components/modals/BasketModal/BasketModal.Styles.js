import {Platform, StyleSheet} from "react-native";
import Constants from "expo-constants";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    modal: {
        backgroundColor: "#fff",
        width: "100%",
        paddingHorizontal: 20,
        maxHeight: "83%"
    },
    wrapper: {
        flexDirection: "row",
        alignItems: 'center',
        paddingTop: 26,
        paddingBottom: 36
    },
    header: {
        alignItems: "center",
        justifyContent: "center",
    },
    title:{
        fontSize: 18,
        color: Colors.SHARK_TEXT,
        marginTop: 0,
        marginRight: 'auto',
        marginBottom: 0,
        marginLeft: 'auto',
        fontFamily: "Montserrat_700Bold"
    }
});
