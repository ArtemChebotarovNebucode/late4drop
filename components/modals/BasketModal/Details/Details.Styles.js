import {StyleSheet} from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    title: {
        fontSize: 12,
        fontFamily: 'Montserrat_500Medium',
        color: Colors.BLUE_TEXT,
        height: 15,
        marginBottom: 12
    }
});
