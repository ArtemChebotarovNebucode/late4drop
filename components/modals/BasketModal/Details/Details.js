import React from "react";
import {View, Text} from "react-native";
import {styles} from './Details.Styles'
import {product} from "../../../../data/productDataExample";
import ProductTag from "../../../../screens/ProductDetailsScreen/ProductTag/ProductTag";

const Details = ({product}) => {
    return (
        <View>
            <View style={{ display: 'flex', flexDirection: 'row', marginBottom: 22, marginTop: 10 }}>
                {product?.categories[0] && <ProductTag title={product?.categories[0]} />}
                {product?.categories[1] && <ProductTag title={product?.categories[1]} condition={true} />}
                {product?.categories[2] && <ProductTag title={product?.categories[2]} color={true} />}
            </View>
        </View>
    )
}

export default Details;