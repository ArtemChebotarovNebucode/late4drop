import React from "react";
import { ScrollView, Text, View } from "react-native";
import { styles } from "./BasketModal.Styles";
import ModalContainer from "../ModalContainer/ModalContainer";
import CircledButton from "../../buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";
import ImageAbout from "./ImageAbout/ImageAbout";
import Details from "./Details/Details";
import Prices from "./Prices/Prices";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import Offsets from "../../../constants/Offsets";
import { useNavigation } from "@react-navigation/native";

const BasketModal = ({
    closeModal,
    isVisible,
    product,
    setIsCongratsAlertOpen,
    isVerificationChecked,
    bankUrl
}) => {
    const navigation = useNavigation();
    return (
        <ModalContainer closeModal={closeModal} isVisible={isVisible}>
            <ScrollView style={styles.modal}>
                <View style={styles.wrapper}>
                    <CircledButton
                        iconType={"svg"}
                        iconSource={require("../../../assets/icons/arrow-left.svg")}
                        iconHeight={15}
                        iconWidth={15}
                        onPress={() => closeModal()}
                        backgroundColor={Colors.GRAY_BACKGROUND}
                        fill={Colors.BLUE}
                    />
                    <Text style={styles.title}>WYBRANY PRZEDMIOT</Text>
                </View>
                <ImageAbout product={product} />
                <Details product={product}/>
                <Prices
                    product={product}
                    isVerificationChecked={isVerificationChecked}
                />
                <PrimaryButton
                    onPress={() => {
                        closeModal();
                        // setIsCongratsAlertOpen();
                        navigation.navigate("WebView", {
                            url: bankUrl,
                            isPayments: true
                        });
                    }}
                    additionalStyle={{
                        marginTop: 30,
                        marginBottom: Offsets.BOTTOM_OFFSET,
                    }}
                    label={"KONTYNUUJ PŁATNOŚĆ"}
                />
            </ScrollView>
        </ModalContainer>
    );
};

export default BasketModal;
