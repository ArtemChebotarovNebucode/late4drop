import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width: "100%",
    },
    container: {
        flex: 1,
        backgroundColor: "#000",
        opacity: 0.8,
        justifyContent: "flex-end",
    },
    modal: {
        backgroundColor: "#fff",
        width: "100%",
        paddingHorizontal: 20,
        maxHeight: "93%",
    },
    title: {
        fontSize: 18,
        color: Colors.SHARK_TEXT,
        marginTop: 0,
        marginRight: "auto",
        marginBottom: 0,
        marginLeft: "auto",
        fontFamily: "Montserrat_700Bold",
        paddingVertical: 35,
    },
    header: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    },
    originalPrice: {
        flexDirection: "row",
        height: 58,
        backgroundColor: Colors.LIGHT_GRAY_BACKGROUND,
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 30,
    },
    text: {
        fontSize: 16,
        color: Colors.DARK_GRAY_TEXT,
        paddingRight: 8,
        fontFamily: "Montserrat_400Regular",
    },
    price: {
        fontSize: 18,
        color: Colors.BLACK_TEXT,
        fontFamily: "Montserrat_400Regular",
    },
    input: {
        height: 44,
        width: "100%",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_BORDER,
        marginTop: 10,
        marginBottom: 20,
        paddingHorizontal: 14,
        fontFamily: "Montserrat_400Regular",
    },
    label: {
        fontSize: 12,
        color: Colors.DARK_GRAY_TEXT,
        fontFamily: "Montserrat_400Regular",
    },
    error: {
        fontSize: 10,
        marginTop: 5,
        fontFamily: "Montserrat_400Regular",
        color: Colors.BLUE,
    },
});
