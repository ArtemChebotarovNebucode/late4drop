import React, { useState } from "react";
import {
    Modal,
    ScrollView,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
} from "react-native";
import { styles } from "./Auction.Styles";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import DefaultTextInput from "../../inputs/DefaultTextInput/DefaultTextInput";
import Offsets from "../../../constants/Offsets";
import { DismissKeyboard } from "../../index";
import Colors from "../../../constants/Colors";
import CircledButton from "../../buttons/CircledButton/CircledButton";

const Auction = ({ isVisible, closeModal, price }) => {
    const [suggestedPrice, setSuggestedPrice] = useState(price);
    const [isOnFocus, setIsOnFocus] = useState(false);
    const minimalPrice = price / 2;

    const handlePriceChange = (suggested) => {
        if (!isNaN(suggested) && !isNaN(parseInt(suggested))) {
            setSuggestedPrice(parseInt(suggested));
        } else if (suggestedPrice === "") {
            setSuggestedPrice(price);
        }
    };
    const handleTouchOutside = (onTouch) => {
        const view = <View style={styles.wrapper} />;
        if (!onTouch) return view;

        return (
            <TouchableWithoutFeedback onPress={onTouch} style={styles.wrapper}>
                {view}
            </TouchableWithoutFeedback>
        );
    };
    return (
        <Modal transparent visible={isVisible} animationType="slide">
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#000000AA",
                    justifyContent: "flex-end",
                }}
            >
                {handleTouchOutside(closeModal)}
                <DismissKeyboard>
                    <ScrollView style={styles.modal}>
                        <View style={styles.header}>
                            <CircledButton
                                iconType={"svg"}
                                iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                                iconHeight={15}
                                iconWidth={15}
                                onPress={() => closeModal()}
                                backgroundColor={Colors.GRAY_BACKGROUND}
                                fill={Colors.BLUE}
                            />
                            <Text style={styles.title}>
                                Zaproponuj swoją cenę
                            </Text>
                        </View>
                        <View style={styles.originalPrice}>
                            <Text style={styles.text}>Cena oryginalna:</Text>
                            <Text style={styles.price}>{price} zł</Text>
                        </View>

                        <Text style={styles.label}>Twoja propozycja</Text>
                        <View style={{ marginBottom: 20 }}>
                            <TextInput
                                value={
                                    isOnFocus
                                        ? suggestedPrice
                                        : suggestedPrice
                                        ? `${suggestedPrice} zł`
                                        : ""
                                }
                                onFocus={() => setIsOnFocus(true)}
                                onBlur={() => setIsOnFocus(false)}
                                onChangeText={handlePriceChange}
                                style={[styles.input, { marginBottom: 0 }]}
                                placeholder={"0 zł"}
                            />
                            <Text
                                style={[
                                    styles.error,
                                    {
                                        display:
                                            minimalPrice <= suggestedPrice
                                                ? "none"
                                                : "flex",
                                    },
                                ]}
                            >
                                Proponowana cena nie może być niższa niż 50% ceny oryginalnej
                            </Text>
                        </View>

                        <DefaultTextInput
                            isDefaultTextArea={true}
                            label={"Kraj dostawy"}
                            placeholder={"Kraj"}
                        />
                        <DefaultTextInput
                            isDefaultTextArea={false}
                            label={"Wiadomość do sprzedawcy"}
                            placeholder={"Wiadomość"}
                        />

                        <PrimaryButton
                            onPress={() => closeModal()}
                            isDisable={minimalPrice > suggestedPrice}
                            additionalStyle={{
                                marginTop: 20,
                                marginBottom: Offsets.BOTTOM_OFFSET,
                            }}
                            label={"WYŚLIJ OFERTĘ"}
                        />
                    </ScrollView>
                </DismissKeyboard>
            </View>
        </Modal>
    );
};

export default Auction;
