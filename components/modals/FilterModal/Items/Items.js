import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import CircledButton from "../../../buttons/CircledButton/CircledButton";
import Colors from "../../../../constants/Colors";
import Item from "./Item/Item";

const Items = ({ closeModal, data,   onItemsChange, selectedItems }) => {
    return (
        <View style={styles.container}>
            <ScrollView
                style={{ width: "100%" }}
                showsVerticalScrollIndicator={false}
            >
                <View style={styles.header}>
                    <Text style={styles.title}>Filtry</Text>
                    <View
                        style={{
                            position: "absolute",
                            width: "100%",
                            flexDirection: "row",
                            alignItems: "center",
                            height: "100%",
                        }}
                    >
                        <CircledButton
                            iconType={"svg"}
                            iconSource={require("../../../../assets/icons/arrow-left.svg")}
                            iconHeight={15}
                            iconWidth={15}
                            fill={Colors.BLUE}
                            backgroundColor={Colors.GRAY_BACKGROUND}
                            additionalStyle={{ marginTop: 5 }}
                            onPress={closeModal}
                        />
                    </View>
                </View>
                {data.data
                    .sort((a, b) => (a.name > b.name ? 1 : -1))
                    .map((peace) => (
                        <Item key={peace.id} selectedItems={selectedItems} onItemsChange={onItemsChange} item={peace}  />
                    ))}
            </ScrollView>
        </View>
    );
};
export const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
        paddingBottom: 20,
    },
    header: {
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "center",
    },
    title: {
        fontSize: 18,
        fontFamily: "Montserrat_700Bold",
        paddingTop: 30,
        paddingBottom: 25,
    },
});

export default Items;
