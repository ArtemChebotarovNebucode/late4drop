import React, {useState} from "react";
import { StyleSheet, Text, TouchableOpacity, CheckBox } from "react-native";
import Colors from "../../../../../constants/Colors";
import SvgUri from "expo-svg-uri";

const Item = ({  onItemsChange, selectedItems, item }) => {
    const [isChecked, setIsChecked] = useState(false);
    const handleChangeValue = () => {
        setIsChecked(!isChecked)
        onItemsChange(item)
    }
    return (
        <TouchableOpacity onPress={() => handleChangeValue()} style={styles.container}>
            <Text style={styles.text}>{item.name}</Text>
            <CheckBox value={isChecked} style={[styles.checkbox, {borderWidth:  selectedItems.find((i) => i.name === item.name) ? 7 : 2}]} />
        </TouchableOpacity>
    );
};
export const styles = StyleSheet.create({
    container: {
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 40,
    },
    text: {
        fontSize: 16,
        fontFamily: "Montserrat_700Bold",
        color: Colors.LIGHT_BLACK_TEXT,
    },
    checkbox: {
        // alignSelf: "center",
        width: 20,
        height: 20,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: Colors.BLUE
    }
});

export default Item;