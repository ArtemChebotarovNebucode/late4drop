import React from "react";
import { StyleSheet, View } from "react-native";
import ModalContainer from "../ModalContainer/ModalContainer";
import Items from "./Items/Items";

const FilterModal = ({ closeModal, isVisible, data,   onItemsChange, selectedItems }) => {
    return (
        <ModalContainer closeModal={closeModal} isVisible={isVisible}>
            <View style={styles.modal}>
                <Items selectedItems={selectedItems} onItemsChange={onItemsChange} closeModal={closeModal} data={data} />
            </View>
        </ModalContainer>
    );
};

export const styles = StyleSheet.create({
    modal: {
        backgroundColor: "#fff",
        width: "100%",
        maxHeight: "93%",
        paddingHorizontal: 20,
    },
});

export default FilterModal;