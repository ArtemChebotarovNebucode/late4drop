import {  StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
  modal: {
    backgroundColor: "#fff",
    width: "100%",
    paddingHorizontal: 20,
    maxHeight: "93%",

  },
  title: {
    fontSize: 18,
    color: Colors.SHARK_TEXT,
    fontFamily: "Montserrat_700Bold"
  },
  header: {
    flexDirection: "row",
    alignItems: 'center',
    paddingTop: 26,
    paddingBottom: 36
  },
});
