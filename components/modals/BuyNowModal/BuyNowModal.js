import React, { useState } from "react";
import {KeyboardAvoidingView, Platform, ScrollView, Text, View} from "react-native";
import ModalContainer from "../ModalContainer/ModalContainer";
import BuyNow from "../../partials/BuyNow/BuyNow";
import CustomCheckBox from "../../inputs/CustomCheckBox/CustomCheckBox";
import { styles } from "./BuyNowModal.Styles";
import Offsets from "../../../constants/Offsets";
import CircledButton from "../../buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";
import TouchableText from "../../buttons/TouchableText/TouchableText";
import { useNavigation } from "@react-navigation/core";

const BuyNowModal = ({ closeModal, isVisible, openBasketModal, productId , setBankUrl }) => {
    const navigation = useNavigation();

    const [verificationChecked, setVerificationChecked] = useState(false);
    const [paymentWithOldDataChecked, setPaymentWithOldDataChecked] = useState(
        false
    );

    return (
        <ModalContainer closeModal={closeModal} isVisible={isVisible}>
            <View style={styles.modal}>

                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={styles.header}>
                        <CircledButton
                            iconType={"svg"}
                            iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                            iconHeight={15}
                            iconWidth={15}
                            onPress={() => closeModal()}
                            backgroundColor={Colors.GRAY_BACKGROUND}
                            fill={Colors.BLUE}
                            additionalStyle={{ marginTop: 5 }}
                        />
                        <View
                            style={{
                                position: "absolute",
                                alignItems: "center",
                                width: "100%",
                                justifyContent: "center",
                            }}
                        >
                            <Text style={styles.title}>Kup teraz</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CustomCheckBox
                            additionalStyle={{ paddingBottom: 30 }}
                            label={
                                <Text
                                    style={{
                                        color: Colors.BLUE,
                                        fontFamily: "Montserrat_700Bold",
                                    }}
                                >
                                    Weryfikacja
                                </Text>
                            }
                            isChecked={verificationChecked}
                            setIsChecked={setVerificationChecked}
                        />
                        <TouchableText
                            label={"Co to jest?"}
                            additionalStyle={{ marginLeft: 5, marginTop: 2 }}
                            onPress={() => {
                                closeModal();
                                navigation.navigate("ProtectionBuyers");
                            }}
                        />
                    </View>

                    <CustomCheckBox
                        additionalStyle={{ paddingBottom: 30 }}
                        label={"Użyj moich danych rozliczeniowych"}
                        isChecked={paymentWithOldDataChecked}
                        setIsChecked={setPaymentWithOldDataChecked}
                    />

                    <BuyNow
                        closeModal={closeModal}
                        isModal={true}
                        openBasketModal={openBasketModal}
                        verificationChecked={verificationChecked}
                        productId={productId}
                        setBankUrl={setBankUrl}
                        paymentWithOldDataChecked={paymentWithOldDataChecked}
                    />
                    <View
                        style={
                            Platform.OS === "android"
                                ? { height: Offsets.BOTTOM_OFFSET }
                                : {
                                      height: Offsets.BOTTOM_OFFSET - 30,
                                  }
                        }
                    />

                </ScrollView>
            </View>
        </ModalContainer>

    );
};

export default BuyNowModal;
