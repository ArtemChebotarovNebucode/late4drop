import React, {useState} from "react";
import { Platform, Text, View, ScrollView } from "react-native";
import { styles } from "./MessageModal.Styles";
import ModalContainer from "../ModalContainer/ModalContainer";
import UserBlock from "../OpinionModal/UserBlock/UserBlock";
import Message from "./Message/Message";
import { DismissKeyboard } from "../../index";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import Offsets from "../../../constants/Offsets";
import CircledButton from "../../buttons/CircledButton/CircledButton";
import Colors from "../../../constants/Colors";
import ChatService from "../../../services/chatService";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";
import {set} from "react-native-reanimated";
import {useNavigation} from "@react-navigation/core";

const MessageModal = ({ closeModal, isVisible, userId, imageUrl, name}) => {
    const { userToken } = useAuth();
    const charService = new ChatService();
    const [message, setMessage] = useState("")
    const navigation = useNavigation();
    const handleSendMessage = () => {
        if(message !== ""){
            charService
                .sendMessage(userToken, userId, message)
                .then((response) => {
                    if(response?.data?.data?.message !== undefined){
                        closeModal()
                        navigation.navigate("Chat", {
                            chatId: response?.data?.data?.message?.chatId,
                            recipientId: userId,
                            photoURL: imageUrl,
                            name: name
                        })
                    }

                });
        }

    };

    return (
        <ModalContainer closeModal={closeModal} isVisible={isVisible}>
            <DismissKeyboard>
                <ScrollView style={styles.modal}>
                    <View style={styles.header}>
                        <Text style={styles.title}>Wyślij wiadomość</Text>
                        <View
                            style={{
                                position: "absolute",
                                width: "100%",
                                flexDirection: "row",
                                alignItems: "center",
                                height: "100%",
                            }}
                        >
                            <CircledButton
                                iconType={"svg"}
                                iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                                iconHeight={15}
                                iconWidth={15}
                                fill={Colors.BLUE}
                                backgroundColor={Colors.GRAY_BACKGROUND}
                                additionalStyle={{ marginTop: 10 }}
                                onPress={closeModal}
                            />
                        </View>
                    </View>
                    <UserBlock imageUrl={imageUrl} name={name} />
                    <Message message={message} setMessage={setMessage} />
                    <PrimaryButton
                        onPress={() => handleSendMessage()}
                        additionalStyle={{ marginBottom: 10 }}
                        label={"WYŚLIJ WIADOMOŚĆ"}
                    />

                    <View
                        style={
                            Platform.OS === "android"
                                ? { height: Offsets.BOTTOM_OFFSET }
                                : {
                                      height: Offsets.BOTTOM_OFFSET - 40,
                                  }
                        }
                    />
                </ScrollView>
            </DismissKeyboard>
        </ModalContainer>
    );
};

export default MessageModal;
