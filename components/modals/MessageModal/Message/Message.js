import React from "react";
import { Text, TextInput, View } from "react-native";
import { styles } from "./Message.Styles";

const Message = ({ message, setMessage }) => {
    return (
        <View style={{ paddingTop: 15 }}>
            <Text style={styles.text}>Napisz wiadomość</Text>
            <TextInput
                multiline={true}
                numberOfLines={15}
                value={message}
                onChangeText={(text) => setMessage(text)}
                textAlignVertical={"top"}
                placeholder="Treść wiadomości"
                style={styles.input}
            />
        </View>
    );
};

export default Message;
