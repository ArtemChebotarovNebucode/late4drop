import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    input: {
        height: 275,
        width: "100%",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_LINE,
        marginTop: 10,
        marginBottom: 43,
        paddingHorizontal: 14,
        paddingTop: 14,
        fontFamily: "Montserrat_400Regular",
    },
    text: {
        fontSize: 12,
        fontFamily: "Montserrat_400Regular",
        color: Colors.DARK_GRAY_TEXT,
    },
});
