import React, { useEffect, useState } from "react";
import {
    FlatList,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";

import ModalContainer from "../ModalContainer/ModalContainer";
import DropDown from "../../inputs/DropDown/DropDown";
import CircledButton from "../../buttons/CircledButton/CircledButton";
import Size from "./Size/Size";
import Color from "./Color/Color";

import { sortBy } from "../../../data/sortBy";
import { styles } from "./FiltersModal.styles";
import { colors as dummyColors } from "../../../data/dummyColorsData";
import Offsets from "../../../constants/Offsets";
import Colors from "../../../constants/Colors";
import FilterModal from "../FilterModal/FilterModal";
import BrandsService from "../../../services/brandsService";
import CategoriesService from "../../../services/categoriesService";
import SortService from "../../../services/sortService";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import ColorService from "../../../services/colorService";
import ClothingSizeService from "../../../services/clothingSizeService";
import ShoeSizeService from "../../../services/shoeSizeService";

const FiltersModal = ({
    closeModal,
    isVisible,
    handleFilterProducts,
    setFilters,
}) => {
    const brandsService = new BrandsService();
    const categoriesService = new CategoriesService();
    const sortService = new SortService();
    const colorService = new ColorService();
    const clothingSizeService = new ClothingSizeService();
    const shoeSizeService = new ShoeSizeService();

    const [isBrandsFilterOpen, setIsBrandsFilterOpen] = useState(false);
    const [isSortListOpen, setIsSortListOpen] = useState(false);
    const [isCategoriesFilterOpen, setIsCategoriesFilterOpen] = useState(false);
    const [selectedSortOption, setSelectedSortOption] = useState(sortBy[0]);
    const [brandsFetched, setBrandsFetched] = useState([]);
    const [categoriesFetched, setCategoriesFetched] = useState([]);
    const [sortOptionsFetched, setSortOptionsFetched] = useState([]);
    const [multiSliderValue, setMultiSliderValue] = useState([0, 30000]);
    const [selectedCategories, setSelectedCategories] = useState([]);
    const [selectedCondition, setSelectedCondition] = useState([]);
    const [selectedMarks, setSelectedMarks] = useState([]);
    const [selectedMark, setSelectedMark] = useState(null);
    const [activeColors, setActiveColors] = useState([]);

    const [shoeSizes, setShoeSizes] = useState([]);
    const [clothSizes, setClothSizes] = useState([]);
    const [colors, setColors] = useState(
        dummyColors.map((color, index) => {
            return {
                ...color,
                active: false,
            };
        })
    );
    const handleAddActiveColor = (color, id) => {
        setActiveColors((prev) =>
            !prev.find((item) => item.color === colors)
                ? prev.concat({color: color, id: id})
                : prev.filter((item) => item.color !== color)
        );
    };
    const resetFilters = () => {
        setFilters({
            brands: [],
            shoeSizes: [],
            clothingSizes: [],
            colors: [],
            conditions: [],
            categories: [],
            sort: "",
            minPrice: 0,
            maxPrice: 15000,
            page: 0,
            searchString: "",
        });
        setSelectedCategories([]);
        setSelectedMarks([]);
        setSelectedCondition([]);

        setColors(
            dummyColors.map((color, index) => {
                return {
                    ...color,
                    active: false,
                };
            })
        );
        setShoeSizes((prev) =>
            prev.map((item) => {
                return { ...item, active: false };
            })
        );
        setClothSizes((prev) =>
            prev.map((item) => {
                return { ...item, active: false };
            })
        );
    };

    const handleListFooterComponentStyle = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <TouchableOpacity onPress={() => handleAddActiveColor("moro", 12)}>
                    <Text
                        style={{
                            marginRight: 10,
                            borderWidth: activeColors.find((item) => item.color === "moro") ? 2 : 1,
                            borderColor:  activeColors.find((item) => item.color === "moro") ? Colors.BLUE
                                : Colors.GRAY_LINE,
                            paddingVertical: 14,
                            paddingHorizontal: 12,
                            fontSize: 12,
                            fontFamily: "Montserrat_400Regular",
                        }}
                    >
                        moro
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => handleAddActiveColor("multi", 16)}>
                    <Text
                        style={{
                            marginRight: 10,
                            borderWidth:activeColors.find((item) => item.color === "multi") ? 2 : 1,
                            borderColor: activeColors.find((item) => item.color === "multi")
                                ? Colors.BLUE
                                : Colors.GRAY_LINE,
                            paddingVertical: 14,
                            paddingHorizontal: 12,
                            fontSize: 12,
                            fontFamily: "Montserrat_400Regular",
                        }}
                    >
                        multi
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };

    const handleChangeFilters = (name, value) => {
        setFilters((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handleChangeMarks = (brand) => {
        setSelectedMarks((prev) => {
            return !prev.includes(brand)
                ? prev.concat(brand)
                : prev.filter((item) => item !== brand);
        });
    };
    const handleChangeCategories = (category) => {
        setSelectedCategories((prev) => {
            return !prev.includes(category)
                ? prev.concat(category)
                : prev.filter((item) => item !== category);
        });
    };

    useEffect(() => {
        brandsService
            .fetchBrands()
            .then((brands) => setBrandsFetched(brands.data));

        categoriesService
            .fetchCategories()
            .then((categories) => setCategoriesFetched(categories.data));

        sortService
            .fetchSortOptions()
            .then((sortOptions) =>
                setSortOptionsFetched(
                    sortOptions?.data?.data?.map(
                        (sortOption) => sortOption.text
                    )
                )
            );
        clothingSizeService.getAllClothingSize().then((response) => {
            setClothSizes(
                response?.data?.data?.map((item) => {
                    return { ...item, active: false };
                })
            );
        });
        shoeSizeService.getAllShoeSize().then((response) => {
            setShoeSizes(
                response?.data?.data?.map((item) => {
                    return { ...item, active: false };
                })
            );
        });
        // Todo: when colors will be with correct names
        // colorService.fetchColors().then((colors) => {
        //      setColors(response?.data?.data?.map((item) => {
        //                 return {...item, active: false}
        //             }))
        // });
    }, []);

    const handleSortOptionChange = (sortOption) => {
        setIsSortListOpen(false);
        setSelectedSortOption(sortOption);
        handleChangeFilters("sort", sortOption);
    };

    const handleOnShoeSizeChoose = (e, size) => {
        setShoeSizes(
            [...shoeSizes].map((sizeFetched) => {
                if (sizeFetched.id === size.item.id) {
                    return { ...sizeFetched, active: !sizeFetched.active };
                }
                return sizeFetched;
            })
        );
    };

    const handleOnClothSizeChoose = (e, size) => {
        setClothSizes(
            [...clothSizes].map((sizeFetched) => {
                if (sizeFetched.id === size.item.id) {
                    return { ...sizeFetched, active: !sizeFetched.active };
                }
                return sizeFetched;
            })
        );
    };

    const handleOnColorChoose = (e, color) => {
        setColors(
            [...colors].map((colorFetched) => {
                if (colorFetched.id === color.item.id) {
                    return { ...colorFetched, active: !colorFetched.active };
                }
                return colorFetched;
            })
        );
    };
    const handleMultiSliderValueChange = (values) => {
        setMultiSliderValue(values);
        handleChangeFilters("minPrice", values[0]);
        handleChangeFilters("maxPrice", values[1]);
    };
    const filterProducts = () => {
        handleFilterProducts(true);
        closeModal();
    };

    useEffect(() => {
        handleChangeFilters(
            "colors",
            colors
                .filter((item) => item.active)
                .reduce((prev, next) => {
                    return prev.concat(next.id);
                }, []).concat(activeColors.reduce((prev, next) => {
                return prev.concat(next.id)
            }, []))
        );
    }, [colors]);
    const handleSelectCondition = (condition) => {
        setSelectedCondition((prev) =>
            prev.find((item) => item.id === condition.id)
                ? prev.filter((item) => item.id !== condition.id)
                : prev.concat(condition)
        );
    };
    useEffect(() => {
        handleChangeFilters(
            "clothingSizes",
            clothSizes?.filter((item) => item.active)
                .reduce((prev, next) => {
                    return prev.concat(next.id);
                }, [])
        );
    }, [clothSizes]);
    useEffect(() => {
        handleChangeFilters(
            "shoeSizes",
            shoeSizes?.filter((item) => item.active)
                .reduce((prev, next) => {
                    return prev.concat(next.id);
                }, [])
        );
    }, [shoeSizes]);
    useEffect(() => {
        handleChangeFilters(
            "categories",
            selectedCategories?.reduce((prev, next) => {
                return prev.concat(next.id);
            }, [])
        );
    }, [selectedCategories]);
    useEffect(() => {
        handleChangeFilters(
            "brands",
            selectedMarks.reduce((prev, next) => {
                return prev.concat(next.id);
            }, [])
        );
    }, [selectedMarks]);
    useEffect(() => {
        handleChangeFilters(
            "conditions",
            selectedCondition.reduce((prev, next) => {
                return prev.concat(next.id);
            }, [])
        );
    }, [selectedCondition]);

    return (
        <ModalContainer closeModal={closeModal} isVisible={isVisible}>
            <View style={styles.modal}>
                <ScrollView
                    style={{ width: "100%" }}
                    showsVerticalScrollIndicator={false}
                >
                    <View style={styles.header}>
                        <Text style={styles.title}>Filtry</Text>
                        <View
                            style={{
                                position: "absolute",
                                width: "100%",
                                flexDirection: "row",
                                alignItems: "center",
                                height: "100%",
                            }}
                        >
                            <CircledButton
                                iconType={"svg"}
                                iconSource={require("../../../assets/icons/black-cross-icon.svg")}
                                iconHeight={15}
                                iconWidth={15}
                                fill={Colors.BLUE}
                                backgroundColor={Colors.GRAY_BACKGROUND}
                                additionalStyle={{ marginTop: 5 }}
                                onPress={closeModal}
                            />
                        </View>
                    </View>
                    <DropDown
                        handleValueChange={handleSortOptionChange}
                        items={sortOptionsFetched}
                        selectedValue={selectedSortOption}
                        handleOpenValue={() => setIsSortListOpen(true)}
                        isOpen={isSortListOpen}
                        additionalStyles={{ marginTop: 20 }}
                    />

                    <View
                        style={{
                            width: "100%",
                            alignItems: "center",
                            flexDirection: "column",
                        }}
                    >
                        <Text style={styles.subtitle}>Cena</Text>

                        {/*<CustomRangeSlider*/}
                        {/*    multiSliderValue={multiSliderValue}*/}
                        {/*    setMultiSliderValue={handleMultiSliderValueChange}*/}
                        {/*/>*/}

                        <View
                            style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                                width: "100%",
                                paddingTop: 15,
                            }}
                        >
                            <TextInput
                                onChangeText={(text) =>
                                    setFilters((prev) => {
                                        return { ...prev, minPrice: text };
                                    })
                                }
                                keyboardType={"numeric"}
                                style={styles.labelText}
                                placeholder={"PLN min"}
                            />

                            <TextInput
                                onChangeText={(text) =>
                                    setFilters((prev) => {
                                        return { ...prev, maxPrice: text };
                                    })
                                }
                                keyboardType={"numeric"}
                                style={styles.labelText}
                                placeholder={"PLN max"}
                            />
                        </View>

                        <Text style={{ ...styles.subtitle, marginTop: 20 }}>
                            Kategoria
                        </Text>

                        <TouchableOpacity
                            style={styles.chooseCategoryButton}
                            onPress={() => setIsCategoriesFilterOpen(true)}
                        >
                            <Text style={styles.chooseCategoryText}>
                                Wybierz kategorię
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            flexDirection: "row",
                            flexWrap: "wrap",
                            marginTop: 3,
                        }}
                    >
                        {selectedCategories.map((item) => (
                            <Text key={item.id} style={styles.selectedCategory}>
                                #{item.name}
                            </Text>
                        ))}
                    </View>

                    <View
                        style={{
                            width: "100%",
                            alignItems: "center",
                            flexDirection: "column",
                        }}
                    >
                        <Text style={{ ...styles.subtitle, marginTop: 20 }}>
                            Marka
                        </Text>

                        <TouchableOpacity
                            style={styles.chooseCategoryButton}
                            onPress={() => setIsBrandsFilterOpen(true)}
                        >
                            <Text style={styles.chooseCategoryText}>
                                Wybierz markę
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            flexDirection: "row",
                            flexWrap: "wrap",
                            marginTop: 3,
                        }}
                    >
                        {selectedMarks.map((item) => (
                            <Text key={item.id} style={styles.selectedCategory}>
                                #{item.name}
                            </Text>
                        ))}
                    </View>
                    <View style={styles.conditionWrapper}>
                        <TouchableOpacity
                            onPress={() =>
                                handleSelectCondition({ name: "Nowe", id: 1 })
                            }
                        >
                            <Text
                                style={[
                                    styles.condition,
                                    {
                                        borderWidth: selectedCondition.find(
                                            (item) => item.name === "Nowe"
                                        )
                                            ? 2
                                            : 1,
                                        borderColor: selectedCondition.find(
                                            (item) => item.name === "Nowe"
                                        )
                                            ? Colors.BLUE_TEXT
                                            : Colors.GRAY_LINE,
                                    },
                                ]}
                            >
                                Nowe
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() =>
                                handleSelectCondition({
                                    name: "Używane",
                                    id: 2,
                                })
                            }
                        >
                            <Text
                                style={[
                                    styles.condition,
                                    {
                                        borderWidth: selectedCondition.find(
                                            (item) => item.name === "Używane"
                                        )
                                            ? 2
                                            : 1,
                                        borderColor: selectedCondition.find(
                                            (item) => item.name === "Używane"
                                        )
                                            ? Colors.BLUE_TEXT
                                            : Colors.GRAY_LINE,
                                    },
                                ]}
                            >
                                Używane
                            </Text>
                        </TouchableOpacity>
                    </View>

                    {!selectedCategories.filter(
                        (category) => (category.name = "Akcesoria")
                    ).length > 0 ? (
                        <View>
                            <View
                                style={{
                                    width: "100%",
                                    alignItems: "center",
                                    flexDirection: "column",
                                }}
                            >
                                <Text style={{ ...styles.subtitle }}>
                                    Rozmiar buta
                                </Text>

                                <FlatList
                                    horizontal
                                    data={shoeSizes}
                                    showsHorizontalScrollIndicator={false}
                                    legacyImplementation={false}
                                    renderItem={(size) => {
                                        return (
                                            <Size
                                                name={size.item.name}
                                                onPress={(e) =>
                                                    handleOnShoeSizeChoose(
                                                        e,
                                                        size
                                                    )
                                                }
                                                isActive={size.item.active}
                                            />
                                        );
                                    }}
                                    style={{ marginTop: 20 }}
                                    keyExtractor={(size) => `${size.name}`}
                                />
                            </View>

                            <View
                                style={{
                                    width: "100%",
                                    alignItems: "center",
                                    flexDirection: "column",
                                }}
                            >
                                <Text
                                    style={{
                                        ...styles.subtitle,
                                        marginTop: 20,
                                    }}
                                >
                                    Rozmiar ubrania
                                </Text>

                                <FlatList
                                    horizontal
                                    data={clothSizes}
                                    showsHorizontalScrollIndicator={false}
                                    legacyImplementation={false}
                                    renderItem={(size) => {
                                        return (
                                            <Size
                                                name={size.item.name}
                                                onPress={(e) =>
                                                    handleOnClothSizeChoose(
                                                        e,
                                                        size
                                                    )
                                                }
                                                isActive={size.item.active}
                                            />
                                        );
                                    }}
                                    style={{ marginTop: 20 }}
                                    keyExtractor={(size) => `${size.name}`}
                                />
                            </View>
                        </View>
                    ) : null}

                    <View
                        style={{
                            width: "100%",
                            alignItems: "center",
                            flexDirection: "column",
                        }}
                    >
                        <Text style={{ ...styles.subtitle, marginTop: 20 }}>
                            Kolor
                        </Text>

                        <FlatList
                            horizontal
                            data={colors}
                            showsHorizontalScrollIndicator={false}
                            legacyImplementation={false}
                            ListFooterComponent={() =>
                                handleListFooterComponentStyle()
                            }
                            renderItem={(color) => {
                                return (
                                    <Color
                                        color={color.item.color}
                                        onPress={(e) =>
                                            handleOnColorChoose(e, color)
                                        }
                                        isActive={color.item.active}
                                        borderNeeded={
                                            color.item.color === "#FFF"
                                        }
                                    />
                                );
                            }}
                            style={{ marginTop: 20 }}
                            keyExtractor={(color) => `${color.name}`}
                        />
                    </View>

                    <PrimaryButton
                        label={"SZUKAJ"}
                        additionalStyle={{ marginTop: 30 }}
                        onPress={() => filterProducts()}
                    />
                    <PrimaryButton
                        label={"ZRESETUJ FILTRY"}
                        additionalStyle={{ marginTop: 15 }}
                        onPress={() => resetFilters()}
                    />

                    <View
                        style={
                            Platform.OS === "ios"
                                ? { height: Offsets.BOTTOM_OFFSET - 30 }
                                : { height: Offsets.BOTTOM_OFFSET }
                        }
                    />
                </ScrollView>
            </View>
            <FilterModal
                isVisible={isCategoriesFilterOpen}
                closeModal={() => setIsCategoriesFilterOpen(false)}
                data={categoriesFetched}
                onItemsChange={handleChangeCategories}
                selectedItems={selectedCategories}
            />
            <FilterModal
                isVisible={isBrandsFilterOpen}
                closeModal={() => setIsBrandsFilterOpen(false)}
                data={brandsFetched}
                onItemsChange={handleChangeMarks}
                selectedItems={selectedMarks}
            />
        </ModalContainer>
    );
};

export default FiltersModal;
