import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    color: {
        width: 48,
        height: 48,
        borderRadius: 50,
        marginHorizontal: 5,
    },
    colorActive: {
        borderWidth: 2,
        borderColor: Colors.BLUE,
    },
});
