import React from "react";
import { TouchableOpacity } from "react-native";

import { styles } from "./Color.Styles";
import Colors from "../../../../constants/Colors";

const Color = ({ color, onPress, isActive, borderNeeded, additionStyles }) => {
    return (
        <TouchableOpacity
            style={[{
                ...(isActive
                    ? {
                          ...styles.colorActive,
                          ...styles.color,
                          backgroundColor: color,
                      }
                    : { ...styles.color, backgroundColor: color }),
                ...(borderNeeded && !isActive
                    ? { borderColor: Colors.GRAY_TEXT, borderWidth: 2 }
                    : {}),
            }, additionStyles]}
            onPress={onPress}
        />
    );
};

export default Color;
