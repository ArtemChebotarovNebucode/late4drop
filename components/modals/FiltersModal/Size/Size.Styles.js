import { StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";

export const styles = StyleSheet.create({
    size: {
        width: 48,
        height: 48,
        borderRadius: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 5,
    },
    sizeActive: {
        backgroundColor: Colors.BLUE,
    },
    activeSizeName: {
        color: "#FFF",
        fontFamily: "Montserrat_500Medium",
        fontSize: 12
    },
    sizeInactive: {
        borderWidth: 1,
        borderColor: Colors.GRAY_TEXT,
    },
    inactiveSizeName: {
        color: Colors.GRAY_TEXT,
        fontFamily: "Montserrat_500Medium",
        fontSize: 12
    },
});
