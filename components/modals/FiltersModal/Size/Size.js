import React from "react";
import { Text, TouchableOpacity } from "react-native";

import { styles } from "./Size.Styles";

const Size = ({ name, onPress, isActive }) => {
    return (
        <TouchableOpacity
            style={
                isActive
                    ? { ...styles.sizeActive, ...styles.size }
                    : { ...styles.sizeInactive, ...styles.size }
            }
            onPress={onPress}
        >
            <Text
                style={
                    isActive ? styles.activeSizeName : styles.inactiveSizeName
                }
            >
                {name}
            </Text>
        </TouchableOpacity>
    );
};

export default Size;
