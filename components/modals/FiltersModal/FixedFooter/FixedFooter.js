import React from "react";
import { View, Text, TouchableOpacity } from "react-native";

import { styles } from "./FixedFooter.Styles";
import SvgUri from "expo-svg-uri";
import Colors from "../../../../constants/Colors";

const FixedFooter = ({ handleOnFiltersDismiss }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.dismissSection}
                onPress={handleOnFiltersDismiss}
            >
                <Text style={styles.text}>WYCZYŚĆ</Text>
                <View style={styles.dismissButton}>
                    <SvgUri
                        width={10}
                        height={10}
                        source={require("../../../../assets/icons/black-cross-icon.svg")}
                        fill={"#000"}
                    />
                </View>
            </TouchableOpacity>

            <View style={styles.chosenFiltersCountSection}>
                <TouchableOpacity>
                    <Text style={{ ...styles.text, color: Colors.BLUE }}>
                      SZUKAJ
                    </Text>
                </TouchableOpacity>
                <View style={styles.chosenFiltersCount}>
                    <Text style={{ ...styles.text, color: "#000" }}>4</Text>
                </View>
            </View>
        </View>
    );
};

export default FixedFooter;
