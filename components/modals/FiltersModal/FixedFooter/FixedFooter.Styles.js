import { Dimensions, Platform, StyleSheet } from "react-native";
import Colors from "../../../../constants/Colors";
import Offsets from "../../../../constants/Offsets";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        width: 288,
        height: 60,
        bottom:
            Platform.OS === "android"
                ? Offsets.BOTTOM_OFFSET
                : Offsets.BOTTOM_OFFSET - 30,
        position: "absolute",
        borderRadius: 50,
        left: Dimensions.get("window").width / 2 - 144,
        flexDirection: "row",
        alignItems: "center",
    },
    text: {
        color: "#FFF",
        fontFamily: "Montserrat_700Bold",
        fontSize: 14,
        textAlign: "center"
    },
    dismissButton: {
        backgroundColor: "#979797",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        width: 14,
        height: 14,
        borderRadius: 50,
        marginLeft: 3,
    },
    dismissSection: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },
    chosenFiltersCountSection: {
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        flex: 1.2,
        marginRight: 10
    },
    chosenFiltersCount: {
        backgroundColor: "#FFF",
        width: 38,
        height: 38,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 50,
        marginLeft: 25
    },
});
