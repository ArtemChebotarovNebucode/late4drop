import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    header: {
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "center",
    },
    modal: {
        backgroundColor: "#fff",
        width: "100%",
        paddingHorizontal: 30,
        maxHeight: "93%",
    },
    title: {
        fontSize: 18,
        fontFamily: "Montserrat_700Bold",
        paddingTop: 30,
        paddingBottom: 25,
    },
    selectedCategory: {
        fontSize: 12,
        fontFamily: "Montserrat_400Regular",
        marginRight: 10,
        color: Colors.DARK_GRAY_TEXT,
    },
    subtitle: {
        fontSize: 16,
        fontFamily: "Montserrat_700Bold",
    },
    sexSection: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10,
        width: "100%",
    },
    sexOptionActive: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: Colors.BLUE,
        paddingVertical: 15,
        width: "30%",
    },
    sexOptionDisabled: {
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 15,
        width: "30%",
        borderWidth: 1,
        borderColor: Colors.GRAY_BORDER,
    },
    sexTextActive: {
        color: "#FFF",
        fontFamily: "Montserrat_400Regular",
        fontSize: 12,
    },
    sexTextDisabled: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 12,
        color: "#000",
    },
    chooseCategoryButton: {
        width: "100%",
        borderWidth: 1,
        borderColor: Colors.BLUE_TEXT,
        borderRadius: 3,
        padding: 5,
        marginTop: 20,
        flexDirection: "row",
        justifyContent: "center",
    },
    chooseCategoryText: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 14,
        color: Colors.BLUE_TEXT,
    },
    brandContainerInactive: {
        borderWidth: 1,
        borderColor: Colors.GRAY_TEXT,
        borderRadius: 3,
        padding: 5,
        marginHorizontal: 5,
    },
    brandTextInactive: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 14,
        color: Colors.GRAY_TEXT,
    },
    brandContainerActive: {
        borderWidth: 1,
        borderColor: Colors.BLUE,
        borderRadius: 3,
        padding: 5,
        marginHorizontal: 5,
    },
    conditionWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginVertical: 15,
    },
    condition: {
        borderWidth: 1,
        borderColor: Colors.GRAY_LINE,
        paddingVertical: 7,
        width: 80,
        textAlign: 'center',
        fontSize: 14,
        fontFamily: "Montserrat_500Medium",

    },
    labelText: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 14,
        borderWidth: 1,
        paddingVertical: 4,
        paddingHorizontal: 7,
        width: 90,
        borderColor: Colors.GRAY_LINE,
    },
});
