import React from 'react';
import { TouchableOpacity, Text } from "react-native";

import { styles } from "./PrimaryButton.Styles";

const PrimaryButton = ({ additionalStyle, onPress, label, isDisable }) => {
    return (
        <TouchableOpacity disabled={isDisable} onPress={onPress} style={{...styles.button, ...additionalStyle}}>
            <Text style={styles.buttonText}>{label}</Text>
        </TouchableOpacity>
    );
}

export default PrimaryButton;