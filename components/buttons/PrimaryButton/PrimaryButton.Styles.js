import { StyleSheet } from 'react-native';
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    button: {
        height: 50,
        backgroundColor: Colors.BLUE,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        fontFamily: 'Montserrat_500Medium'
    },
});