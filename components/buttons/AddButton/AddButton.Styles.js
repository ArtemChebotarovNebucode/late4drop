import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
  wrapper: {
    alignItems: "center",
    justifyContent: "center",
    width: 112,
    height: 120,
  },
  icon: {
    backgroundColor: Colors.BLUE,
    width: 25,
    height: 25,
    alignItems: "center",
    justifyContent: "center",
  },
});
