import React from "react";
import { View, TouchableOpacity } from "react-native";
import SvgUri from "expo-svg-uri";
import { styles } from "./AddButton.Styles";
import Colors from "../../../constants/Colors";

const AddButton = ({ takeImageHandler }) => {
  return (
    <TouchableOpacity onPress={() => takeImageHandler()}>
      <View style={styles.wrapper}>
        <View style={styles.icon}>
          <SvgUri
            width={20}
            height={20}
            source={require("../../../assets/icons/plus-icon.svg")}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default AddButton;
