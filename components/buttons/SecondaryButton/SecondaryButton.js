import React from 'react';
import { TouchableOpacity, Text } from "react-native";

import { styles } from "./SecondaryButton.Styles";

const SecondaryButton = ({ additionalStyle, onPress, label }) => {
    return (
        <TouchableOpacity onPress={onPress} style={{...styles.button, ...additionalStyle}}>
            <Text style={styles.buttonText}>{label}</Text>
        </TouchableOpacity>
    );
}

export default SecondaryButton;