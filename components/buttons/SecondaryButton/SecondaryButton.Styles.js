import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    button: {
        height: 50,
        borderRadius: 5,
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: Colors.BLUE,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText: {
        color: Colors.BLUE,
        fontFamily: "Montserrat_500Medium",
    },
});
