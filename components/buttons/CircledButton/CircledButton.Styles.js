import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    button: {
        width: 38,
        height: 38,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
    },
});