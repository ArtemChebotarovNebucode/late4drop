import React from "react";
import { View, TouchableOpacity, Image } from "react-native";
import SvgUri from "expo-svg-uri";

import { styles } from "./CircledButton.Styles";

const CircledButton = ({
  iconType,
  iconWidth,
  iconHeight,
  iconSource,
  fill,
  isCloseIcon,
  additionalStyle,
  onPress,
  backgroundColor,
}) => {
  return (
    <TouchableOpacity
      style={isCloseIcon ? { position: "absolute", top: -8, right: 2} : {zIndex: 10}}
      onPress={onPress}
    >
      <View style={{ ...styles.button, backgroundColor, ...additionalStyle }}>
        {iconType === "svg" ? (
          <SvgUri
            width={iconWidth}
            height={iconHeight}
            source={iconSource}
            fill={fill}
          />
        ) : (
          <Image
            style={{ width: iconWidth, height: iconHeight }}
            source={iconSource}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

export default CircledButton;
