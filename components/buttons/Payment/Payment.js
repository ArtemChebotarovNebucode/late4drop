import React from "react";
import { Text, TouchableOpacity } from "react-native";

import { styles } from "./Payment.Style";
import SvgUri from "expo-svg-uri";
import Colors from "../../../constants/Colors";

const Payment = ({ icon, name, iconWidth, iconHeight, isActive, onPress }) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={
                isActive
                    ? { ...styles.wrapper, borderColor: Colors.BLUE }
                    : { ...styles.wrapper, borderColor: Colors.GRAY_BORDER }
            }
        >
            <SvgUri width={iconWidth} height={iconHeight} source={icon} />
            {/*<Text style={styles.paymentName}>{name}</Text>*/}
        </TouchableOpacity>
    );
};

export default Payment;
