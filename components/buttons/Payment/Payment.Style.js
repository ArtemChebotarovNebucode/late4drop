import { StyleSheet } from 'react-native';
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        width: '100%',
        height: 80,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderWidth: 1,
        marginRight: 15
    },
    paymentName: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 14
    }
});