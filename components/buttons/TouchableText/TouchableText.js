import React from 'react';
import { TouchableOpacity, Text } from "react-native";

import { styles } from './TouchableText.Styles';


const TouchableText = ({ additionalStyle, onPress, label, fontSize }) => {
    return (
        <TouchableOpacity onPress={onPress} style={additionalStyle}>
            <Text style={{...styles.text, fontSize: fontSize}}>
                {label}
            </Text>
        </TouchableOpacity>
    );
}

export default TouchableText;