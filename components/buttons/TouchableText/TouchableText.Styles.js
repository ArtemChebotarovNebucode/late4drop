import { StyleSheet } from 'react-native';
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    text: {
        color: Colors.BLUE_TEXT,
        fontFamily: 'Montserrat_400Regular',
        fontSize: 14
    },
});