import { StyleSheet } from 'react-native';
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    label: {
        fontFamily: 'Montserrat_400Regular',
        color: Colors.GRAY_TEXT,
        fontSize: 14
    },
});