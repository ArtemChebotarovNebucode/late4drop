import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import SvgUri from "expo-svg-uri";

import { styles } from "./CustomCheckBox.Styles";

const CustomCheckBox = ({
    label,
    additionalStyle,
    fontSize,
    isChecked,
    setIsChecked,
}) => {
    return (
        <TouchableOpacity
            onPress={() => setIsChecked(!isChecked)}
            style={additionalStyle}
        >
            <View style={styles.container}>
                <SvgUri
                    style={{ marginRight: 10 }}
                    width={21}
                    height={21}
                    source={
                        !isChecked
                            ? require("../../../assets/icons/checkbox-outer.svg")
                            : require("../../../assets/icons/checkbox-inner.svg")
                    }
                />
                <Text style={{ ...styles.label, fontSize: fontSize }}>
                    {label}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

export default CustomCheckBox;