import {  StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    input: {
        height: 44,
        width: "100%",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_BORDER,
        marginTop: 10,
        marginBottom: 20,
        paddingHorizontal: 14,
        fontFamily: 'Montserrat_400Regular',
        fontSize: 14,
    },
    label: {
        fontSize: 12,
        color: Colors.DARK_GRAY_TEXT,
        fontFamily: "Montserrat_400Regular",
    },
});
