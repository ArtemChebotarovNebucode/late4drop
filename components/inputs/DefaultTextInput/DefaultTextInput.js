import React from "react";
import { Text, TextInput, View } from "react-native";
import { styles } from "./DefaultTextInput.Styles";

const DefaultTextInput = ({ label, isDefaultTextArea, additionalStyle ,value, handleChange, isNumeric, keyboardType, placeholder, onFocus  }) => {
  return (
    <View style={additionalStyle}>
      <Text style={styles.label}>{label}</Text>
      {isDefaultTextArea ? (
        <TextInput placeholder={placeholder} style={styles.input} value={value} onChangeText={handleChange}  keyboardType={keyboardType !== undefined ? keyboardType : "default" } />
      ) : (
        <TextInput
          multiline={true}
          numberOfLines={4}
          textAlignVertical={"top"}
          placeholder="Treść wiadomości"
          style={[styles.input, { height: 100, paddingTop: 14 }]}
          value={value}

        />
      )}
    </View>
  );
};

export default DefaultTextInput;
