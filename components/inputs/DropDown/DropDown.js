import React from "react";
import { Picker, Platform, Text, TouchableOpacity, View } from "react-native";
import { styles } from "./DropDown.Styles";
import SvgUri from "expo-svg-uri";

const DropDown = ({
    selectedValue,
    handleValueChange,
    items,
    handleOpenValue,
    isOpen,
}) => {
    return Platform.OS === "android" ? (
        <View
            style={[
                styles.input,
                { justifyContent: "center", paddingRight: 0 },
            ]}
        >
            <Picker
                selectedValue={selectedValue}
                onValueChange={(item) => handleValueChange(item)}
            >
                {items?.map((item) => (
                    <Picker.Item
                        key={item}
                        label={item}
                        value={item}
                        style={styles.text}
                    />
                ))}
            </Picker>
        </View>
    ) : (
        <View style={{ position: "relative", marginVertical: isOpen ? 60 : 0 }}>
            <TouchableOpacity
                style={[styles.input, { justifyContent: "center" }]}
                onPress={() => handleOpenValue()}
            >
                <View
                    style={{
                        justifyContent: "space-between",
                        flexDirection: "row",
                        alignItems: "center",
                    }}
                >
                    <Text style={styles.text}>{selectedValue}</Text>
                    <SvgUri
                        width={7}
                        height={5}
                        source={require("../../../assets/icons/arrow-down-icon.svg")}
                    />
                </View>
            </TouchableOpacity>
            <Picker
                selectedValue={selectedValue}
                onValueChange={(value) => handleValueChange(value)}
                style={[styles.picker, { display: isOpen ? "block" : "none" }]}
            >
                {items?.map((item) => (
                    <Picker.Item
                        key={item}
                        label={item}
                        value={item}
                        style={styles.text}
                    />
                ))}
            </Picker>
        </View>
    );
};
export default DropDown;
