import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    input: {
        height: 44,
        width: "100%",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_LINE,
        marginTop: 10,
        marginBottom: 20,
        paddingHorizontal: 5,
    },
    picker: {
        position: "absolute",
        width: "100%",
        height: "100%",
        backgroundColor: "#fff",
        justifyContent: "center",
        zIndex: 200,
    },
    text: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 14,
        marginRight: 10,
    },
});
