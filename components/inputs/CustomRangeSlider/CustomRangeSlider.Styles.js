import { StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    labelWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 20,
        width: "100%",
        borderColor: "red",
        borderWidth: 1
    },
    labelText: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 14,
        borderWidth: 1,
        paddingVertical: 4,
        paddingHorizontal: 7,
        width: 90,
        borderColor: Colors.GRAY_LINE,
    },
});
