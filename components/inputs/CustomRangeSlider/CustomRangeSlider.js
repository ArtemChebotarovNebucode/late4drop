import React, { useState } from "react";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { Platform, View, Text, Dimensions, TextInput } from "react-native";
import { styles } from "./CustomRangeSlider.Styles";
import Colors from "../../../constants/Colors";




// COMPONENT IS NOT USED




const CustomRangeSlider = ({ multiSliderValue, setMultiSliderValue }) => {
    const multiSliderValuesChange = (values) => setMultiSliderValue(values);
    const [isMinOnFocus, setIsMinOnFocus] = useState(false);
    const [isMaxOnFocus, setIsMaxOnFocus] = useState(false);
    const handleChangeMinValue = (value) => {
        if(parseInt(value) < parseInt(multiSliderValue[1]) && parseInt(value) < 30000)
            multiSliderValuesChange([value, multiSliderValue[1]])
        else  multiSliderValuesChange([parseInt(multiSliderValue[1]) - 10, multiSliderValue[1]])
    };
    const handleChangeMaxValue = (value) => {
        if(parseInt(value) > parseInt(multiSliderValue[0]) && parseInt(value) <= 30000)
            multiSliderValuesChange([multiSliderValue[0], value])
        else multiSliderValuesChange([multiSliderValue[0], parseInt(multiSliderValue[0]) + 10])
    };

    return (
        <View style={styles.viewContainer}>
            <View style={styles.sliderWrapper}>
                <View style={styles.labelWrapper}>
                    <TextInput
                        onChangeText={(text) => handleChangeMinValue(text)}
                        keyboardType={"numeric"}
                        onFocus={() => setIsMinOnFocus(true)}
                        onBlur={() => setIsMinOnFocus(false)}
                        style={styles.labelText}
                        value={
                            isMinOnFocus
                                ? multiSliderValue[0]
                                : `${multiSliderValue[0]} zł`
                        }
                    />
                    <TextInput
                        onChangeText={(text) => handleChangeMaxValue(text)}
                        keyboardType={"numeric"}
                        onFocus={() => setIsMaxOnFocus(true)}
                        onBlur={() => setIsMaxOnFocus(false)}
                        style={styles.labelText}
                        value={
                            isMaxOnFocus
                                ? multiSliderValue[1]
                                : multiSliderValue[1] === 30000 ?`${multiSliderValue[1]}+ zł` :  `${multiSliderValue[1]} zł`
                        }
                    />
                </View>
                <MultiSlider
                    markerStyle={{
                        height: 30,
                        width: 30,
                        shadowColor: "#000000",
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowRadius: 1,
                        shadowOpacity: 0.1,
                        borderColor: Colors.BLUE,
                        borderWidth: 2,
                        borderRadius: 50,
                        backgroundColor: "#FFF",
                    }}
                    pressedMarkerStyle={{
                        ...Platform.select({
                            android: {
                                height: 30,
                                width: 30,
                                borderRadius: 20,
                            },
                        }),
                    }}
                    selectedStyle={{
                        backgroundColor: Colors.BLUE,
                    }}
                    trackStyle={{
                        backgroundColor: "#CECECE",
                    }}
                    touchDimensions={{
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        slipDisplacement: 40,
                    }}
                    values={[multiSliderValue[0], multiSliderValue[1]]}
                    onValuesChange={multiSliderValuesChange}
                    min={0}
                    max={30000}
                    step={50}
                    allowOverlap={false}
                    minMarkerOverlapDistance={10}
                    sliderLength={Dimensions.get("window").width - 90}
                />
            </View>
        </View>
    );
};

export default CustomRangeSlider;
