import { StyleSheet } from 'react-native';
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    textInput: {
        fontFamily: 'Montserrat_600SemiBold',
        fontSize: 16,
        flex: 10
    },
    textInputWrap: {
        borderBottomWidth: 1,
        borderColor: Colors.GRAY_TEXT,
        height: 25,
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 5,
        justifyContent: 'space-between',
    },
    label: {
        fontFamily: 'Montserrat_400Regular',
        color: Colors.GRAY_TEXT,
        marginBottom: 8
    },
});