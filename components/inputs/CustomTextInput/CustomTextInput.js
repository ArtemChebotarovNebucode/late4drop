import React, { useState } from "react";
import { Text, TextInput, TouchableOpacity, View } from "react-native";
import SvgUri from "expo-svg-uri";

import { styles } from "./CustomTextInput.Styles";

const CustomTextInput = ({
    additionalStyle,
    label,
    inlineImageLeft,
    textContentType,
    placeholder,
    onChangeText,
}) => {
    const [secure, setSecure] = useState(textContentType === "password");

    const handleOnEyePress = () => {
        setSecure(!secure);
    };

    return (
        <View style={additionalStyle}>
            <Text style={styles.label}>{label}</Text>
            <View style={styles.textInputWrap}>
                <View style={{ flexDirection: "row", flex: 19 }}>
                    <SvgUri
                        style={{ marginRight: 10, flex: 1 }}
                        width={21}
                        height={21}
                        source={inlineImageLeft}
                    />
                    <TextInput
                        style={styles.textInput}
                        secureTextEntry={secure}
                        placeholder={placeholder}
                        onChangeText={onChangeText}
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                </View>

                {textContentType === "password" ? (
                    <TouchableOpacity
                        onPress={handleOnEyePress}
                        style={{ flex: 1.3 }}
                    >
                        <SvgUri
                            width={21}
                            height={14.7}
                            source={
                                secure
                                    ? require("../../../assets/icons/open-eye-icon.svg")
                                    : require("../../../assets/icons/closed-eye-icon.svg")
                            }
                        />
                    </TouchableOpacity>
                ) : null}
            </View>
        </View>
    );
};

export default CustomTextInput;
