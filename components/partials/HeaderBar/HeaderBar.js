import React from "react";
import { View, SafeAreaView, StatusBar } from "react-native";
import { styles } from "./HeaderBar.Style";
const HeaderBar = ({ children, backgroundColor, barStyle }) => {
  return (
    <View>
      <SafeAreaView style={[styles.wrapper, { backgroundColor }]}>
        <StatusBar
          translucent
          backgroundColor={backgroundColor}
          barStyle={barStyle}
        />
      </SafeAreaView>
      <View style={[styles.container, { backgroundColor }]}>
          {children}
      </View>
    </View>
  );
};

export default HeaderBar;
