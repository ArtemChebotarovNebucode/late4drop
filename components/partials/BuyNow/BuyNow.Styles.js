import { Dimensions, StyleSheet } from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width: "100%",
    },
    container: {
        flex: 1,
        backgroundColor: "#000",
        opacity: 0.8,
        justifyContent: "flex-end",
    },
    modal: {
        backgroundColor: "#fff",
        width: "100%",
        borderTopRightRadius: 12,
        borderTopLeftRadius: 12,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 22,
        fontFamily: "Montserrat_700Bold",
        paddingTop: 44,
        paddingBottom: 25,
    },
    header: {
        alignItems: "center",
        justifyContent: "center",
    },
    originalPrice: {
        flexDirection: "row",
        height: 58,
        backgroundColor: Colors.LIGHT_GRAY_BACKGROUND,
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 30,
    },
    text: {
        fontSize: 16,
        color: Colors.DARK_GRAY_TEXT,
        paddingRight: 8,
        fontFamily: "Montserrat_400Regular",
    },
    price: {
        fontSize: 18,
        color: Colors.BLACK_TEXT,
        fontFamily: "Montserrat_400Regular",
    },
    // input: {
    //     height: 44,
    //     width: "100%",
    //     borderWidth: 1,
    //     borderRadius: 2,
    //     borderColor: Colors.GRAY_BORDER,
    //     marginTop: 10,
    //     marginBottom: 20,
    //     paddingHorizontal: 14,
    // },
    label: {
        fontSize: 12,
        color: Colors.DARK_GRAY_TEXT,
        fontFamily: "Montserrat_400Regular",
    },
    name: {
        color: Colors.BLUE,
        fontFamily: "Montserrat_700Bold"
    },
    input: {
        height: 44,
        width: "100%",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: Colors.GRAY_BORDER,
        marginTop: 10,
        marginBottom: 20,
        paddingHorizontal: 14,
        fontFamily: 'Montserrat_400Regular',
        fontSize: 14,
    },
    checkbox: {
        // alignSelf: "center",
        width: 20,
        height: 20,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: Colors.BLUE
    },
    containerPaczkomats: {
        width: '100%',
        position: 'absolute',
        height: 'auto',
        backgroundColor: Colors.WHITE,
        top: 54,
        zIndex: 9999
    },
    autosuggest :{
        width: '100%',
        height: 44,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: Colors.GRAY_LINE,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    suggest: {
        // width: '100%',
        fontSize: 14,
        fontFamily: "Montserrat_500Medium"
    },
    brand: {
        position: 'relative',
        width: '100%',
        height: 'auto',
        zIndex: 1000
    },
    error: {
        fontSize: 11,
        fontFamily: "Montserrat_400Regular",
        color: "#F20656",
    },
});
