import React, { useEffect, useState } from "react";
import {
    CheckBox, KeyboardAvoidingView, Platform,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import PrimaryButton from "../../buttons/PrimaryButton/PrimaryButton";
import DefaultTextInput from "../../inputs/DefaultTextInput/DefaultTextInput";
import { styles } from "./BuyNow.Styles";
import Payment from "../../buttons/Payment/Payment";
import { deliveries } from "./deliveries";
import UserSettingsService from "../../../services/userSettingsService";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";
import PaymentService from "../../../services/paymentService";
import DeliveryService from "../../../services/deliveryService";
import {SafeAreaView} from "react-native-safe-area-context";

const BuyNow = ({
    closeModal,
    isModal,
    openBasketModal,
    navigation,
    verificationChecked,
    productId,
    setBankUrl,
    paymentWithOldDataChecked
}) => {
    const userSettingsService = new UserSettingsService();
    const paymentService = new PaymentService();
    const deliveryService = new DeliveryService();

    const { userToken, currentUser, refreshMyInfo } = useAuth();
    //inpost_locker_standard
    const [selectedDelivery, setSelectedDelivery] = useState(deliveries[0]);
    const [selectedPaymentMethod, setSelectedPaymentMethod] = useState({});
    const [isPaczkomat, setIsPaczkomat] = useState(false);
    const [isKurier, setIsKurier] = useState(true);
    const [searchedText, setSearchedText] = useState("");
    const [selectedPostCod, setSelectedPostCod] = useState("");
    const [errors, setErrors] = useState("")
    const [paczkomats, setPaczkomats] = useState([]);
    const [selectedAddress, setSelectedAddress] = useState("");
    const [delivery, setDelivery] = useState("inpost_courier_standard");
    const [payments, setPayments] = useState({
        apartmentNumber: !isModal ? (currentUser?.userSettings?.apartmentNumber ?? "")   : "",
        bankNumber: !isModal ?( currentUser?.userSettings?.bankNumber ?? "") : "",
        buildingNumber: !isModal ? (currentUser?.userSettings?.buildingNumber ?? "") : "",
        city: !isModal ? (currentUser?.userSettings?.city ?? "") : "",
        email: !isModal ? (currentUser?.email ?? "" ) : "",
        name: !isModal ? (currentUser?.userSettings?.name ?? ""): "",
        street: !isModal ? (currentUser?.userSettings?.street ?? "") : "",
        zip: !isModal ?( currentUser?.userSettings?.zip ?? "") : "",
        payment: "p24"
    });
    const [otherPaymentsDetails, setOtherPaymentsDetails] = useState({
        productId: productId,
        email: currentUser?.userSettings?.email ?? "",
        name: currentUser?.userSettings?.name ?? "",
        phoneNumber: "",
        country: "Poland",
        code:  currentUser?.userSettings?.zip ?? "",
        city: currentUser?.userSettings?.city ?? "",
        street: currentUser?.userSettings?.street ?? "",
        buildingNumber: "",
        apartmentNumber: "",
        delivery: "Polska",
        deliveryService: "inpost_locker_standard",
        deliveryTargetPoint: "",
        verification: verificationChecked,
    });
    const [isDeliveryOpen, setIsDeliveryOpen] = useState(false);
    const [paymentMethods, setPaymentMethods] = useState([]);
    const handleChangeDelivery = (name) => {
        console.log(isPaczkomat, isKurier, name)

        if (name === "inpost_courier_standard") {
            setIsKurier(true);
            setIsPaczkomat(false);
        } else {
            setIsKurier(false);
            setIsPaczkomat(true);
        }
        setDelivery(name);
    };

    const handleChangePayments = (name, value) => {
        setPayments((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
        if (name !== "zip" && name !== "payment") {
            setOtherPaymentsDetails((prev) => {
                return {
                    ...prev,
                    [name]: value,
                };
            });
        }
    };
    const handleChangeOtherPayments = (name, value) => {
        setOtherPaymentsDetails((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handlePaczkomatChange = (postKode) => {
        setSelectedPostCod(postKode?.name);
        setSelectedAddress(postKode?.address?.line1);
        setSearchedText(postKode?.address?.line1);
        handleChangeOtherPayments("deliveryTargetPoint", postKode?.name);
    };
    const isFieldsEmpty = () => {
        return otherPaymentsDetails?.name === ""
            || otherPaymentsDetails?.city === ""
            || otherPaymentsDetails?.buildingNumber === ""
            || otherPaymentsDetails?.phoneNumber === ""
            || otherPaymentsDetails?.street === ""
            || otherPaymentsDetails?.code === ""
    }

    const handlePayment = () => {
        if(!isFieldsEmpty()){
            paymentService
                .createPayment(userToken, otherPaymentsDetails)
                .then((response) => {
                    if (response?.data?.data?.paymentUrl !== undefined) {
                        setBankUrl(response?.data?.data?.paymentUrl);
                    }
                });
            closeModal();
            openBasketModal(verificationChecked);
        }
    };

    const handlePostalCodeChange = (text) => {
        if (text.length <= 6) {
            if(text.length === 3){
                if(text[text.length - 1] === '-'){
                    handleChangePayments("zip", text.split('-')[0]);
                }else{
                    handleChangePayments("zip", `${text.slice(0,2)}-${text.slice(2)}`);
                }
            }
            else {
                handleChangePayments("zip", text);
            }
        }
    };
    const handleBankNumberChange = (text) => {
        if(text.length <= 26){
            handleChangePayments("bankNumber", text);
        }
    }
    const handleChangePaymentMethods = () => {
        setErrors("")
        userSettingsService
            .changePayments(userToken, payments)
            .then((response) => {
                if(response.data.success){
                    refreshMyInfo(userToken)
                    navigation.goBack();
                }else{
                    setErrors(response.data.errors)
                }

            });
    };
    const handleGetPaczkomats = (text) => {
        setSearchedText(text);
        deliveryService
            .getPointOnMap(text)
            .then((response) => {
                if (response.data[0] !== undefined)
                    return {
                        latitude: response?.data[0].lat,
                        longitude: response?.data[0].lon,
                    };
            })
            .then((result) => {
                if (result !== undefined) {
                    deliveryService
                        .getPaczkomats(result.latitude, result.longitude)
                        .then((paczkomatsResult) => {
                            setPaczkomats(paczkomatsResult.data.items);
                        });
                }
            });
    };
    const handleChangePaymentsData = (isChecked) => {
        setPayments(isChecked ? {
            apartmentNumber:   currentUser?.userSettings?.apartmentNumber ?? "",
            bankNumber:  currentUser?.userSettings?.bankNumber ?? "",
            buildingNumber:  currentUser?.userSettings?.buildingNumber ?? "",
            city:  currentUser?.userSettings?.city ?? "",
            email:  currentUser?.email ?? "",
            name:  currentUser?.userSettings?.name ?? "",
            street:  currentUser?.userSettings?.street?? "",
            zip: currentUser?.userSettings?.zip ?? "",
        } : {
            apartmentNumber:   "",
            bankNumber:   "",
            buildingNumber:   "",
            city:  "",
            email:   "",
            name: "",
            street:   "",
            zip: "",
        })
        setOtherPaymentsDetails(isChecked ? {
            productId: productId,
            email: currentUser?.userSettings?.email ?? "",
            name: currentUser?.userSettings?.name ?? "",
            phoneNumber: "",
            country: "Poland",
            code:  currentUser?.userSettings?.zip ?? "",
            city: currentUser?.userSettings?.city ?? "",
            street: currentUser?.userSettings?.street ?? "",
            buildingNumber: "",
            apartmentNumber: "",
            delivery: "Polska",
            deliveryService: "inpost_locker_standard",
            deliveryTargetPoint: "",
            verification: verificationChecked,
        } : {
            productId: productId,
            email: "",
            name: "",
            phoneNumber: "",
            country: "Poland",
            code:  "",
            city: "",
            street: "",
            buildingNumber: "",
            apartmentNumber: "",
            delivery: "Polska",
            deliveryService: "inpost_locker_standard",
            deliveryTargetPoint: "",
            verification: verificationChecked,
        })
    }
    const getErrorText = () => {
        switch (errors){
            case "Invalid bank account number":
                return 'Niepoprawny numer bankowy'
            default: return 'Pola niepoprawne'
        }
    }

    useEffect(() => {
        if(isModal)
            handleChangePaymentsData(paymentWithOldDataChecked)
    }, [paymentWithOldDataChecked])
    useEffect(() => {
        // userSettingsService.getPayments().then((response) => {
        //     handleChangePayments("payment", response?.data?.data[0].key);
        //     setPaymentMethods(
        //         response?.data?.data !== undefined ? response?.data?.data : []
        //     );
        // });
        userSettingsService
            .getCurrentUserPayments(userToken)
            .then((response) => {
                const paymentsKeys = Object.keys(payments);
                const paymentResponse = {};
                const requestPaymentKeys = Object.keys(response?.data?.data);
                requestPaymentKeys.map((item) =>
                    paymentsKeys.includes(item)
                        ? (paymentResponse[item] = response?.data?.data[item]
                              ? response?.data?.data[item]
                              : "")
                        : {}
                );
                paymentResponse["payment"] = response?.data?.data?.paymentMethod
                    ? response?.data?.data?.paymentMethod
                    : "";
                paymentResponse["bankNumber"] = "00000000000000000000000000";
                // setPayments(paymentResponse);
            });
    }, []);
    // useEffect(() => {
    //     setSelectedPaymentMethod(
    //         paymentMethods.find((item) => item?.key === payments.payment)
    //     );
    // }, [payments, paymentMethods]);
    return (
        // <KeyboardAvoidingView
        // behavior={Platform.OS === "ios" ? "padding" : null}
        // style={{flexGrow: 1}}
    // >
        <View >

            <Text style={styles.name}>Sposób dostawy</Text>
            <View
                style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 10,
                    marginBottom: 30
                }}
            >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TouchableOpacity
                        onPress={() =>
                            handleChangeDelivery("inpost_courier_standard")
                        }
                        style={{flexDirection: 'row', alignItems: 'center'}}
                    >
                        <CheckBox
                            value={isKurier}
                            style={[
                                styles.checkbox,
                                { borderWidth: isKurier ? 10 : 2, marginRight: Platform.OS === 'ios' ? 0 : 7 },
                            ]}
                        />

                    <Text style={[styles.label, { marginLeft: 5 }]}>
                        Kurier
                    </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TouchableOpacity
                        onPress={() =>
                            handleChangeDelivery("inpost_locker_standard")
                        }
                        style={{flexDirection: 'row', alignItems: 'center'}}
                    >
                        <CheckBox
                            value={isPaczkomat}
                            style={[
                                styles.checkbox,
                                { borderWidth: isPaczkomat ? 10 : 2, marginRight: Platform.OS === 'ios' ? 0 : 7 },
                            ]}
                        />


                    <Text style={[styles.label, { marginLeft: 5 }]}>
                        Paczkomat
                    </Text>
                    </TouchableOpacity>
                </View>
            </View>
            {isPaczkomat ? (
                <View style={styles.brand}>
                    <TextInput
                        onChangeText={(text) => handleGetPaczkomats(text)}
                        style={styles.input}
                        value={searchedText}
                        placeholder={"Miasto adres, kod pocztowy"}
                    />
                    <View
                        style={[
                            styles.containerPaczkomats,
                            {
                                display:
                                    searchedText === "" ||
                                    selectedAddress === searchedText
                                        ? "none"
                                        : "block",
                            },
                        ]}
                    >
                        {paczkomats
                            ?.filter((item) =>
                                item?.address?.line1?.includes(searchedText)
                            )
                            ?.slice(0, 10)
                            .map((item) => (
                                <TouchableOpacity
                                    key={item?.name}
                                    onPress={() => handlePaczkomatChange(item)}
                                    style={styles.autosuggest}
                                >
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            justifyContent: "space-between",
                                            width: "100%",
                                        }}
                                    >
                                        <Text style={styles.suggest}>
                                            {item?.address?.line1}
                                        </Text>
                                        <Text style={styles.suggest}>
                                            {item?.name}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            ))}
                    </View>
                </View>
            ) : (
                <></>
            )}
            <Text style={[styles.name, { paddingBottom: 27 }]}>
                Preferowany sposób wypłaty
            </Text>

            <DefaultTextInput
                isDefaultTextArea={true}
                label={"Imię i nazwisko"}
                value={payments?.name ? payments?.name : ""}
                handleChange={(text) => handleChangePayments("name", text)}
                placeholder={"Imię i nazwisko"}
            />
            {isModal ? (
                <DefaultTextInput
                    isDefaultTextArea={true}
                    label={"Telefon"}
                    value={otherPaymentsDetails.phoneNumber}
                    handleChange={(text) =>
                        handleChangeOtherPayments("phoneNumber", text)
                    }
                    keyboardType={"numeric"}
                    placeholder={"Telefon"}
                />
            ) : (
                <></>
            )}
            {isModal ? (
                <DefaultTextInput
                    isDefaultTextArea={true}
                    label={"Kraj zamieszkania"}
                    value={otherPaymentsDetails.country}
                    handleChange={(text) =>
                        handleChangeOtherPayments("country", text)
                    }
                    placeholder={"Kraj"}
                />
            ) : (
                <></>
            )}
            <DefaultTextInput
                isDefaultTextArea={true}
                label={"Ulica zamieszkania"}
                value={payments?.street ? payments?.street : ""}
                handleChange={(text) => handleChangePayments("street", text)}
                placeholder={"Ulica"}
            />

            {isModal ? (
                <View style={{ flexDirection: "row" }}>
                    <DefaultTextInput
                        additionalStyle={{ flex: 2, marginRight: 20 }}
                        isDefaultTextArea={true}
                        label={"Nr budynku"}
                        value={otherPaymentsDetails.buildingNumber}
                        handleChange={(text) =>
                            handleChangeOtherPayments("buildingNumber", text)
                        }
                        keyboardType={"numeric"}
                        placeholder={"Nr budynku"}
                    />
                    <DefaultTextInput
                        additionalStyle={{ flex: 2 }}
                        isDefaultTextArea={true}
                        label={"Nr mieszkania "}
                        value={otherPaymentsDetails.apartmentNumber}
                        handleChange={(text) =>
                            handleChangeOtherPayments("apartmentNumber", text)
                        }
                        keyboardType={"numeric"}
                        placeholder={"Nr mieszkania"}
                    />
                </View>
            ) : (
                <></>
            )}
            <View style={{ flexDirection: "row" }}>
                <DefaultTextInput
                    additionalStyle={{ flex: 4, marginRight: 20 }}
                    isDefaultTextArea={true}
                    label={"Miasto"}
                    value={payments?.city ? payments?.city : ""}
                    handleChange={(text) => handleChangePayments("city", text)}
                    placeholder={"Miasto"}
                />
                <DefaultTextInput
                    additionalStyle={{ flex: 2 }}
                    isDefaultTextArea={true}
                    label={"Kod pocztowy"}
                    placeholder={"00-000"}
                    value={payments?.zip }
                    handleChange={handlePostalCodeChange}
                    isNumeric={true}
                    keyboardType={"numeric"}
                />
            </View>
            {isModal ? (
                <></>
            ) : (
                <DefaultTextInput
                    isDefaultTextArea={true}
                    label={"Adres e-mail"}
                    placeholder={"E-mail"}
                    value={payments.email ? payments.email : ""}
                    handleChange={(text) => handleChangePayments("email", text)}
                />
            )}
            {/*<Text style={[styles.name, { paddingBottom: 25 }]}>*/}
            {/*    Metoda płatności*/}
            {/*</Text>*/}
            {/*<View*/}
            {/*    style={{*/}
            {/*        flexDirection: "row",*/}
            {/*        justifyContent: "space-between",*/}
            {/*    }}*/}
            {/*>*/}
            {/*    <Payment*/}
            {/*        icon={require("../../../assets/icons/tpay.svg")}*/}
            {/*        name={""}*/}
            {/*        iconWidth={180}*/}
            {/*        iconHeight={180}*/}
            {/*        isActive={true}*/}
            {/*        onPress={() => {}}*/}
            {/*    />*/}
            {/*</View>*/}
            {/*<KeyboardAvoidingView*/}
            {/*    behavior={Platform.OS === "ios" ? "height" : null}*/}
            {/*    style={{flex: 1}}*/}
            {/*>*/}
            {/*    <SafeAreaView style={{flex: 1}} >*/}
                    <DefaultTextInput  value={payments?.bankNumber ? payments?.bankNumber : ""} label={"Numer konta bankowego"}  handleChange={handleBankNumberChange}  isDefaultTextArea={true} placeholder={"00 0000 0000 0000 0000 0000 0000"} keyboardType={"numeric"}/>
                    {errors !== "" ? <Text style={styles.error}>{getErrorText()}</Text> : <></>}
                {/*</SafeAreaView>*/}
            {/**/}
            {/*</KeyboardAvoidingView>*/}


            <PrimaryButton
                onPress={() => {
                    if (isModal) {
                        handlePayment();

                    } else {
                        handleChangePaymentMethods();
                    }
                }}
                additionalStyle={{ marginTop: 20, marginBottom: 100 }}
                label={isModal ? "KONTYNUUJ PŁATNOŚĆ" : "ZAPISZ"}
            />
        </View>
        // </KeyboardAvoidingView>

    );
};

export default BuyNow;
