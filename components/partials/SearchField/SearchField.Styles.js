import { StyleSheet } from 'react-native';
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#323233',
        flexDirection: 'row',
    },
    submitButton: {
        backgroundColor: Colors.BLUE,
        width: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        flex: 1,
        height: 40,
        paddingHorizontal: 10,
        color: '#FFFFFF',
        fontFamily: 'Montserrat_400Regular',
    }
});