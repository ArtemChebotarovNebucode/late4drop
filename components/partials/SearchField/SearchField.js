import React from "react";
import { TextInput, TouchableOpacity, View } from "react-native";
import SvgUri from "expo-svg-uri";

import { styles } from "./SearchField.Styles";
import Colors from "../../../constants/Colors";

const SearchField = ({setSearchedText, additionalStyles, handleSearchProducts }) => {
    return (
        <View style={{...styles.container, ...additionalStyles}}>
            <TouchableOpacity onPress={() => handleSearchProducts()} style={styles.submitButton}>
                <SvgUri
                    width={24}
                    height={24}
                    source={require("../../../assets/icons/magnifier-white-icon.svg")}
                />
            </TouchableOpacity>
            <TextInput
                style={styles.textInput}
                placeholder={"Szukaj przedmiotu lub marki"}
                placeholderTextColor={Colors.GRAY_TEXT}
                onChangeText={(text) => setSearchedText(text)}
                returnKeyType="search"
                onSubmitEditing={() =>handleSearchProducts() }
            />
        </View>
    );
};

export default SearchField;