import React from "react";
import { Dimensions, Text, TouchableOpacity, View } from "react-native";
import SvgUri from "expo-svg-uri";

import { styles } from "./ProductContainer.Styles";
import Colors from "../../../constants/Colors";
import ImageBlurLoading from "react-native-image-blur-loading/src/index";
import { useNavigation } from "@react-navigation/native";

const ProductContainer = ({
    productImg,
    productTitle,
    productImagesCount,
    productPrice,
    productOldPrice,
    productSize,
    additionalStyles,
    productId,
    special,
    isProductLike,
    isAuction,
                              productEnds,
    isSold,
}) => {
    const navigation = useNavigation();
    const getAuctionAndTime = () => {
        const dayNow = new Date();
        const dateNowTemp = new Date();
        const date = new Date(productEnds);
        const distance = date.getTime() - dayNow.getTime();
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor(
            (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

        return (
            <Text
                style={{
                    fontSize: 12,
                    color: Colors.WHITE,
                    fontFamily: "Montserrat_500Medium",
                }}
            >
                {days}d {hours}h {minutes}m
            </Text>
        );
    };
    return (
        <TouchableOpacity
            style={{ ...additionalStyles, margin: 5 }}
            onPress={() => {
                navigation.navigate("ProductDetails", {
                    type: isAuction ? "AUCTION" : "BUY_NOW",
                    productId: productId,
                });
            }}
        >
            <View style={styles.container}>
                {special ? (
                    <View style={styles.specialWrap}>
                        <View style={styles.special}>
                            <Text style={styles.specialText}>
                                WYBÓR LATE4DROP
                            </Text>
                        </View>
                    </View>
                ) : null}

                <View
                    style={{
                        backgroundColor: Colors.GRAY_BACKGROUND,
                        alignItems: "center",
                    }}
                >
                    {productImagesCount !== 0 || productImagesCount ? (
                        <ImageBlurLoading
                            style={styles.productImg}
                            source={{ uri: productImg }}
                        />
                    ) : (
                        <View style={styles.productImg} />
                    )}
                    {isAuction ? (
                        <View
                            style={{
                                width: 126,
                                height: 20,
                                backgroundColor: Colors.RED_BACKGROUND,
                                position: "absolute",
                                top: 3,
                                borderRadius: 4,
                                alignItems: "center",
                                justifyContent: "center",
                            }}
                        >
                            {getAuctionAndTime()}
                        </View>
                    ) : (
                        <></>
                    )}
                    {isSold ? (
                        <View
                            style={{
                                position: "absolute",
                                backgroundColor: Colors.BLACK,
                                width: "100%",
                                height: "100%",
                                opacity: 0.3,
                            }}
                        />
                    ) : (
                        <></>
                    )}
                </View>

                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginHorizontal: 3,
                    }}
                >
                    <Text style={styles.productTitle}>
                        {Dimensions.get("window").width > 340
                            ? productTitle?.length > 12
                                ? `${productTitle?.slice(0, 12).trim()}...`
                                : productTitle
                            : productTitle?.length > 10
                            ? `${productTitle?.slice(0, 10).trim()}...`
                            : productTitle}
                    </Text>

                    <View
                        style={{ flexDirection: "row", alignItems: "flex-end" }}
                    >

                        <SvgUri
                            width={16}
                            height={16}
                            source={
                                !isProductLike
                                    ? require("../../../assets/icons/blue-heart-icon.svg")
                                    : require("../../../assets/icons/blue-heart-icon-filled.svg")
                            }
                            style={{ marginBottom: 1 }}
                            fill={Colors.BLUE_TEXT}
                        />
                    </View>
                </View>

                <View style={{ flexDirection: "row", marginHorizontal: 3 }}>
                    {!isSold ? (
                        <Text style={styles.productNewPrice}>
                            {parseInt(productPrice)} zł
                        </Text>
                    ) : (
                        <Text
                            style={{
                                fontSize: 14,
                                marginRight: "auto",
                                color: Colors.DARK_GRAY_TEXT,
                                fontFamily: "Montserrat_400Regular",
                            }}
                        >
                            Sprzedane
                        </Text>
                    )}

                    {/*{!isSold &&*/}
                    {/*(productOldPrice !== "0.00") ? (*/}
                    {/*    <Text style={styles.productOldPrice}>*/}
                    {/*        {parseInt(productPrice)} zł*/}
                    {/*    </Text>*/}
                    {/*) : (*/}
                    {/*    <></>*/}
                    {/*)}*/}

                    <Text style={styles.productSize}>{productSize}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default ProductContainer;
