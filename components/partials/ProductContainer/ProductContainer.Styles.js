import { StyleSheet, Dimensions } from "react-native";
import ImageResizeMode from "react-native/Libraries/Image/ImageResizeMode";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    productImg: {
        height: 190,
        width: "100%",
        resizeMode: ImageResizeMode.cover,
    },
    container: {
        width: Dimensions.get("window").width < 340 ? 140 : 160,
        height: 230,
    },
    productTitle: {
        fontFamily: "Montserrat_700Bold",
        fontSize: 14,
        marginTop: 5,
    },
    productNewPrice: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 14,
        marginRight: 5,
        color: Colors.GRAY_TEXT
    },
    productOldPrice: {
        fontFamily: "Montserrat_400Regular",
        fontSize: 14,
        color: Colors.GRAY_TEXT,
        textDecorationLine: "line-through",
        marginRight: 5,
    },
    productSize: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 14,
        color: Colors.GRAY_TEXT,
        marginBottom: 1,
        marginLeft: 'auto'
    },
    productLikesCount: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 14,
        color: Colors.BLUE_TEXT,
        marginHorizontal: 3,
    },
    special: {
        backgroundColor: Colors.BLUE,
        paddingHorizontal: 10,
    },
    specialText: {
        fontFamily: "Montserrat_500Medium",
        fontSize: 12,
        color: "#FFFFFF",
    },
    specialWrap: {
        position: "absolute",
        zIndex: 30,
        alignItems: "center",
        width: "100%",
        marginTop: 5,
    },
});