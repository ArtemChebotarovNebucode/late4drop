import React from "react";
import { Image, View } from "react-native";
import SvgUri from "expo-svg-uri";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import MainScreen from "../../../screens/MainScreen/MainScreen";
import ProfileScreen from "../../../screens/ProfileScreen/LoggedInUser/ProfileScreen";

import { styles } from "./CustomNavBar.Styles";
import Colors from "../../../constants/Colors";
import SearchScreen from "../../../screens/SearchScreen/SearchScreen";
import {ChatListScreen, SearchScreenMain} from "../../../screens";
import SeparatorScreen from "../../../screens/SeparatorScreen/SeparatorScreen";
import { useAuth } from "../../../contexts/AuthProvider/AuthProvider";

const CustomNavBar = () => {
    const Tab = createBottomTabNavigator();
    const { currentUser } = useAuth();

    const customTabBarStyle = {
        activeTintColor: Colors.BLUE,
        inactiveTintColor: "#8F92A1",
        showLabel: false,
        style: styles.stickyNav,
    };

    return (
        <Tab.Navigator
            initialRouteName="Main"
            tabBarOptions={customTabBarStyle}
            shifting="false"
        >
            <Tab.Screen
                name="Main"
                options={{
                    tabBarIcon: ({ color }) => (
                        <View style={styles.tabWrapper}>
                            <SvgUri
                                width={20}
                                height={20}
                                source={require("../../../assets/icons/home-icon.svg")}
                                fill={color}
                            />
                        </View>
                    ),
                }}
                component={SearchScreenMain}
            />
            <Tab.Screen
                name="Search"
                options={{
                    tabBarIcon: ({ color }) => (
                        <View style={styles.tabWrapper}>
                            <SvgUri
                                width={28}
                                height={28}
                                source={require("../../../assets/icons/magnifier-white-icon.svg")}
                                fill={color}
                            />
                        </View>
                    ),
                }}
                component={SearchScreen}
            />
            <Tab.Screen
                name="Separator"
                options={{
                    tabBarIcon: ({}) => (
                        <View style={styles.tabWrapper}>
                            <SvgUri
                                width={30}
                                height={30}
                                source={require("../../../assets/icons/plus-nav-icon.svg")}
                            />
                        </View>
                    ),
                }}
                component={SeparatorScreen}
            />

            <Tab.Screen
                name="ChatList"
                options={{
                    tabBarIcon: ({ color }) => (
                        <View style={styles.tabWrapper}>
                            <SvgUri
                                width={24}
                                height={19.58}
                                source={require("../../../assets/icons/profile-menu-options/messages-icon.svg")}
                                fill={color}
                            />
                        </View>
                    ),
                }}
                component={ChatListScreen}
            />
            <Tab.Screen
                name="Profile"
                options={{
                    tabBarIcon: ({ color }) => (
                        <View style={styles.tabWrapper}>
                            <Image
                                style={{
                                    width: 21,
                                    height: 21,
                                    borderWidth: 1,
                                    borderColor:
                                        color !== "#8F92A1"
                                            ? Colors.BLUE
                                            : "transparent",
                                    borderRadius: 50,
                                }}
                                source={
                                    currentUser?.photoURL
                                        ? { uri: currentUser?.photoURL }
                                        : require("../../../assets/images/profile-photo-standard.png")
                                }
                            />
                        </View>
                    ),
                }}
                component={ProfileScreen}
            />
        </Tab.Navigator>
    );
};

export default CustomNavBar;
