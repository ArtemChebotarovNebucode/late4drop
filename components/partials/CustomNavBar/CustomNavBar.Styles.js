import {StyleSheet} from "react-native";
import Colors from "../../../constants/Colors";

export const styles = StyleSheet.create({
    stickyNav: {
        backgroundColor: Colors.BLACK_BACKGROUND,
        borderTopWidth: 0
    },
    tabWrapper: {
        flexDirection: "row",
        alignItems:"center",
    }
});
