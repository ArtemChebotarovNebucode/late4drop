import React from 'react';
import { SafeAreaView, StatusBar, View } from 'react-native';

const StyledStatusBar = ({ backgroundColor }) => {
	return (
		<View style={{ backgroundColor }}>
			<SafeAreaView>
				<StatusBar translucent backgroundColor={backgroundColor} barStyle="light-content" />
			</SafeAreaView>
		</View>
	);
};

export default StyledStatusBar;
