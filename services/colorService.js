import axios from "axios";


export default class ColorService {
  _apiBase = "https://api.late4drop.com"
  // _apiBase = "https://skrr-api-dev.nextrope.com";
  fetchColors = async () => {
    return await axios.get(`${this._apiBase}/color/all`);
  };
}
