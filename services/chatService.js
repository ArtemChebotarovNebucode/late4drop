import axios from "axios";

export default class ChatService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";
    getUserChats = async (token) => {
        return await axios.get(`${this._apiBase}/chat/me/chats`, {
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    };
    getChatById = async (token, chatId) => {
        return await axios.get(`${this._apiBase}/chat/${chatId}`, {
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    };
    sendMessage = async (token, chatId, message) => {
        return await axios.post(
            `${this._apiBase}/chat/sendMessage/${chatId}`,
            { message: message },
            {
                headers: {
                    Authorization: "Bearer " + token,
                },
            }
        );
    };
}
