import axios from "axios";


export default class CategoriesService {
  _apiBase = "https://api.late4drop.com"
  // _apiBase = "https://skrr-api-dev.nextrope.com";
  fetchCategories = async () => {
    return await axios.get(`${this._apiBase}/category/all`);
  };
}
