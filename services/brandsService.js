import axios from "axios";

export default class BrandsService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";

    fetchBrands = async () => {
        return await axios.get(`${this._apiBase}/brand/all`);
    };
}
