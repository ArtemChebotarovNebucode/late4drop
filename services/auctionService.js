import axios from "axios";

export default class AuctionService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";

    addAuction = async (token, product) => {
        return await axios.post(`${this._apiBase}/auction/add_auction`, product,{
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    };
    getAllAuction = async (filters) => {
        return await axios.post(`${this._apiBase}/auction/get_auctions`, filters);
    };
    getAuctionById = async (id) => {
        return await axios.get(`${this._apiBase}/auction/${id}`);
    };


}
