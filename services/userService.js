import axios from "axios";
import { AsyncStorage } from "react-native";

export default class UserService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";
    fetchMyInfo = async (token) => {
        return await axios.get(`${this._apiBase}/user/me`, {
            headers: { Authorization: `Bearer ${token}` },
        });
    };

    changeAvatar = async (base64) => {
        return await AsyncStorage.getItem("ACCESS_TOKEN").then((token) =>
          axios.post(
            `${this._apiBase}/user/change_avatar`,
            { base64: `data:image/png;base64,${base64}` },
            { headers: { Authorization: `Bearer ${token}` } }
          )
        );
    };

    editMyInfo = async (data) => {
        return await AsyncStorage.getItem("ACCESS_TOKEN").then((token) =>
          axios.put(`${this._apiBase}/user/me/edit`, data, {
              headers: { Authorization: `Bearer ${token}` },
          })
        );
    };

    login = async (token, email) => {
        return await axios.post(
          `${this._apiBase}/user/login`,
          {
              username: email,
          },
          {
              headers: {
                  Authorization: "Bearer " + token,
              },
          }
        );
    };

    getOpinions = async (id) => {
        return await axios.post(`${this._apiBase}/user/${id}/opinions`);
    };

    startLikingById = async (token, id) => {
        return await axios.post(
          `${this._apiBase}/user/start_liking_by_id`,
          { id: id },
          {
              headers: { Authorization: `Bearer ${token}` },
          }
        );
    };

    stopLikingById = async (token, id) => {
        return await axios.post(
          `${this._apiBase}/user/stop_liking_by_id`,
          { id: id },
          {
              headers: { Authorization: `Bearer ${token}` },
          }
        );
    };
    getLikedProducts = async (id) => {
        return await axios.get(`${this._apiBase}/user/${id}/liked_products`);
    };

    getFollowedByCount = async (id) => {
        return await axios.get(`${this._apiBase}/user/${id}/followed_by_count`);
    };

    getFollowedUsersCount = async (id) => {
        return await axios.get(
          `${this._apiBase}/user/${id}/followed_users_count`
        );
    };

    startFollowingById = async (id) => {
        return await AsyncStorage.getItem("ACCESS_TOKEN").then((token) =>
          axios.post(
            `${this._apiBase}/user/start_following_by_id`,
            { id: id },
            { headers: { Authorization: `Bearer ${token}` } }
          )
        );
    };

    stopFollowingById = async (id) => {
        return await AsyncStorage.getItem("ACCESS_TOKEN").then((token) =>
          axios.post(
            `${this._apiBase}/user/stop_following_by_id`,
            { id: id },
            { headers: { Authorization: `Bearer ${token}` } }
          )
        );
    };

    getActiveProducts = async (userId) => {
        return await axios.get(
          `${this._apiBase}/user/${userId}/active_products`
        );
    };

    getAnotherUserInfo = async (userId) => {
        return axios.get(`${this._apiBase}/user/${userId}`);
    };

    getMySoldProducts = async () => {
        return await AsyncStorage.getItem("ACCESS_TOKEN").then((token) => {
            return axios.get(`${this._apiBase}/user/me/sold_products`, {
                headers: { Authorization: `Bearer ${token}` },
            });
        }
        );
    }

    getMyBoughtProducts = async () => {
        return await AsyncStorage.getItem("ACCESS_TOKEN").then((token) => {
              return axios.get(`${this._apiBase}/user/me/bought_products`, {
                  headers: { Authorization: `Bearer ${token}` },
              });
          }
        );
    }
}
