import axios from "axios";

const axiosInstance = axios.create({
    baseURL: "https://api.late4drop.com/",
});

export default axiosInstance;
