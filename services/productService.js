import axios from "axios";

export default class ProductService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";

    getSortOption = (sort) => {
        switch (sort){
            case "Najnowsze":
                return ',"order":["price ASC"]'
            case "Najdroższe":
                return ',"order":["price DESC"]'
            default: return ',"order":["id desc"]';
        }
    }

    getQueryItemsId = (items) =>  {
        let result = "";
        for(let i =0; i< items?.length; i++){
            if(i !== items?.length - 1){
                result += `${items[i]},`
            }else{
                result += items[i]
            }
        }
        return `"inq":[${result}]`
    }
    getQueryItems = (items, name) => {
        let result = "";
        for(let i=0; i < items?.length; i++){
            result += `&${name}=${items[i]}`
        }
        return result;
    }

    getQueryString = (filters) => {
        const clothingSizes = filters?.clothingSizes?.length !== 0 ? this.getQueryItems(filters?.clothingSizes, "clothingSizes") : "";
        const shoeSizes = filters?.shoeSizes?.length !== 0 ? this.getQueryItems(filters?.shoeSizes, "shoeSizes") : "";
        const searchString = filters?.searchString !== "" ? `&searchString=${filters.searchString}` : "";
        const brandId = filters?.brands?.length === 0 ? "" : `"brandId": {${this.getQueryItemsId(filters.brands)}},`;
        const colorId = filters?.colors?.length === 0 ? "" : `"colorId": {${this.getQueryItemsId(filters.colors)}},`;
        const conditionId = filters?.conditions?.length === 0 ? "" : `"conditionId": {${this.getQueryItemsId(filters.conditions)}},`;
        const categoryId = filters?.categories?.length === 0 ? "" : `"categoryId": {${this.getQueryItemsId(filters.categories)}},`;
        const sort = filters?.sort ? this.getSortOption(filters.sort) : ',"order":["id desc"]'
        return `{"where":{${brandId}${colorId}${conditionId}${categoryId}"and":[{"price":{"gte": ${filters?.minPrice}}},{"price": {"lte": ${filters?.maxPrice === 30000 ? 9999999 : filters?.maxPrice}}}]} ${sort},"limit":40, "skip": ${filters.page * 40}}${clothingSizes}${shoeSizes}${searchString}`
    }

    getAllProducts = async (filters) => {
        const queryString = this.getQueryString(filters)
        return await axios.get(`${this._apiBase}/product?filter=${queryString}`);
    };
    getLateForDropChoiceProducts = async () => {
        return await axios.get(`${this._apiBase}/product/skrr_choice`);
    };
    getLastAddedProducts = async () => {
        return await axios.get(`${this._apiBase}/product/last_twenty`);
    };
    getProductById = async (id) => {
        return await axios.get(`${this._apiBase}/product/${id}`);
    };
    getGetSimilarProduct = async (id) => {
        return await axios.get(`${this._apiBase}/product/${id}/similar_products`);
    };
    addProduct = async (token, product) => {
        return await axios.post(`${this._apiBase}/product/add_product`, product,{
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }
    searchProducts = async (search) => {
        return await axios.get(`${this._apiBase}/product/by_name_or_desc/$${search}`);
    }

    getSpecialProducts = async () => {
        return await axios.get(`${this._apiBase}/product/skrr_choice`);
    }

    deleteProduct = async (productId, token) => {
        return await axios.put(`${this._apiBase}/product/cancel/${productId}`, {},{
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }
}
