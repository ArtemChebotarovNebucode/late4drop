import axios from "axios";


export default class ConditionService {
  _apiBase = "https://api.late4drop.com"
  // _apiBase = "https://skrr-api-dev.nextrope.com";
  fetchConditions = async () => {
    return await axios.get(`${this._apiBase}/condition/all`);
  };
}
