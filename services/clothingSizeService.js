import axios from "axios";

export default class ClothingSizeService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";

    getAllClothingSize = async () => {
        return await axios.get(`${this._apiBase}/cothing_size/all`);
    };


}
