import axios from "axios";

export default class ShoeSizeService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";

    getAllShoeSize = async () => {
        return await axios.get(`${this._apiBase}/shoe_size/all`);
    };


}
