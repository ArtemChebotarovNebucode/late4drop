import axios from "axios";

export default class PaymentService {
    _apiBase = "https://api.late4drop.com/create_payment";
    // _apiBase = "https://skrr-api-dev.nextrope.com/create_payment";
    createPayment = async (token, payment) => {
        return await axios.post(`${this._apiBase}/user`, payment,{
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    };


}
