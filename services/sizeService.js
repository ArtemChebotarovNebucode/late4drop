import axios from "axios";


export default class SizeService {
  _apiBase = "https://api.late4drop.com"
  // _apiBase = "https://skrr-api-dev.nextrope.com";
  fetchShoeSizes = async () => {
    return await axios.get(`${this._apiBase}/shoe_size/all`);
  };

  fetchClothingSizes = async () => {
    return await axios.get(`${this._apiBase}/cothing_size/all`);
  }
}
