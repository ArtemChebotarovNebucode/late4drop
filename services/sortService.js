import axios from "axios";


export default class SortService {
  _apiBase = "https://api.late4drop.com"
  // _apiBase = "https://skrr-api-dev.nextrope.com";
  fetchSortOptions = async () => {
    return await axios.get(`${this._apiBase}/sort/all`);
  };
}
