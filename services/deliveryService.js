import axios from "axios";

export default class DeliveryService {

    getPointOnMap = async (name) => {
        return await axios.get(`https://osm.inpost.pl/nominatim/search?q=${name}&format=jsonv2`);
    };
    getPaczkomats = async (latitude, longitude) => {
        return await axios.get(`https://api-pl-points.easypack24.net/v1/points?fields=name%2Ctype%2Clocation%2Caddress%2Caddress_details%2Cfunctions%2Clocation_date%2Copening_hours%2Clocation_247%2Capm_doubled%2Cimage_url&status=Operating%2CNonOperating&type=parcel_locker_superpop%2Cparcel_locker&relative_point=${latitude}%2C${longitude}9&max_distance=1197.1939533312227&limit=200&source=geov4_pl`)
    }
    /*

    https://api-pl-points.easypack24.net/v1/points?fields=name%2Ctype%2Clocation%2Caddress%2Caddress_details%2Cfunctions%2Clocation_date%2Copening_hours%2Clocation_247%2Capm_doubled%2Cimage_url&status=Operating%2CNonOperating&type=parcel_locker_superpop%2Cparcel_locker&relative_point=52.231925921644205%2C21.00673913955689&max_distance=1197.1939533312227&limit=200&source=geov4_pl

     */


}
