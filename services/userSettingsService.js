import axios from "axios";
import { AsyncStorage } from "react-native";

export default class UserSettingsService {
    _apiBase = "https://api.late4drop.com";
    // _apiBase = "https://skrr-api-dev.nextrope.com";

    changePayments = async (token, payments) => {
        return await axios.put(
            `${this._apiBase}/user_settings/change/payments`,
            payments,
            {
                headers: { Authorization: `Bearer ${token}` },
            }
        );
    };
    getPayments = async () => {
        return await axios.get(`${this._apiBase}/user_settings/payments`);
    };
    getCurrentUserPayments = async (token) => {
        return await axios.get(
            `${this._apiBase}/user_settings/me`,
            {
                headers: { Authorization: `Bearer ${token}` },
            }
        );
    };
}
